#ifndef IO_H
#define IO_H
#include "global.h"
#include "uart.h"
#include "hi_mpp.h"

#define GPIO1_5MUX      0x120F0018  //1-GPIO
#define GPIO1_5DIR      0x12160400
#define GPIO1_5DATA     0x12160080
#define GPIO1_5SET      0x20

#define GPIO20_5MUX     0x120F0270  //0-GPIO
#define GPIO20_5DIR     0x12290400
#define GPIO20_5DATA    0x12290080
#define GPIO20_5SET     0x20

#define GPIO1_6MUX      0x120F001C  //1-GPIO
#define GPIO1_6DIR      0x12160400
#define GPIO1_6DATA     0x12160100
#define GPIO1_6SET      0x40

#define GPIO1_7MUX      0x120F0020  //1-GPIO
#define GPIO1_7DIR      0x12160400
#define GPIO1_7DATA     0x12160200
#define GPIO1_7SET      0x80

#define GPIO15_6MUX     0x120F01C8  //1-GPIO
#define GPIO15_6DIR     0x12240400
#define GPIO15_6DATA    0x12240100
#define GPIO15_6SET     0x40

#ifdef __7000__
#define GPIO0_6DIR      0x12150400
#define GPIO0_6DATA     0x12150100
#define GPIO0_6SET      0x40

#define GPIO13_5MUX     0x120F0178
#define GPIO13_5DIR     0x12220400
#define GPIO13_5DATA    0x12220080
#define GPIO13_5SET     0x20

#define GPIO0_7DIR      0x12150400
#define GPIO0_7DATA     0x12150200
#define GPIO0_7SET      0x80
#endif


#define GPIO0_3DIR      0x12150400      //IO_1 or IR
#define GPIO0_3DATA     0x12150020
#define GPIO0_3SET      0x8

#define GPIO0_4DIR      0x12150400      //DIR_1
#define GPIO0_4DATA     0x12150040
#define GPIO0_4SET      0x10

#define GPIO0_6DIR      0x12150400      //IO_2
#define GPIO0_6DATA     0x12150100
#define GPIO0_6SET      0x40

#define GPIO13_5MUX     0x120F0178      //DIR_2
#define GPIO13_5DIR     0x12220400
#define GPIO13_5DATA    0x12220080
#define GPIO13_5SET     0x20


#define GPIO0_5DIR      0x12150400      //io_1 or ir
#define GPIO0_5DATA     0x12150080
#define GPIO0_5SET      0x20

#define GPIO0_3DIR      0x12150400
#define GPIO0_3DATA     0x12150020
#define GPIO0_3SET      0x8


//3536
#define GPIO11_2REG     0x120F0160        //gpio 1
#define GPIO11_2DIR     0x12200400
#define GPIO11_2DATA    0x12200010
#define GPIO11_2SET     0x4

#define GPIO11_1REG     0x120F015C      //gpio 1
#define GPIO11_1DIR     0x12200400
#define GPIO11_1DATA    0x12200008
#define GPIO11_1SET     0x2

#define GPIO11_3REG     0x120F009C      //gpio 0
#define GPIO11_3DIR     0x12200400
#define GPIO11_3DATA    0x12200020
#define GPIO11_3SET     0x8

#define GPIO14_7DIR     0x12230400
#define GPIO14_7DATA    0x12230200
#define GPIO14_7SET     0x80

//南京31DV200
#define IO_0_DIR_REG    0x102F0004      //3:0 0x0 GPIO0_1
#define IO_0_DIR_DIR    0x11090400
#define IO_0_DIR_DATA   0x11090008
#define IO_0_DIR_SET    0x2
#define IO_0_DATA_REG   0x102F0000      //3:0 0x0 GPIO0_0
#define IO_0_DATA_DIR   0x11090400
#define IO_0_DATA_DATA  0x11090004
#define IO_0_DATA_SET   0x1

#define IO_1_DIR_REG    0x102F000C      //3:0 0x0 GPIO0_3
#define IO_1_DIR_DIR    0x11090400
#define IO_1_DIR_DATA   0x11090020
#define IO_1_DIR_SET    0x8
#define IO_1_DATA_REG   0x102F0008      //3:0 0x0 GPIO0_2
#define IO_1_DATA_DIR   0x11090400
#define IO_1_DATA_DATA  0x11090010
#define IO_1_DATA_SET   0x4

#define IO_2_DIR_REG    0x102F0010      //3:0 0x0 GPIO0_4
#define IO_2_DIR_DIR    0x11090400
#define IO_2_DIR_DATA   0x11090040
#define IO_2_DIR_SET    0x10
#define IO_2_DATA_REG   0x102F0090      //3:0 0x0 GPIO5_4
#define IO_2_DATA_DIR   0x11095400
#define IO_2_DATA_DATA  0x11095040
#define IO_2_DATA_SET   0x10

#define IO_3_DIR_REG    0x102F0014      //3:0 0x0 GPIO0_5
#define IO_3_DIR_DIR    0x11090400
#define IO_3_DIR_DATA   0x11090080
#define IO_3_DIR_SET    0x20
#define IO_3_DATA_REG   0x10FF0070      //3:0 0x1 GPIO1_4
#define IO_3_DATA_DIR   0x11091400
#define IO_3_DATA_DATA  0x11091040
#define IO_3_DATA_SET   0x10

//南京31DV200  A304
#define A304_IO_0_DIR_REG    0x017C7020C    //3:0 0x0 GPIO22_5
#define A304_IO_0_DIR_DIR    0x110a6400
#define A304_IO_0_DIR_DATA   (0x110a6000+(1<<7))
#define A304_IO_0_DIR_SET    (1<<5)
#define A304_IO_0_DATA_REG   0x010FF007C    //3:0 0x0 GPIO1_7
#define A304_IO_0_DATA_DIR   0x11091400
#define A304_IO_0_DATA_DATA  (0x11091000+(1<<9))
#define A304_IO_0_DATA_SET   (1<<7)

#define A304_IO_1_DIR_REG    0x017C70194    //3:0 0x3 GPIO18_7
#define A304_IO_1_DIR_DIR    0x110a2400
#define A304_IO_1_DIR_DATA   (0x110a2000+(1<<9))
#define A304_IO_1_DIR_SET    (1<<7)
#define A304_IO_1_DATA_REG   0x017C701B4    //3:0 0x3 GPIO19_7
#define A304_IO_1_DATA_DIR   0x110a3400
#define A304_IO_1_DATA_DATA  (0x110a3000+(1<<9))
#define A304_IO_1_DATA_SET   (1<<7)

#define A304_IO_2_DIR_REG    0x017C70188    //3:0 0x3 GPIO18_4
#define A304_IO_2_DIR_DIR    0x110a2400
#define A304_IO_2_DIR_DATA   (0x110a2000+(1<<6))
#define A304_IO_2_DIR_SET    (1<<4)
#define A304_IO_2_DATA_REG   0x017C7018C    //3:0 0x3 GPIO18_5
#define A304_IO_2_DATA_DIR   0x110a2400
#define A304_IO_2_DATA_DATA  (0x110a2000+(1<<7))
#define A304_IO_2_DATA_SET   (1<<5)

#define A304_IO_3_DIR_REG    0x017C70208    //3:0 0x0 GPIO22_4
#define A304_IO_3_DIR_DIR    0x110a6400
#define A304_IO_3_DIR_DATA   (0x110a6000+(1<<6))
#define A304_IO_3_DIR_SET    (1<<4)
#define A304_IO_3_DATA_REG   0x017C701B8    //3:0 0x3 GPIO20_0
#define A304_IO_3_DATA_DIR   0x110a4400
#define A304_IO_3_DATA_DATA  (0x110a4000+(1<<2))
#define A304_IO_3_DATA_SET   (1<<0)

//广州31DV200
#define IO_4_DIR_REG    0x017C701A0           //3:0 0x3 GPIO19_2
#define IO_4_DIR_DIR    (0x110a3000+0x400)
#define IO_4_DIR_DATA   (0x110a3000+0x10)     //(100_00)
#define IO_4_DIR_SET    0x4                   //输出（0100）
#define IO_4_DATA_REG   0x010FF007C         //3:0 0x0 GPIO1_7
#define IO_4_DATA_DIR   (0x11091000+0x400)  //0x11091400
#define IO_4_DATA_DATA  (0x11091000+0x200)  //(10000000_00)
#define IO_4_DATA_SET   0x80                //(10000000)


#define IO_5_DIR_REG     0x017C7019C          //3:0 0x3 GPIO19_1
#define IO_5_DIR_DIR    (0x110a3000+0x400)
#define IO_5_DIR_DATA   (0x110a3000+0x8)      //(10_00)
#define IO_5_DIR_SET    0x2                   //输出(0010)
#define IO_5_DATA_REG   0x017C70198           //3:0 0x3 GPIO19_0
#define IO_5_DATA_DIR   (0x110a3000+0x400)
#define IO_5_DATA_DATA  (0x110a3000+0x4)      //(1_00)
#define IO_5_DATA_SET   0x1                   //(0001)

//南京21DV100 核心板
//IN
#define A151_IO_0_DIR_REG    0x120F00B0           //0  GPIO9_4   IO_0方向复用（input板）
#define A151_IO_0_DIR_DIR    (0x121E0000+0x400)
#define A151_IO_0_DIR_DATA   (0x121E0000+0x40)     //(10000_00)
#define A151_IO_0_DIR_SET    0x10                   //输出（00010000）

#define A151_IO_0_DATA_DIR   (0x12150000+0x400)  //0x0x12150400  GPIO0_5
#define A151_IO_0_DATA_DATA  (0x12150000+0x80)  //(100000_00)
#define A151_IO_0_DATA_SET   0x20                //(100000)
//OUT
#define A151OUT_IO_0_DIR_REG    0x120F00BC           //0  GPIO9_7  IO_0方向复用(output板)
#define A151OUT_IO_0_DIR_DIR    (0x121E0000+0x400)
#define A151OUT_IO_0_DIR_DATA   (0x121E0000+0x200)     //(10000000_00)
#define A151OUT_IO_0_DIR_SET    0x80                   //输出（00010000）

#define A151OUT_IO_0_DATA_REG   0x120F00B0            // 0  GPIO9_4   IO_0数据复用（output板）
#define A151OUT_IO_0_DATA_DIR   (0x121E0000+0x400)  //
#define A151OUT_IO_0_DATA_DATA  (0x121E0000+0x40)  //(10000_00)
#define A151OUT_IO_0_DATA_SET   0x10                //输出（00010000）


//IN
#define A151_IO_1_DIR_REG     0x120F00B4          //0  GPIO9_5      IO_1方向复用（input板）
#define A151_IO_1_DIR_DIR    (0x121E0000+0x400)
#define A151_IO_1_DIR_DATA   (0x121E0000+0x80)      //(100000_00)
#define A151_IO_1_DIR_SET    0x20                   //输出(00100000)

#define A151_IO_1_DATA_DIR   (0x12150000+0x400)     // GPIO0_3
#define A151_IO_1_DATA_DATA  (0x12150000+0x20)      //(1000_00)
#define A151_IO_1_DATA_SET   0x8                   //(1000)
//OUT
#define A151OUT_IO_1_DIR_REG     0x120F00C0          //0  GPIO5_4    IO_1方向复用(output板)
#define A151OUT_IO_1_DIR_DIR    (0x121A0000+0x400)
#define A151OUT_IO_1_DIR_DATA   (0x121A0000+0x40)      //(10000_00)
#define A151OUT_IO_1_DIR_SET    0x10                   //输出(0010000)


#define A151OUT_IO_1_DATA_REG   0x120F00B4          //0  GPIO9_5      IO_1数据复用（output板）
#define A151OUT_IO_1_DATA_DIR   (0x121E0000+0x400)
#define A151OUT_IO_1_DATA_DATA  (0x121E0000+0x80)      //(100000_00)
#define A151OUT_IO_1_DATA_SET   0x20                   //输出(00100000)


#define A151_UART_RX_REG 0x120F0100          //串口复用地址  1  UART2_RX
#define A151_UART_TX_REG 0x120F0104          //  1  UART2_TX
//21D核心板 A323
#define A323_IO_0_DIR_DIR   (0x12150000+0x400)     // GPIO0_3
#define A323_IO_0_DIR_DATA  (0x12150000+0x20)      //(1000_00)
#define A323_IO_0_DIR_SET   0x8                   //(1000)

#define A323_IO_0_DATA_DIR   (0x12150000+0x400)  //0x0x12150400  GPIO0_5
#define A323_IO_0_DATA_DATA  (0x12150000+0x80)  //(100000_00)
#define A323_IO_0_DATA_SET   0x20                //(100000)

#define A323_IO_1_DATA_DIR   (0x12150000+0x400)  //0x0x12150400  GPIO0_2
#define A323_IO_1_DATA_DATA  (0x12150000+0x10)  //(100_00)
#define A323_IO_1_DATA_SET   0x4                //(100)

#define A323_UART_RX_REG 0x120F0100          //串口复用地址  1  UART2_RX
#define A323_UART_TX_REG 0x120F0104          //  1  UART2_TX


//广州31A_D2

#define GZ31A_D2_UART_RX_REG 0x120F0218         //串口复用地址  1  UART3_RX
#define GZ31A_D2_UART_TX_REG 0x120F021C          //  1  UART3_TX

//21A
#define IO_DIR_11_6_MUX     0x120F0098              //0x0 GPIO11_6
#define IO_DIR_11_6_DIR     0x12200400
#define IO_DIR_11_6_DATA    0x12200100
#define IO_DIR_11_6_SET     0x40

#define IO_DIR_11_3_MUX     0x120F009C              //0x0 GPIO11_3
#define IO_DIR_11_3_DIR     0x12200400
#define IO_DIR_11_3_DATA    0x12200020
#define IO_DIR_11_3_SET     0x8

#define IO_1_5_6_MUX        0x120F00DC             //0x1 GPIO5_6
#define IO_1_5_6_DIR        0x121A0400
#define IO_1_5_6_DATA       0x121A0100
#define IO_1_5_6_SET        0x40

#define IO_0_10_2_MUX       0x120F0154              //0x0 GPIO10_2
#define IO_0_10_2_DIR       0x121F0400
#define IO_0_10_2_DATA      0x121F0010
#define IO_0_10_2SET        0X4

//A186  7000plus  21D
// GPIO12_4
#define IO0_DIR_A186_DIR     0x12210400
#define IO0_DIR_A186_DATA    0x12210040
#define IO0_DIR_A186_SET     0x10

// GPIO10_2
#define IO0_DATA_A186_MUX     0x120F0154              //0x0 GPIO10_2
#define IO0_DATA_A186_DIR     0x121F0400
#define IO0_DATA_A186_DATA    0x121F0010
#define IO0_DATA_A186_SET     0x4

// GPIO12_3
#define IO1_DIR_A186_DIR     0x12210400
#define IO1_DIR_A186_DATA    0x12210020
#define IO1_DIR_A186_SET     0x8

// GPIO12_2
#define IO1_DATA_A186_DIR     0x12210400
#define IO1_DATA_A186_DATA    0x12210010
#define IO1_DATA_A186_SET     0x4

#define A335_IO0_DIR_0_5_DIR        0x12150400
#define A335_IO0_DIR_0_5_DATA       0x12150080
#define A335_IO0_DIR_0_5_SET        0x20

#define A335_IO0_DATA_0_7_DIR       0x12150400
#define A335_IO0_DATA_0_7_DATA      0x12150200
#define A335_IO0_DATA_0_7_SET       0x80

#define A335_IO1_DIR_5_0_REG        0x120F00C4          //2-GPIO
#define A335_IO1_DIR_5_0_DIR        0x121A0400
#define A335_IO1_DIR_5_0_DATA       0x121A0004
#define A335_IO1_DIR_5_0_SET        0x1

#define A335_IO1_DATA_13_2_REG      0x120F016C          //0-GPIO
#define A335_IO1_DATA_13_2_DIR      0x12220400
#define A335_IO1_DATA_13_2_DATA     0x12220010
#define A335_IO1_DATA_13_2_SET      0x4



class io
{
public:
io();
~io();
bool ioIni(int ioNum);
bool rcvData2IO();
bool IOSet(int set,int ioNum);
bool UMPsetParam(bool uartFlag,bool ctrlPortFlag,int svPortFlag);
struct _io io_0_dir;
struct _io io_0_set;
struct _io io_1_dir;
struct _io io_1_set;
struct _io io_2_dir;
struct _io io_2_set;
struct _io io_3_dir;
struct _io io_3_set;
public:
int socketIO = -1;
int io0Str = -1;
int io1Str = -1;
int io2Str = -1;
int io3Str = -1;
#ifdef __31DV200__
int io2Str = -1;
int io3Str = -1;
#endif
private:
struct sockaddr_in ctrlAddr;
bool gpio_init(struct _io dir, struct _io set, int data);
};

#endif // IO_H
