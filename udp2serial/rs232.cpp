#include "rs232.h"

rs232::rs232()
{

}

rs232::~rs232()
{
    delete tcp_sv;
    FD_CLR(dev232Fd,&readFd);
    FD_CLR(socket232,&readFd);
    close(dev232Fd);
    close(socket232);
    dev232Fd = -1;
    socket232 = -1;
}

void* rs232::pth_tcp_server(void *arg)
{
    rs232 * p_this = (rs232*)arg;
    p_this->tcp_sv->tcp_running(p_this->dev232Fd);

    return (void*)0;
}

bool rs232::iniRs232()
{
    open_port(global_data.port232,&dev232Fd);
    set_optBak(global_data.uartspeed232,global_data.nbit232,global_data.nstop232,   \
               global_data.event232[0],global_data.nVtime232,global_data.nVmin232,&dev232Fd);

    if(NJ_A335 == hd_ver) {
        uint value;
        Hi_SetReg(A335_13_4_MUX, 0x0);
        Hi_GetReg(A335_13_4_DIR, &value);
        value |= A335_13_4_SET;
        Hi_SetReg(A335_13_4_DIR, value);
        Hi_GetReg(A335_13_4_DATA, &value);
        value |= A335_13_4_SET;
        Hi_SetReg(A335_13_4_DATA, value);
    }

    FD_SET(dev232Fd,&readFd);
    if(dev232Fd >= maxFd)
        maxFd = dev232Fd + 1;
    socket232 = createUdp(global_data.sv232port);
    FD_SET(socket232,&readFd);
    if(socket232 >= maxFd)
        maxFd = socket232 + 1;
    if(hd_232_edc) {
        open_port("com2",&dev232Fd_edc);
        set_optBak(global_data.uartspeed232,global_data.nbit232,global_data.nstop232,   \
                   global_data.event232[0],global_data.nVtime232,global_data.nVmin232,&dev232Fd_edc);
        FD_SET(dev232Fd_edc,&readFd);
        if(dev232Fd_edc >= maxFd)
            maxFd = dev232Fd_edc + 1;
        socket232_edc = createUdp(16234);
        FD_SET(socket232_edc,&readFd);
        if(socket232_edc >= maxFd)
            maxFd = socket232_edc + 1;
    }
    else if(GZ16A_200_V1 == hd_ver) {
        open_port("com3", &dev232Fd_edc);
        set_optBak(global_data.uartspeed232,global_data.nbit232,global_data.nstop232,   \
                   global_data.event232[0],global_data.nVtime232,global_data.nVmin232,&dev232Fd_edc);
        FD_SET(dev232Fd_edc,&readFd);
        if(dev232Fd_edc >= maxFd)
            maxFd = dev232Fd_edc + 1;
    }
    else if(GZ36_200_V1 == hd_ver) {
        open_port("com1", &dev232Fd_edc);
        set_optBak(global_data.uartspeed232,global_data.nbit232,global_data.nstop232,   \
                   global_data.event232[0],global_data.nVtime232,global_data.nVmin232,&dev232Fd_edc);
        FD_SET(dev232Fd_edc,&readFd);
        if(dev232Fd_edc >= maxFd)
            maxFd = dev232Fd_edc + 1;
    }

    tcp_sv = new tcp_server();

    pthread_t tcp_pid = 0;
    if(global_data.tcp_en) {
        tcp_pid = CreateThread(pth_tcp_server, 99, SCHED_FIFO, true, this);
        if(tcp_pid == 0 ) {
            perror("create thread pth_tcp_server error");
        }
    }

    return true;
}

bool rs232::rcvData2uart()
{
    char tmpData[128] = {0};
    int rcvLen = 0;
    string str;
    socklen_t addrLen = 0;
    memset(tmpData,0,sizeof(tmpData));

    addrLen = sizeof(struct sockaddr_in);

    rcvLen = recvfrom(socket232,tmpData,MAXDATA,0,(struct sockaddr*)(&ctrlAddr),&addrLen);
    if(rcvLen > 0) {
#ifdef __DEBUG__
    print0x(tmpData,rcvLen);
#endif
        str = tmpData;
        if(string::npos != str.find(HEAD232_0)) {
            str = str.substr(strlen(HEAD232_0));
            write(dev232Fd, str.c_str(), rcvLen - strlen(HEAD232_0));
        }
        else if(string::npos != str.find(HEAD232_1)) {
            str = str.substr(strlen(HEAD232_1));
            write(dev232Fd_edc, str.c_str(), rcvLen - strlen(HEAD232_1));
        }
        else {
            write(dev232Fd,tmpData,rcvLen);
        }
    }
    else {
        COMMON_PRT("net not rcv 232data\n");
        return false;
    }
    return true;
}

bool rs232::rcvData2uart_edc()
{
    char tmpData[128] = {0};
    int rcvLen = 0;
    socklen_t addrLen = 0;
    memset(tmpData,0,sizeof(tmpData));

    addrLen = sizeof(struct sockaddr_in);

    rcvLen = recvfrom(socket232_edc,tmpData,MAXDATA,0,(struct sockaddr*)(&ctrlAddr),&addrLen);
    if(rcvLen > 0) {
#ifdef __DEBUG__
    print0x(tmpData,rcvLen);
#endif
        write(dev232Fd_edc,tmpData,rcvLen);
    }
    else {
        COMMON_PRT("net not rcv 232data\n");
        return false;
    }
    return true;
}


bool rs232::readUart2ctrl()
{
    char tmpData[256] = {0}, tmp[256];
    int rdLen = 0, sendLen = 0;
    memset(tmpData,0,sizeof(tmpData));

    rdLen = read(dev232Fd,tmpData,MAXDATA);
#ifdef __DEBUG__
    print0x(tmpData,rdLen);
#endif
    if(rdLen > 0) {
        if(GZ16A_200_V1 == hd_ver || GZ36_200_V1 == hd_ver) {
            memset(tmp, 0, sizeof(tmp));
            strncpy(tmp, tmpData, rdLen);
            strncpy(&tmpData[strlen(HEAD232_0)], tmp, rdLen);
            strncpy(tmpData,  HEAD232_0, strlen(HEAD232_0));
            rdLen += strlen(HEAD232_0);
        }
        if(global_data.tcp_en) {
            tcp_sv->tcp_send(tmpData, rdLen);
        }
        if((global_data.ctl232port != 0) && (strlen(global_data.ctlip) >= 7)) {
            ctrlAddr.sin_family = AF_INET;
            ctrlAddr.sin_port = htons(global_data.ctl232port);
            ctrlAddr.sin_addr.s_addr = inet_addr(global_data.ctlip);
            sendLen = sendto(socket232,tmpData,rdLen,0,(struct sockaddr*)&ctrlAddr,sizeof(struct sockaddr_in));
            if(sendLen > 0)
                return true;
            else {
                COMMON_PRT("send dev 232 data to ctrl error\n");
                return false;
            }
        }
        else {
            COMMON_PRT("UMP not set ctrl IP PORT\n");
            return true;
        }
    }
    else {
        COMMON_PRT("RS232 dev not read data\n");
        return false;
    }
    return true;
}

bool rs232::readUart2ctrl_edc()
{
    char tmpData[128] = {0}, tmp[256];
    int rdLen = 0, sendLen = 0;
    memset(tmpData,0,sizeof(tmpData));

    rdLen = read(dev232Fd_edc,tmpData,MAXDATA);
#ifdef __DEBUG__
    print0x(tmpData,rdLen);
#endif
    if(rdLen > 0) {
        if(GZ16A_200_V1 == hd_ver || GZ36_200_V1 == hd_ver) {
            memset(tmp, 0, sizeof(tmp));
            strncpy(tmp, tmpData, rdLen);
            strncpy(&tmpData[strlen(HEAD232_0)], tmp, rdLen);
            strncpy(tmpData,  HEAD232_1, strlen(HEAD232_1));
            rdLen += strlen(HEAD232_1);
        }
        if((global_data.ctl232port != 0) && (strlen(global_data.ctlip) >= 7)) {
            ctrlAddr.sin_family = AF_INET;
            ctrlAddr.sin_port = htons(global_data.ctl232port);
            ctrlAddr.sin_addr.s_addr = inet_addr(global_data.ctlip);
            if(GZ16A_200_V1 == hd_ver|| GZ36_200_V1 == hd_ver) {
                sendLen = sendto(socket232,tmpData,rdLen,0,(struct sockaddr*)&ctrlAddr,sizeof(struct sockaddr_in));
            }
            else {
                sendLen = sendto(socket232_edc,tmpData,rdLen,0,(struct sockaddr*)&ctrlAddr,sizeof(struct sockaddr_in));
            }

            if(sendLen > 0)
                return true;
            else {
                COMMON_PRT("send dev 232 data to ctrl error\n");
                return false;
            }
        }
        else {
            COMMON_PRT("UMP not set ctrl IP PORT\n");
            return true;
        }
    }
    else {
        COMMON_PRT("RS232 dev not read data\n");
        return false;
    }
    return true;
}

/*
 * 配合UMP设置，时时生效
 * 全局参数在外部修改，保存
 * */

bool rs232::UMPsetParam(bool uartFlag,bool ctrlPortFlag,bool svPortFlag)
{
    if(uartFlag) {
        COMMON_PRT("UMP set RS232 UART Param\n");
        FD_CLR(dev232Fd,&readFd);
        close(dev232Fd);
        sleep(1);

        open_port(global_data.port232,&dev232Fd);
        set_optBak(global_data.uartspeed232,global_data.nbit232,global_data.nstop232,   \
                   global_data.event232[0],global_data.nVtime232,global_data.nVmin232,&dev232Fd);
        FD_SET(dev232Fd,&readFd);
        if(dev232Fd >= maxFd)
            maxFd = dev232Fd + 1;
    }

    if(ctrlPortFlag) {
        COMMON_PRT("UMP set RS232 ctrl port Param\n");
    }

    if(svPortFlag) {
        FD_CLR(socket232,&readFd);
        close(socket232);
        sleep(1);

        socket232 = createUdp(global_data.sv232port);
        FD_SET(socket232,&readFd);
        if(socket232 >= maxFd)
            maxFd = socket232 + 1;
    }
    return true;
}
