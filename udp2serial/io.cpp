#include "io.h"

io::io()
{

}

io::~io()
{
    if(-1 != socketIO) {
        FD_CLR(socketIO,&readFd);
        close(socketIO);
        socketIO = -1;
    }
}

bool io::gpio_init(struct _io dir, struct _io set, int data)
{
    /*--- 先置方向引角 ---*/
    uint value;
    Hi_GetReg(dir.dir, &value);
    value |= dir.set;
    Hi_SetReg(dir.dir, value);

    Hi_GetReg(dir.data, &value);
    if(2 == data) {
        value |= dir.set;
    }
    else if(1 == data) {
        value &= (~dir.set);
    }
    Hi_SetReg(dir.data, value);

    /*--- 确定数据引脚 ---*/
    Hi_GetReg(set.dir, &value);
    if(2 == data) {
        value |= set.set;
    }
    else if(1 == data){
        value &= (~set.set);
    }
    Hi_SetReg(set.dir, value);

    return true;
}

bool io::ioIni(int ioNum)
{
    uint value;
    if((hd_ver == NJ_7000) || (hd_ver == PTN_7000)) {
        if(0 == ioNum) {
            Hi_SetReg(GPIO13_5MUX,0x0);

            Hi_GetReg(GPIO13_5DIR,&value);
            value |= GPIO13_5SET;
            Hi_SetReg(GPIO13_5DIR,value);

            if(1 == global_data.enIo_0) {         //IO作为输入
                Hi_GetReg(GPIO13_5DATA,&value);
                value &= (~GPIO13_5SET);
                Hi_SetReg(GPIO13_5DATA,value);

                Hi_GetReg(GPIO0_6DIR,&value);
                value &= (~GPIO0_6SET);
                Hi_SetReg(GPIO0_6DIR,value);
            }
            else if(2 == global_data.enIo_0) {    //IO作为输出
                Hi_GetReg(GPIO13_5DATA,&value);
                value |= GPIO13_5SET;
                Hi_SetReg(GPIO13_5DATA,value);

                Hi_GetReg(GPIO0_6DIR,&value);
                value |= GPIO0_6SET;
                Hi_SetReg(GPIO0_6DIR,value);

            }
            else {
                COMMON_PRT("io en value error\n");
                return false;
            }
        }

        if((1 == ioNum) && (0 == global_data.enInfrared)) {

            Hi_GetReg(GPIO0_4DIR,&value);
            value |= GPIO0_4SET;
            Hi_SetReg(GPIO0_4DIR,value);


            if(1 == global_data.enIo_1) {         //IO作为输入
                Hi_GetReg(GPIO0_4DATA,&value);
                value &= (~GPIO0_4SET);
                Hi_SetReg(GPIO0_4DATA,value);

                Hi_GetReg(GPIO0_3DIR,&value);
                value &= (~GPIO0_3SET);
                Hi_SetReg(GPIO0_3DIR,value);

            }
            else if(2 == global_data.enIo_1) {    //IO作为输出
                Hi_GetReg(GPIO0_4DATA,&value);
                value |= GPIO0_4SET;
                Hi_SetReg(GPIO0_4DATA,value);

                Hi_GetReg(GPIO0_3DIR,&value);
                value |= GPIO0_3SET;
                Hi_SetReg(GPIO0_3DIR,value);

            }
            else {
                COMMON_PRT("io en value error\n");
                return false;
            }
        }
    }
    else if(hd_ver == PTN_21A) {
        Hi_SetReg(IO_DIR_11_6_MUX, 0x0);

        Hi_GetReg(IO_DIR_11_6_DIR, &value);
        value |= IO_DIR_11_6_SET;
        Hi_SetReg(IO_DIR_11_6_DIR, value);

        Hi_SetReg(IO_DIR_11_3_MUX, 0x0);

        Hi_GetReg(IO_DIR_11_3_DIR, &value);
        value |= IO_DIR_11_3_SET;
        Hi_SetReg(IO_DIR_11_3_DIR, value);

        if(0 == ioNum) {
            Hi_SetReg(IO_0_10_2_MUX, 0x0);

            if(1 == global_data.enIo_0) {
                Hi_GetReg(IO_DIR_11_3_DATA, &value);
                value &= (~IO_DIR_11_3_SET);
                Hi_SetReg(IO_DIR_11_3_DATA, value);

                Hi_GetReg(IO_0_10_2_DIR, &value);
                value &= (~IO_0_10_2SET);
                Hi_SetReg(IO_0_10_2_DIR, value);
            }
            else if(2 == global_data.enIo_0) {
                Hi_GetReg(IO_DIR_11_3_DATA, &value);
                value |= IO_DIR_11_3_SET;
                Hi_SetReg(IO_DIR_11_3_DATA, value);

                Hi_GetReg(IO_0_10_2_DIR, &value);
                value |= IO_0_10_2SET;
                Hi_SetReg(IO_0_10_2_DIR, value);
            }
        }
        else if(1 == ioNum && 0 == global_data.enInfrared) {
            Hi_SetReg(IO_1_5_6_MUX, 0x1);
            if(1 == global_data.enIo_1) {
                Hi_GetReg(IO_DIR_11_6_DATA, &value);
                value &= (~IO_DIR_11_6_SET);
                Hi_SetReg(IO_DIR_11_6_DATA, value);

                Hi_GetReg(IO_1_5_6_DIR, &value);
                value &= (~IO_1_5_6_SET);
                Hi_SetReg(IO_1_5_6_DIR, value);
            }
            else if(2 == global_data.enIo_1) {
                Hi_GetReg(IO_DIR_11_6_DATA, &value);
                value |= IO_DIR_11_6_SET;
                Hi_SetReg(IO_DIR_11_6_DATA, value);

                Hi_GetReg(IO_1_5_6_DIR, &value);
                value |= IO_1_5_6_SET;
                Hi_SetReg(IO_1_5_6_DIR, value);
            }
        }

    }
    else if(hd_ver == GZ_700S) {
        if(0 == ioNum) {
            Hi_SetReg(GPIO11_2REG,0x1);

            Hi_GetReg(GPIO11_2DIR,&value);
            value |= GPIO11_2SET;
            Hi_SetReg(GPIO11_2DIR,value);

            if(1 == global_data.enIo_0) {         //IO作为输入

                Hi_GetReg(GPIO11_2DATA,&value);
                value &= (~GPIO11_2SET);
                Hi_SetReg(GPIO11_2DATA,value);

                Hi_GetReg(GPIO11_1DIR,&value);
                value &= (~GPIO11_1SET);
                Hi_SetReg(GPIO11_1DIR,value);
            }
            else if(2 == global_data.enIo_0) {    //IO作为输出
                Hi_GetReg(GPIO11_2DATA,&value);
                value |= GPIO11_2SET;
                Hi_SetReg(GPIO11_2DATA,value);

                Hi_GetReg(GPIO11_1DIR,&value);
                value |= GPIO11_1SET;
                Hi_SetReg(GPIO11_1DIR,value);

            }
            else {
                COMMON_PRT("io en value error\n");
                return false;
            }
        }

        if((1 == ioNum) && (0 == global_data.enInfrared)) {

            Hi_GetReg(GPIO11_3DIR,&value);
            value |= GPIO11_3SET;
            Hi_SetReg(GPIO11_3DIR,value);


            if(1 == global_data.enIo_1) {         //IO作为输入
                Hi_GetReg(GPIO11_3DATA,&value);
                value &= (~GPIO11_3SET);
                Hi_SetReg(GPIO11_3DATA,value);

                Hi_GetReg(GPIO14_7DIR,&value);
                value &= (~GPIO14_7SET);
                Hi_SetReg(GPIO14_7DIR,value);

            }
            else if(2 == global_data.enIo_1) {    //IO作为输出
                Hi_GetReg(GPIO11_3DATA,&value);
                value |= GPIO11_3SET;
                Hi_SetReg(GPIO11_3DATA,value);

                Hi_GetReg(GPIO14_7DIR,&value);
                value |= GPIO14_7SET;
                Hi_SetReg(GPIO14_7DIR,value);

            }
            else {
                COMMON_PRT("io en value error\n");
                return false;
            }
        }
    }
    else if(hd_ver == NJ_21D) {
        if(0 == ioNum) {
            Hi_GetReg(GPIO0_3DIR,&value);


            if(1 == global_data.enIo_0) {         //IO作为输入
                value &= (~GPIO0_3SET);
                Hi_SetReg(GPIO0_3DIR,value);
            }
            else if(2 == global_data.enIo_0) {    //IO作为输出
                value |= GPIO0_3SET;
                Hi_SetReg(GPIO0_3DIR,value);
            }
            else {
                COMMON_PRT("io en value error\n");
                return false;
            }
        }
        else {
            Hi_GetReg(GPIO0_5DIR,&value);
            if(1 == global_data.enIo_1) {         //IO作为输入
                value &= (~GPIO0_5SET);
                Hi_SetReg(GPIO0_5DIR,value);
                printf("gpio0_5:0x%x\n",value);
            }
            else if(2 == global_data.enIo_1) {    //IO作为输出
                value |= GPIO0_5SET;
                Hi_SetReg(GPIO0_5DIR,value);
            }
            else {
                COMMON_PRT("io en value error\n");
                return false;
            }

        }
    }
    else if((hd_ver == NJ_700K) || (hd_ver == GZ_700K) || (hd_ver == GZ_8000)|| (hd_ver == NJ_A156_V2)||(hd_ver == NJ_A241)) {
        if(1 == ioNum) {
            if(ir_nj_700k) {
                Hi_SetReg(GPIO15_6MUX,0x1);

                if(1 == global_data.enIo_1) {       //IO作为输入
                    Hi_GetReg(GPIO15_6DIR,&value);
                    value &= (~GPIO15_6SET);
                    Hi_SetReg(GPIO15_6DIR,value);
                }
                else if(2 == global_data.enIo_1) {  //IO作为输出
                    Hi_GetReg(GPIO15_6DIR,&value);
                    value |= GPIO15_6SET;
                    Hi_SetReg(GPIO15_6DIR,value);
                }

                Hi_SetReg(GPIO1_7MUX, 0x1);

                if(1 == global_data.enIo_1) {       //IO作为输入
                    Hi_GetReg(GPIO1_7DIR,&value);
                    value &= (~GPIO1_7SET);
                    Hi_SetReg(GPIO1_7DIR,value);
                }
                else if(2 == global_data.enIo_1) {  //IO作为输出
                    Hi_GetReg(GPIO1_7DIR,&value);
                    value |= GPIO1_7SET;
                    Hi_SetReg(GPIO1_7DIR,value);
                }
            }
            else {
                Hi_SetReg(GPIO1_5MUX,0x1);

                Hi_GetReg(GPIO1_5DIR,&value);
                value |= GPIO1_5SET;
                Hi_SetReg(GPIO1_5DIR,value);

                Hi_SetReg(GPIO20_5MUX,0x0);

                if(1 == global_data.enIo_1) {         //IO作为输入
                    Hi_GetReg(GPIO1_5DATA,&value);
                    value &= (~GPIO1_5SET);
                    Hi_SetReg(GPIO1_5DATA,value);

                    Hi_GetReg(GPIO20_5DIR,&value);
                    value &= (~GPIO20_5SET);
                    Hi_SetReg(GPIO20_5DIR,value);
                }
                else if(2 == global_data.enIo_1) {    //IO作为输出
                    Hi_GetReg(GPIO1_5DATA,&value);
                    value |= GPIO1_5SET;
                    Hi_SetReg(GPIO1_5DATA,value);

                    Hi_GetReg(GPIO20_5DIR,&value);
                    value |= GPIO20_5SET;
                    Hi_SetReg(GPIO20_5DIR,value);
                }
                else {
                    COMMON_PRT("io en value error\n");
                    return false;
                }
            }

        }
        if(0 == ioNum) {
            if(ir_nj_700k) {
                Hi_SetReg(GPIO20_5MUX,0x0);

                if(1 == global_data.enIo_0) {           //IO作为输入
                    Hi_GetReg(GPIO20_5DIR,&value);
                    value &= (~GPIO20_5SET);
                    Hi_SetReg(GPIO20_5DIR,value);
                }
                else if(2 == global_data.enIo_0) {      //IO作为输出
                    Hi_GetReg(GPIO20_5DIR,&value);
                    value |= GPIO20_5SET;
                    Hi_SetReg(GPIO20_5DIR,value);
                }
                else {
                    COMMON_PRT("io en value error\n");
                    return false;
                }
            }
            else {
                Hi_SetReg(GPIO1_6MUX,0x1);

                Hi_GetReg(GPIO1_6DIR,&value);
                value |= GPIO1_6SET;
                Hi_SetReg(GPIO1_6DIR,value);

                Hi_SetReg(GPIO1_7MUX,0x1);

                if(1 == global_data.enIo_0) {         //IO作为输入
                    Hi_GetReg(GPIO1_6DATA,&value);
                    value &= (~GPIO1_6SET);
                    Hi_SetReg(GPIO1_6DATA,value);

                    Hi_GetReg(GPIO1_7DIR,&value);
                    value &= (~GPIO1_7SET);
                    Hi_SetReg(GPIO1_7DIR,value);
                }
                else if(2 == global_data.enIo_0) {    //IO作为输出
                    Hi_GetReg(GPIO1_6DATA,&value);
                    value |= GPIO1_6SET;
                    Hi_SetReg(GPIO1_6DATA,value);

                    Hi_GetReg(GPIO1_7DIR,&value);
                    value |= GPIO1_7SET;
                    Hi_SetReg(GPIO1_7DIR,value);
                }
                else {
                    COMMON_PRT("io en value error\n");
                    return false;
                }
            }

        }
    }
    else if(hd_ver == NJ_D_700K) {

    }
    else if(hd_ver == NJ_31DV200) {
        if(0 == ioNum) {
            Hi_GetReg(IO_0_DIR_REG,&value);
            value &= (~0x1);
            Hi_SetReg(IO_0_DIR_REG, value);

            Hi_GetReg(IO_0_DIR_DIR, &value);
            value |= IO_0_DIR_SET;
            Hi_SetReg(IO_0_DIR_DIR, value);

            Hi_GetReg(IO_0_DATA_REG, &value);
            value &= (~0x1);
            Hi_SetReg(IO_0_DATA_REG, value);

            if(1 == global_data.enIo_0) {
                Hi_GetReg(IO_0_DIR_DATA, &value);
                value &= (~IO_0_DIR_SET);
                Hi_SetReg(IO_0_DIR_DATA, value);

                Hi_GetReg(IO_0_DATA_DIR, &value);
                value &= (~IO_0_DATA_SET);
                Hi_SetReg(IO_0_DATA_DIR, value);
            }
            else if(2 ==  global_data.enIo_0) {
                Hi_GetReg(IO_0_DIR_DATA, &value);
                value |= IO_0_DIR_SET;
                Hi_SetReg(IO_0_DIR_DATA, value);

                Hi_GetReg(IO_0_DATA_DIR, &value);
                value |= IO_0_DATA_SET;
                Hi_SetReg(IO_0_DATA_DIR, value);
            }
        }
        else if(1 == ioNum) {
            Hi_GetReg(IO_1_DIR_REG,&value);
            value &= (~0x1);
            Hi_SetReg(IO_1_DIR_REG, value);

            Hi_GetReg(IO_1_DIR_DIR, &value);
            value |= IO_1_DIR_SET;
            Hi_SetReg(IO_1_DIR_DIR, value);

            Hi_GetReg(IO_1_DATA_REG, &value);
            value &= (~0x1);
            Hi_SetReg(IO_1_DATA_REG, value);

            if(1 == global_data.enIo_0) {
                Hi_GetReg(IO_1_DIR_DATA, &value);
                value &= (~IO_1_DIR_SET);
                Hi_SetReg(IO_1_DIR_DATA, value);

                Hi_GetReg(IO_1_DATA_DIR, &value);
                value &= (~IO_1_DATA_SET);
                Hi_SetReg(IO_1_DATA_DIR, value);
            }
            else if(2 ==  global_data.enIo_0) {
                Hi_GetReg(IO_1_DIR_DATA, &value);
                value |= IO_1_DIR_SET;
                Hi_SetReg(IO_1_DIR_DATA, value);

                Hi_GetReg(IO_1_DATA_DIR, &value);
                value |= IO_1_DATA_SET;
                Hi_SetReg(IO_1_DATA_DIR, value);
            }

        }
        else if(2 == ioNum) {
            Hi_GetReg(IO_2_DIR_REG,&value);
            value &= (~0x1);
            Hi_SetReg(IO_2_DIR_REG, value);

            Hi_GetReg(IO_2_DIR_DIR, &value);
            value |= IO_2_DIR_SET;
            Hi_SetReg(IO_2_DIR_DIR, value);

            Hi_GetReg(IO_2_DATA_REG, &value);
            value &= (~0x1);
            Hi_SetReg(IO_2_DATA_REG, value);

            if(1 == global_data.enIo_0) {
                Hi_GetReg(IO_2_DIR_DATA, &value);
                value &= (~IO_2_DIR_SET);
                Hi_SetReg(IO_2_DIR_DATA, value);

                Hi_GetReg(IO_2_DATA_DIR, &value);
                value &= (~IO_2_DATA_SET);
                Hi_SetReg(IO_2_DATA_DIR, value);
            }
            else if(2 ==  global_data.enIo_0) {
                Hi_GetReg(IO_2_DIR_DATA, &value);
                value |= IO_2_DIR_SET;
                Hi_SetReg(IO_2_DIR_DATA, value);

                Hi_GetReg(IO_2_DATA_DIR, &value);
                value |= IO_2_DATA_SET;
                Hi_SetReg(IO_2_DATA_DIR, value);
            }

        }
        else if(3 ==  ioNum) {
            Hi_GetReg(IO_3_DIR_REG,&value);
            value &= (~0x1);
            Hi_SetReg(IO_3_DIR_REG, value);

            Hi_GetReg(IO_3_DIR_DIR, &value);
            value |= IO_3_DIR_SET;
            Hi_SetReg(IO_3_DIR_DIR, value);

            Hi_GetReg(IO_3_DATA_REG, &value);
            value |= 0x1;
            Hi_SetReg(IO_3_DATA_REG, value);

            if(1 == global_data.enIo_0) {
                Hi_GetReg(IO_3_DIR_DATA, &value);
                value &= (~IO_3_DIR_SET);
                Hi_SetReg(IO_3_DIR_DATA, value);

                Hi_GetReg(IO_3_DATA_DIR, &value);
                value &= (~IO_3_DATA_SET);
                Hi_SetReg(IO_3_DATA_DIR, value);
            }
            else if(2 ==  global_data.enIo_0) {
                Hi_GetReg(IO_3_DIR_DATA, &value);
                value |= IO_3_DIR_SET;
                Hi_SetReg(IO_3_DIR_DATA, value);

                Hi_GetReg(IO_3_DATA_DIR, &value);
                value |= IO_3_DATA_SET;
                Hi_SetReg(IO_3_DATA_DIR, value);
            }

        }
    }
    else if(hd_ver == NJ_A304) {
        if(0 == ioNum) {
            Hi_GetReg(A304_IO_0_DIR_REG,&value);
            value &= (~0x1);
            Hi_SetReg(A304_IO_0_DIR_REG, value);

            Hi_GetReg(A304_IO_0_DIR_DIR, &value);
            value |= A304_IO_0_DIR_SET;
            Hi_SetReg(A304_IO_0_DIR_DIR, value);

            Hi_GetReg(A304_IO_0_DATA_REG, &value);
            value &= (~0x1);
            Hi_SetReg(A304_IO_0_DATA_REG, value);

            if(1 == global_data.enIo_0) {
                Hi_GetReg(A304_IO_0_DIR_DATA, &value);
                value &= (~A304_IO_0_DIR_SET);
                Hi_SetReg(A304_IO_0_DIR_DATA, value);

                Hi_GetReg(A304_IO_0_DATA_DIR, &value);
                value &= (~A304_IO_0_DATA_SET);
                Hi_SetReg(A304_IO_0_DATA_DIR, value);
            }
            else if(2 ==  global_data.enIo_0) {
                Hi_GetReg(A304_IO_0_DIR_DATA, &value);
                value |= A304_IO_0_DIR_SET;
                Hi_SetReg(A304_IO_0_DIR_DATA, value);

                Hi_GetReg(A304_IO_0_DATA_DIR, &value);
                value |= A304_IO_0_DATA_SET;
                Hi_SetReg(A304_IO_0_DATA_DIR, value);
            }
        }
        else if(1 == ioNum) {
            Hi_GetReg(A304_IO_1_DIR_REG,&value);
            value |= (0x3);
            Hi_SetReg(A304_IO_1_DIR_REG, value);

            Hi_GetReg(A304_IO_1_DIR_DIR, &value);
            value |= A304_IO_1_DIR_SET;
            Hi_SetReg(A304_IO_1_DIR_DIR, value);

            Hi_GetReg(A304_IO_1_DATA_REG, &value);
            value |= (0x3);
            Hi_SetReg(A304_IO_1_DATA_REG, value);

            if(1 == global_data.enIo_1) {
                Hi_GetReg(A304_IO_1_DIR_DATA, &value);
                value &= (~A304_IO_1_DIR_SET);
                Hi_SetReg(A304_IO_1_DIR_DATA, value);

                Hi_GetReg(A304_IO_1_DATA_DIR, &value);
                value &= (~A304_IO_1_DATA_SET);
                Hi_SetReg(A304_IO_1_DATA_DIR, value);
            }
            else if(2 ==  global_data.enIo_1) {
                Hi_GetReg(A304_IO_1_DIR_DATA, &value);
                value |= A304_IO_1_DIR_SET;
                Hi_SetReg(A304_IO_1_DIR_DATA, value);

                Hi_GetReg(A304_IO_1_DATA_DIR, &value);
                value |= A304_IO_1_DATA_SET;
                Hi_SetReg(A304_IO_1_DATA_DIR, value);
            }

        }
        #ifdef __31DV200__
        else if(2 == ioNum) {
            Hi_GetReg(A304_IO_2_DIR_REG,&value);
            value |= (0x3);
            Hi_SetReg(A304_IO_2_DIR_REG, value);

            Hi_GetReg(A304_IO_2_DIR_DIR, &value);
            value |= A304_IO_2_DIR_SET;
            Hi_SetReg(A304_IO_2_DIR_DIR, value);

            Hi_GetReg(A304_IO_2_DATA_REG, &value);
            value |= (0x3);
            Hi_SetReg(A304_IO_2_DATA_REG, value);

            if(1 == global_data.enIo_2) {
                Hi_GetReg(A304_IO_2_DIR_DATA, &value);
                value &= (~A304_IO_2_DIR_SET);
                Hi_SetReg(A304_IO_2_DIR_DATA, value);

                Hi_GetReg(A304_IO_2_DATA_DIR, &value);
                value &= (~A304_IO_2_DATA_SET);
                Hi_SetReg(A304_IO_2_DATA_DIR, value);
            }
            else if(2 ==  global_data.enIo_2) {
                Hi_GetReg(A304_IO_2_DIR_DATA, &value);
                value |= A304_IO_2_DIR_SET;
                Hi_SetReg(A304_IO_2_DIR_DATA, value);

                Hi_GetReg(A304_IO_2_DATA_DIR, &value);
                value |= A304_IO_2_DATA_SET;
                Hi_SetReg(A304_IO_2_DATA_DIR, value);
            }

        }
        else if(3 ==  ioNum) {
            Hi_GetReg(A304_IO_3_DIR_REG,&value);
            value &= (~0x1);
            Hi_SetReg(A304_IO_3_DIR_REG, value);

            Hi_GetReg(A304_IO_3_DIR_DIR, &value);
            value |= A304_IO_3_DIR_SET;
            Hi_SetReg(A304_IO_3_DIR_DIR, value);

            Hi_GetReg(A304_IO_3_DATA_REG, &value);
            value |= (0x3);
            Hi_SetReg(A304_IO_3_DATA_REG, value);

            if(1 == global_data.enIo_3) {
                Hi_GetReg(A304_IO_3_DIR_DATA, &value);
                value &= (~A304_IO_3_DIR_SET);
                Hi_SetReg(A304_IO_3_DIR_DATA, value);

                Hi_GetReg(A304_IO_3_DATA_DIR, &value);
                value &= (~A304_IO_3_DATA_SET);
                Hi_SetReg(A304_IO_3_DATA_DIR, value);
            }
            else if(2 ==  global_data.enIo_3) {
                Hi_GetReg(A304_IO_3_DIR_DATA, &value);
                value |= A304_IO_3_DIR_SET;
                Hi_SetReg(A304_IO_3_DIR_DATA, value);

                Hi_GetReg(A304_IO_3_DATA_DIR, &value);
                value |= A304_IO_3_DATA_SET;
                Hi_SetReg(A304_IO_3_DATA_DIR, value);
            }

        }
        #endif
    }
    else if(hd_ver == GZ_31DV200) {
        if(0 == ioNum) {
            Hi_GetReg(IO_4_DIR_REG,&value);
            value |= (0x3);
            Hi_SetReg(IO_4_DIR_REG, value);  //设置DIR为io模式

            Hi_GetReg(IO_4_DIR_DIR, &value);
            value |= IO_4_DIR_SET;
            Hi_SetReg(IO_4_DIR_DIR, value); //设置控制方向的io(DIR)方向为输出，写入数据来控制整体方向

            Hi_GetReg(IO_4_DATA_REG, &value);
            value &= (~0x1);
            Hi_SetReg(IO_4_DATA_REG, value);//设置红外数据DATA接口为IO模式

            if(1 == global_data.enIo_0) {    //接收
                Hi_GetReg(IO_4_DIR_DATA, &value);
                value &= (~IO_4_DIR_SET);
                Hi_SetReg(IO_4_DIR_DATA, value); //DIR拉低 gpio19_2写0 设置整体方向为输入

                Hi_GetReg(IO_4_DATA_DIR, &value);
                value &= (~IO_4_DATA_SET);
                Hi_SetReg(IO_4_DATA_DIR, value);//设置红外数据DATA接口方向为输入 ，可读取此时红外数据口gpio1_7数据
            }
            else if(2 ==  global_data.enIo_0) {  //发送  可写状态
                Hi_GetReg(IO_4_DIR_DATA, &value);
                value |= IO_4_DIR_SET;
                Hi_SetReg(IO_4_DIR_DATA, value);//DIR拉高 gpio19_2写1 设置整体方向为输出

                Hi_GetReg(IO_4_DATA_DIR, &value);
                value |= IO_4_DATA_SET;
                Hi_SetReg(IO_4_DATA_DIR, value);//设置红外数据DATA接口方向为输出，可向红外数据口gpio1_7写数据
            }
        }
        else if(1 == ioNum) {
            Hi_GetReg(IO_5_DIR_REG,&value);
            value |= (0x3);
            Hi_SetReg(IO_5_DIR_REG, value);

            Hi_GetReg(IO_5_DIR_DIR, &value);
            value |= IO_5_DIR_SET;
            Hi_SetReg(IO_5_DIR_DIR, value);

            Hi_GetReg(IO_5_DATA_REG, &value);
            value |= (0x3);
            Hi_SetReg(IO_5_DATA_REG, value);

            if(1 == global_data.enIo_1) {          //这里应该是enio_1
                Hi_GetReg(IO_5_DIR_DATA, &value);
                value &= (~IO_5_DIR_SET);
                Hi_SetReg(IO_5_DIR_DATA, value);

                Hi_GetReg(IO_5_DATA_DIR, &value);
                value &= (~IO_5_DATA_SET);
                Hi_SetReg(IO_5_DATA_DIR, value);
            }
            else if(2 ==  global_data.enIo_1) {
                Hi_GetReg(IO_5_DIR_DATA, &value);
                value |= IO_5_DIR_SET;
                Hi_SetReg(IO_5_DIR_DATA, value);

                Hi_GetReg(IO_5_DATA_DIR, &value);
                value |= IO_5_DATA_SET;
                Hi_SetReg(IO_5_DATA_DIR, value);
            }

        }

    }
    else if(hd_ver == NJ_A151) {
        if(0 == ioNum) {
            if(outmode)
            {
                Hi_GetReg(A151OUT_IO_0_DIR_REG,&value);
                value &= (~0x1);
                Hi_SetReg(A151OUT_IO_0_DIR_REG, value);

                Hi_GetReg(A151OUT_IO_0_DIR_DIR, &value);
                value |= A151OUT_IO_0_DIR_SET;
                Hi_SetReg(A151OUT_IO_0_DIR_DIR, value);

                Hi_GetReg(A151OUT_IO_0_DATA_REG, &value);
                value &= (~0x1);
                Hi_SetReg(A151OUT_IO_0_DATA_REG, value);//设置IO 0 DATA接口为IO模式
            }
            else
            {
                Hi_GetReg(A151_IO_0_DIR_REG,&value);
                value &= (~0x1);
                Hi_SetReg(A151_IO_0_DIR_REG, value);  //设置DIR为io模式

                Hi_GetReg(A151_IO_0_DIR_DIR, &value);
                value |= A151_IO_0_DIR_SET;
                Hi_SetReg(A151_IO_0_DIR_DIR, value); //设置控制方向的io(DIR)方向为输出，写入数据来控制整体方向
             }


//            Hi_GetReg(IO_4_DATA_REG, &value);
//            value &= (~0x1);
//            Hi_SetReg(IO_4_DATA_REG, value);//设置红外数据DATA接口为IO模式

            if(1 == global_data.enIo_0) {    //接收
                if(outmode)
                {
                    Hi_GetReg(A151OUT_IO_0_DIR_DATA, &value);
                    value &= (~A151OUT_IO_0_DIR_SET);
                    Hi_SetReg(A151OUT_IO_0_DIR_DATA, value);

                    Hi_GetReg(A151OUT_IO_0_DATA_DIR, &value);
                    value &= (~A151OUT_IO_0_DATA_SET);
                    Hi_SetReg(A151OUT_IO_0_DATA_DIR, value);//设置io 0数据口方向为输入 可读取此时io 0口gpio9_4数据
                }
                else{
                    Hi_GetReg(A151_IO_0_DIR_DATA, &value);
                    value &= (~A151_IO_0_DIR_SET);
                    Hi_SetReg(A151_IO_0_DIR_DATA, value); //DIR拉低 写0 设置整体方向为输入

                    Hi_GetReg(A151_IO_0_DATA_DIR, &value);
                    value &= (~A151_IO_0_DATA_SET);
                    Hi_SetReg(A151_IO_0_DATA_DIR, value);//设置io 0数据口方向为输入 ，可读取此时io 0口gpio0_5数据
                }

            }
            else if(2 ==  global_data.enIo_0) {  //发送  可写状态
                if(outmode)
                {
                    Hi_GetReg(A151OUT_IO_0_DIR_DATA, &value);
                    value |= A151OUT_IO_0_DIR_SET;
                    Hi_SetReg(A151OUT_IO_0_DIR_DATA, value);//DIR拉高 写1 设置整体方向为输出

                    Hi_GetReg(A151OUT_IO_0_DATA_DIR, &value);
                    value |= A151OUT_IO_0_DATA_SET;
                    Hi_SetReg(A151OUT_IO_0_DATA_DIR, value);//设置io 0口方向为输出，可向io 0口gpio9_4写数据
                }
                else{
                    Hi_GetReg(A151_IO_0_DIR_DATA, &value);
                    value |= A151_IO_0_DIR_SET;
                    Hi_SetReg(A151_IO_0_DIR_DATA, value);

                    Hi_GetReg(A151_IO_0_DATA_DIR, &value);
                    value |= A151_IO_0_DATA_SET;
                    Hi_SetReg(A151_IO_0_DATA_DIR, value);//设置io 0口方向为输出，可向io 0口gpio0_5写数据
                    }
            }
        }
        else if(1 == ioNum) {
            if(outmode)
            {
                Hi_GetReg(A151OUT_IO_1_DIR_REG,&value);
                value &= (~0x1);
                Hi_SetReg(A151OUT_IO_1_DIR_REG, value);

                Hi_GetReg(A151OUT_IO_1_DIR_DIR, &value);
                value |= A151OUT_IO_1_DIR_SET;
                Hi_SetReg(A151OUT_IO_1_DIR_DIR, value);

                Hi_GetReg(A151OUT_IO_1_DATA_REG, &value);
                value &= (~0x1);
                Hi_SetReg(A151OUT_IO_1_DATA_REG, value);//设置IO 1 DATA接口为IO模式
            }
            else
            {
                Hi_GetReg(A151_IO_1_DIR_REG,&value);
                value &= (~0x1);
                Hi_SetReg(A151_IO_1_DIR_REG, value);  //设置DIR为io模式

                Hi_GetReg(A151_IO_1_DIR_DIR, &value);
                value |= A151_IO_1_DIR_SET;
                Hi_SetReg(A151_IO_1_DIR_DIR, value); //设置控制方向的io(DIR)方向为输出，写入数据来控制整体方向
             }


//            Hi_GetReg(IO_5_DATA_REG, &value);
//            value |= (0x3);
//            Hi_SetReg(IO_5_DATA_REG, value);

            if(1 == global_data.enIo_1) {
                if(outmode)
                {
                    Hi_GetReg(A151OUT_IO_1_DIR_DATA, &value);
                    value &= (~A151OUT_IO_1_DIR_SET);
                    Hi_SetReg(A151OUT_IO_1_DIR_DATA, value);

                    Hi_GetReg(A151OUT_IO_1_DATA_DIR, &value);
                    value &= (~A151OUT_IO_1_DATA_SET);
                    Hi_SetReg(A151OUT_IO_1_DATA_DIR, value);
                }
                else
                {
                    Hi_GetReg(A151_IO_1_DIR_DATA, &value);
                    value &= (~A151_IO_1_DIR_SET);
                    Hi_SetReg(A151_IO_1_DIR_DATA, value);

                    Hi_GetReg(A151_IO_1_DATA_DIR, &value);
                    value &= (~A151_IO_1_DATA_SET);
                    Hi_SetReg(A151_IO_1_DATA_DIR, value);
                 }


            }
            else if(2 ==  global_data.enIo_1) {
                if(outmode)
                {
                    Hi_GetReg(A151OUT_IO_1_DIR_DATA, &value);
                    value |= A151OUT_IO_1_DIR_SET;
                    Hi_SetReg(A151OUT_IO_1_DIR_DATA, value);//DIR拉高 写1 设置整体方向为输出


                    Hi_GetReg(A151OUT_IO_1_DATA_DIR, &value);
                    value |= A151OUT_IO_1_DATA_SET;
                    Hi_SetReg(A151OUT_IO_1_DATA_DIR, value);
                }
                else
                {
                    Hi_GetReg(A151_IO_1_DIR_DATA, &value);
                    value |= A151_IO_1_DIR_SET;
                    Hi_SetReg(A151_IO_1_DIR_DATA, value);

                    Hi_GetReg(A151_IO_1_DATA_DIR, &value);
                    value |= A151_IO_1_DATA_SET;
                    Hi_SetReg(A151_IO_1_DATA_DIR, value);
                }


            }

        }

    }
    else if(hd_ver == NJ_A323) {
        if(0 == ioNum) {
            if(outmode)
            {
                Hi_GetReg(A323_IO_0_DIR_DIR, &value);
                value |= A323_IO_0_DIR_SET;
                Hi_SetReg(A323_IO_0_DIR_DIR, value);
            }
            else
            {
                Hi_GetReg(A151_IO_0_DIR_REG,&value);
                value &= (~0x1);
                Hi_SetReg(A151_IO_0_DIR_REG, value);  //设置DIR为io模式

                Hi_GetReg(A151_IO_0_DIR_DIR, &value);
                value |= A151_IO_0_DIR_SET;
                Hi_SetReg(A151_IO_0_DIR_DIR, value); //设置控制方向的io(DIR)方向为输出，写入数据来控制整体方向
             }


            if(1 == global_data.enIo_0) {    //接收
                if(outmode)
                {
                    Hi_GetReg(A323_IO_0_DIR_DATA, &value);
                    value &= (~A323_IO_0_DIR_SET);
                    Hi_SetReg(A323_IO_0_DIR_DATA, value); //DIR拉低 写0 设置整体方向为输入

                    Hi_GetReg(A323_IO_0_DATA_DIR, &value);
                    value &= (~A323_IO_0_DATA_SET);
                    Hi_SetReg(A323_IO_0_DATA_DIR, value);//设置io 0数据口方向为输入 ，可读取此时io 0口gpio0_5数据
                }
                else{
                    Hi_GetReg(A151_IO_0_DIR_DATA, &value);
                    value &= (~A151_IO_0_DIR_SET);
                    Hi_SetReg(A151_IO_0_DIR_DATA, value); //DIR拉低 写0 设置整体方向为输入

                    Hi_GetReg(A151_IO_0_DATA_DIR, &value);
                    value &= (~A151_IO_0_DATA_SET);
                    Hi_SetReg(A151_IO_0_DATA_DIR, value);//设置io 0数据口方向为输入 ，可读取此时io 0口gpio0_5数据
                }

            }
            else if(2 ==  global_data.enIo_0) {  //发送  可写状态
                if(outmode)
                {
                    Hi_GetReg(A323_IO_0_DIR_DATA, &value);
                    value |= A323_IO_0_DIR_SET;
                    Hi_SetReg(A323_IO_0_DIR_DATA, value);

                    Hi_GetReg(A323_IO_0_DATA_DIR, &value);
                    value |= A323_IO_0_DATA_SET;
                    Hi_SetReg(A323_IO_0_DATA_DIR, value);//设置io 0口方向为输出，可向io 0口gpio0_5写数据
                }
                else{
                    Hi_GetReg(A151_IO_0_DIR_DATA, &value);
                    value |= A151_IO_0_DIR_SET;
                    Hi_SetReg(A151_IO_0_DIR_DATA, value);

                    Hi_GetReg(A151_IO_0_DATA_DIR, &value);
                    value |= A151_IO_0_DATA_SET;
                    Hi_SetReg(A151_IO_0_DATA_DIR, value);//设置io 0口方向为输出，可向io 0口gpio0_5写数据
                    }
            }
        }
        else if(1 == ioNum) {
            if(outmode)
            {
                //无dir操作
            }
            else
            {
                Hi_GetReg(A151_IO_1_DIR_REG,&value);
                value &= (~0x1);
                Hi_SetReg(A151_IO_1_DIR_REG, value);  //设置DIR为io模式

                Hi_GetReg(A151_IO_1_DIR_DIR, &value);
                value |= A151_IO_1_DIR_SET;
                Hi_SetReg(A151_IO_1_DIR_DIR, value); //设置控制方向的io(DIR)方向为输出，写入数据来控制整体方向
             }


            if(1 == global_data.enIo_1) {
                if(outmode)
                {
                    Hi_GetReg(A323_IO_1_DATA_DIR, &value); //加电阻后默认方向输出 只操作data
                    value &= (~A323_IO_1_DATA_SET);
                    Hi_SetReg(A323_IO_1_DATA_DIR, value);
                }
                else
                {
                    Hi_GetReg(A151_IO_1_DIR_DATA, &value);
                    value &= (~A151_IO_1_DIR_SET);
                    Hi_SetReg(A151_IO_1_DIR_DATA, value);

                    Hi_GetReg(A151_IO_1_DATA_DIR, &value);
                    value &= (~A151_IO_1_DATA_SET);
                    Hi_SetReg(A151_IO_1_DATA_DIR, value);
                 }


            }
            else if(2 ==  global_data.enIo_1) {
                if(outmode)
                {
                    Hi_GetReg(A323_IO_1_DATA_DIR, &value);
                    value |= A323_IO_1_DATA_SET;
                    Hi_SetReg(A323_IO_1_DATA_DIR, value);
                }
                else
                {
                    Hi_GetReg(A151_IO_1_DIR_DATA, &value);
                    value |= A151_IO_1_DIR_SET;
                    Hi_SetReg(A151_IO_1_DIR_DATA, value);

                    Hi_GetReg(A151_IO_1_DATA_DIR, &value);
                    value |= A151_IO_1_DATA_SET;
                    Hi_SetReg(A151_IO_1_DATA_DIR, value);
                }


            }

        }

    }
    else if(hd_ver == NJ_A345) {         //IO0 dir:0_3 set:6_5; IO1/IR dir:0_2 set:6_7
        if(0 == ioNum) {
            io_0_dir.dir = 0x12150400;
            io_0_dir.data = 0x12150020;
            io_0_dir.set = 0x8;

            /*--- 先置方向引角 ---*/
            Hi_GetReg(io_0_dir.dir, &value);
            value |= io_0_dir.set;
            Hi_SetReg(io_0_dir.dir, value);

            Hi_GetReg(io_0_dir.data, &value);
            if(2 == global_data.enIo_0) {
                value |= io_0_dir.set;
            }
            else if(1 == global_data.enIo_0) {
                value &= (~io_0_dir.set);
            }
            Hi_SetReg(io_0_dir.data, value);

            /*--- 确定数据引脚 ---*/
            io_0_set.mux = 0x120F00F8;
            io_0_set.dir = 0x121B0400;
            io_0_set.data = 0x121B0080;
            io_0_set.set = 0x20;

            Hi_SetReg(io_0_set.mux, 0x0);
            Hi_GetReg(io_0_set.dir, &value);
            if(2 == global_data.enIo_0) {
                value |= io_0_set.set;
            }
            else if(1 == global_data.enIo_0){
                value &= (~io_0_set.set);
            }
            Hi_SetReg(io_0_set.dir, value);
        }
        else if(1 == ioNum) {
            io_1_dir.dir = 0x12150400;
            io_1_dir.data = 0x12150010;
            io_1_dir.set = 0x4;

            /*--- 先置方向引角 ---*/
            Hi_GetReg(io_1_dir.dir, &value);
            value |= io_1_dir.set;
            Hi_SetReg(io_1_dir.dir, value);

            Hi_GetReg(io_1_dir.data, &value);
            if(2 == global_data.enIo_1) {
                value |= io_1_dir.set;
            }
            else if(1 == global_data.enIo_1){
                value &= (~io_1_dir.set);
            }
            Hi_SetReg(io_1_dir.data, value);

            /*--- 确定数据引脚 ---*/
            io_1_set.mux = 0x120F00FC;
            io_1_set.dir = 0x121B0400;
            io_1_set.data = 0x121B0200;
            io_1_set.set = 0x80;

            Hi_SetReg(io_1_set.mux, 0x0);
            Hi_GetReg(io_1_set.dir, &value);
            if(2 == global_data.enIo_1) {
                value |= io_1_set.set;
            }
            else if(1 == global_data.enIo_1){
                value &= (~io_1_set.set);
            }
            Hi_SetReg(io_1_set.dir, value);
        }
    }
#if 0
    else if(hd_ver == NJ_A323) {
        if(0 == ioNum) {

                Hi_GetReg(A323_IO_0_DIR_DIR, &value);
                value |= A323_IO_0_DIR_SET;
                Hi_SetReg(A323_IO_0_DIR_DIR, value);


            if(1 == global_data.enIo_0) {    //接收


                    Hi_GetReg(A323_IO_0_DIR_DATA, &value);
                    value &= (~A323_IO_0_DIR_SET);
                    Hi_SetReg(A323_IO_0_DIR_DATA, value); //DIR拉低 写0 设置整体方向为输入

                    Hi_GetReg(A323_IO_0_DATA_DIR, &value);
                    value &= (~A323_IO_0_DATA_SET);
                    Hi_SetReg(A323_IO_0_DATA_DIR, value);//设置io 0数据口方向为输入 ，可读取此时io 0口gpio0_5数据


            }
            else if(2 ==  global_data.enIo_0) {  //发送  可写状态


                    Hi_GetReg(A323_IO_0_DIR_DATA, &value);
                    value |= A323_IO_0_DIR_SET;
                    Hi_SetReg(A323_IO_0_DIR_DATA, value);

                    Hi_GetReg(A323_IO_0_DATA_DIR, &value);
                    value |= A323_IO_0_DATA_SET;
                    Hi_SetReg(A323_IO_0_DATA_DIR, value);//设置io 0口方向为输出，可向io 0口gpio0_5写数据

            }
        }
        else if(1 == ioNum) {



            if(1 == global_data.enIo_1) {


//                    Hi_GetReg(A151_IO_1_DIR_DATA, &value);
//                    value &= (~A151_IO_1_DIR_SET);
//                    Hi_SetReg(A151_IO_1_DIR_DATA, value);

                    Hi_GetReg(A323_IO_1_DATA_DIR, &value);
                    value &= (~A323_IO_1_DATA_SET);
                    Hi_SetReg(A323_IO_1_DATA_DIR, value);



            }
            else if(2 ==  global_data.enIo_1) {


//                    Hi_GetReg(A151_IO_1_DIR_DATA, &value);
//                    value |= A151_IO_1_DIR_SET;
//                    Hi_SetReg(A151_IO_1_DIR_DATA, value);

                    Hi_GetReg(A323_IO_1_DATA_DIR, &value);
                    value |= A323_IO_1_DATA_SET;
                    Hi_SetReg(A323_IO_1_DATA_DIR, value);



            }

        }

    }
#endif

    else if(hd_ver == NJ_A186_V1) {
        //Hi_SetReg(IO0_DATA_A186_MUX, 0x0);

        Hi_GetReg(IO0_DIR_A186_DIR, &value);
        value |= IO0_DIR_A186_SET;
        Hi_SetReg(IO0_DIR_A186_DIR, value);

        //Hi_SetReg(IO_DIR_11_3_MUX, 0x0);

        Hi_GetReg(IO1_DIR_A186_DIR, &value);
        value |= IO1_DIR_A186_SET;
        Hi_SetReg(IO1_DIR_A186_DIR, value);

        if(0 == ioNum) {
            Hi_SetReg(IO0_DATA_A186_MUX, 0x0);

            if(1 == global_data.enIo_0) {
                Hi_GetReg(IO0_DIR_A186_DATA, &value);
                value &= (~IO0_DIR_A186_SET);
                Hi_SetReg(IO0_DIR_A186_DATA, value);

                Hi_GetReg(IO0_DATA_A186_DIR, &value);
                value &= (~IO0_DATA_A186_SET);
                Hi_SetReg(IO0_DATA_A186_DIR, value);
            }
            else if(2 == global_data.enIo_0) {
                Hi_GetReg(IO0_DIR_A186_DATA, &value);
                value |= IO0_DIR_A186_SET;
                Hi_SetReg(IO0_DIR_A186_DATA, value);

                Hi_GetReg(IO0_DATA_A186_DIR, &value);
                value |= IO0_DATA_A186_SET;
                Hi_SetReg(IO0_DATA_A186_DIR, value);
            }
        }
        else if(1 == ioNum ) {
            //&& 0 == global_data.enInfrared
            if(1 == global_data.enIo_1) {
                Hi_GetReg(IO1_DIR_A186_DATA, &value);
                value &= (~IO1_DIR_A186_SET);
                Hi_SetReg(IO1_DIR_A186_DATA, value);

                Hi_GetReg(IO1_DATA_A186_DIR, &value);
                value &= (~IO1_DATA_A186_SET);
                Hi_SetReg(IO1_DATA_A186_DIR, value);
            }
            else if(2 == global_data.enIo_1) {
                Hi_GetReg(IO1_DIR_A186_DATA, &value);
                value |= IO1_DIR_A186_SET;
                Hi_SetReg(IO1_DIR_A186_DATA, value);

                Hi_GetReg(IO1_DATA_A186_DIR, &value);
                value |= IO1_DATA_A186_SET;
                Hi_SetReg(IO1_DATA_A186_DIR, value);
            }
        }

    }
    else if(hd_ver == GZ_31A_D2) {
        if(1 == ioNum) {

                Hi_SetReg(GPIO1_5MUX,0x1);

                Hi_GetReg(GPIO1_5DIR,&value);
                value |= GPIO1_5SET;
                Hi_SetReg(GPIO1_5DIR,value);

                Hi_SetReg(GPIO20_5MUX,0x0);

                if(1 == global_data.enIo_1) {         //IO作为输入
                    Hi_GetReg(GPIO1_5DATA,&value);
                    value &= (~GPIO1_5SET);
                    Hi_SetReg(GPIO1_5DATA,value);

                    Hi_GetReg(GPIO20_5DIR,&value);
                    value &= (~GPIO20_5SET);
                    Hi_SetReg(GPIO20_5DIR,value);
                }
                else if(2 == global_data.enIo_1) {    //IO作为输出
                    Hi_GetReg(GPIO1_5DATA,&value);
                    value |= GPIO1_5SET;
                    Hi_SetReg(GPIO1_5DATA,value);

                    Hi_GetReg(GPIO20_5DIR,&value);
                    value |= GPIO20_5SET;
                    Hi_SetReg(GPIO20_5DIR,value);
                }
                else {
                    COMMON_PRT("io en value error\n");
                    return false;
                }

        }
        if(0 == ioNum) {

                Hi_SetReg(GPIO1_6MUX,0x1);

                Hi_GetReg(GPIO1_6DIR,&value);
                value |= GPIO1_6SET;
                Hi_SetReg(GPIO1_6DIR,value);

                Hi_SetReg(GPIO15_6MUX,0x1);

                if(1 == global_data.enIo_0) {         //IO作为输入
                    Hi_GetReg(GPIO1_6DATA,&value);
                    value &= (~GPIO1_6SET);
                    Hi_SetReg(GPIO1_6DATA,value);

                    Hi_GetReg(GPIO15_6DIR,&value);
                    value &= (~GPIO15_6SET);
                    Hi_SetReg(GPIO15_6DIR,value);
                }
                else if(2 == global_data.enIo_0) {    //IO作为输出
                    Hi_GetReg(GPIO1_6DATA,&value);
                    value |= GPIO1_6SET;
                    Hi_SetReg(GPIO1_6DATA,value);

                    Hi_GetReg(GPIO15_6DIR,&value);
                    value |= GPIO15_6SET;
                    Hi_SetReg(GPIO15_6DIR,value);
                }
                else {
                    COMMON_PRT("io en value error\n");
                    return false;
                }

        }
    }
    else if(NJ_A335 == hd_ver) {
        if(0 == ioNum) {
            Hi_GetReg(A335_IO0_DIR_0_5_DIR, &value);
            value |= A335_IO0_DIR_0_5_SET;
            Hi_SetReg(A335_IO0_DIR_0_5_DIR, value);
            if(1 == global_data.enIo_0) {         //IO作为输入
                Hi_GetReg(A335_IO0_DIR_0_5_DATA,&value);
                value &= (~A335_IO0_DIR_0_5_SET);
                Hi_SetReg(A335_IO0_DIR_0_5_DATA,value);

                Hi_GetReg(A335_IO0_DATA_0_7_DIR,&value);
                value &= (~A335_IO0_DATA_0_7_SET);
                Hi_SetReg(A335_IO0_DATA_0_7_DIR,value);
            }
            else if(2 == global_data.enIo_0) {    //IO作为输出
                Hi_GetReg(A335_IO0_DIR_0_5_DATA,&value);
                value |= A335_IO0_DIR_0_5_SET;
                Hi_SetReg(A335_IO0_DIR_0_5_DATA,value);

                Hi_GetReg(A335_IO0_DATA_0_7_DIR,&value);
                value |= A335_IO0_DATA_0_7_SET;
                Hi_SetReg(A335_IO0_DATA_0_7_DIR,value);
            }
            else {
                COMMON_PRT("io en value error\n");
                return false;
            }
        }
        else if(1 == ioNum) {
            Hi_SetReg(A335_IO1_DIR_5_0_REG, 0x2);
            Hi_SetReg(A335_IO1_DATA_13_2_REG, 0x0);

            Hi_GetReg(A335_IO1_DIR_5_0_DIR, &value);
            value |= A335_IO1_DIR_5_0_SET;
            Hi_SetReg(A335_IO1_DIR_5_0_DIR, value);

            if(1 == global_data.enIo_0) {         //IO作为输入
                Hi_GetReg(A335_IO1_DIR_5_0_DATA,&value);
                value &= (~A335_IO1_DIR_5_0_SET);
                Hi_SetReg(A335_IO1_DIR_5_0_DATA,value);

                Hi_GetReg(A335_IO1_DATA_13_2_DIR,&value);
                value &= (~A335_IO1_DATA_13_2_SET);
                Hi_SetReg(A335_IO1_DATA_13_2_DIR,value);
            }
            else if(2 == global_data.enIo_0) {    //IO作为输出
                Hi_GetReg(A335_IO1_DIR_5_0_DATA,&value);
                value |= A335_IO1_DIR_5_0_SET;
                Hi_SetReg(A335_IO1_DIR_5_0_DATA,value);

                Hi_GetReg(A335_IO1_DATA_13_2_DIR,&value);
                value |= A335_IO1_DATA_13_2_SET;
                Hi_SetReg(A335_IO1_DATA_13_2_DIR,value);
            }
            else {
                COMMON_PRT("io en value error\n");
                return false;
            }
        }
    }
    else if(GZ16A_100_V1 == hd_ver) {
        if(0 == ioNum) {
            io_0_dir.mux = 0x200F00A0;          //GPIO10_4  0x0;
            io_0_dir.dir = 0x201E0400;
            io_0_dir.data = 0x201E0040;
            io_0_dir.set = 0x10;

            io_0_set.mux = 0x200F00A4;          //GPIO10_5  0x0
            io_0_set.dir = 0x201E0400;
            io_0_set.data = 0x201E0080;
            io_0_set.set = 0x20;

            Hi_SetReg(io_0_dir.mux, 0x0);
            Hi_SetReg(io_0_set.mux, 0x0);
            gpio_init(io_0_dir, io_0_set, global_data.enIo_0);
        }
        else if(1 == ioNum) {
            io_1_dir.mux = 0x200F00E4;          //GPIO0_4  0x0;
            io_1_dir.dir = 0x20140400;
            io_1_dir.data = 0x20140040;
            io_1_dir.set = 0x10;

            io_1_set.mux = 0x200F00E0;          //GPIO0_3  0x0
            io_1_set.dir = 0x20140400;
            io_1_set.data = 0x20140020;
            io_1_set.set = 0x8;

            Hi_SetReg(io_1_dir.mux, 0x0);
            Hi_SetReg(io_1_set.mux, 0x0);
            gpio_init(io_1_dir, io_1_set, global_data.enIo_1);
        }
        else if(2 == ioNum) {
            io_2_dir.mux = 0x200F0098;          //GPIO10_2  0x0;
            io_2_dir.dir = 0x201E0400;
            io_2_dir.data = 0x201E0010;
            io_2_dir.set = 0x4;

            io_2_set.mux = 0x200F009C;          //GPIO10_3  0x0
            io_2_set.dir = 0x201E0400;
            io_2_set.data = 0x201E0020;
            io_2_set.set = 0x8;

            Hi_SetReg(io_2_dir.mux, 0x0);
            Hi_SetReg(io_2_set.mux, 0x0);
            gpio_init(io_2_dir, io_2_set, global_data.enIo_2);
        }
        else if(3 == ioNum) {
            io_3_dir.mux = 0x200F0090;          //GPIO10_0  0x0;
            io_3_dir.dir = 0x201E0400;
            io_3_dir.data = 0x201E0004;
            io_3_dir.set = 0x1;

            io_3_set.mux = 0x200F0094;          //GPIO10_1  0x0
            io_3_set.dir = 0x201E0400;
            io_3_set.data = 0x201E0008;
            io_3_set.set = 0x2;

            Hi_SetReg(io_3_dir.mux, 0x0);
            Hi_SetReg(io_3_set.mux, 0x0);
            gpio_init(io_3_dir, io_3_set, global_data.enIo_3);
        }

    }


#ifdef __7000__
    Hi_SetReg(GPIO13_5MUX,0x0);

    Hi_GetReg(GPIO13_5DIR,&value);
    value |= GPIO13_5SET;
    Hi_SetReg(GPIO13_5DIR,value);


    if(1 == global_data.enIo) {         //IO作为输入
        Hi_GetReg(GPIO13_5DATA,&value);
        value &= (~GPIO13_5SET);
        Hi_SetReg(GPIO13_5DATA,value);

        Hi_GetReg(GPIO0_7DIR,&value);
        value &= (~GPIO0_7SET);
        Hi_SetReg(GPIO0_7DIR,value);
    }
    else if(2 == global_data.enIo) {    //IO作为输出
        Hi_GetReg(GPIO13_5DATA,&value);
        value |= GPIO13_5SET;
        Hi_SetReg(GPIO13_5DATA,value);

        Hi_GetReg(GPIO0_7DIR,&value);
        value |= GPIO0_7SET;
        Hi_SetReg(GPIO0_7DIR,value);
    }
    else {
        COMMON_PRT("io en value error\n");
        return false;
    }

#endif
    if(0 == ioNum) {
        socketIO = createUdp(global_data.svIOport);
        FD_SET(socketIO,&readFd);
        if(socketIO >= maxFd)
            maxFd = socketIO + 1;
    }
#ifdef __31DV200__
    if(3 == ioNum) {
        socketIO = createUdp(global_data.svInfraredport);
        FD_SET(socketIO,&readFd);
        if(socketIO >= maxFd)
            maxFd = socketIO + 1;
    }
#else
    if(1 == ioNum) {
        socketIO = createUdp(global_data.svInfraredport);
        FD_SET(socketIO,&readFd);
        if(socketIO >= maxFd)
            maxFd = socketIO + 1;
    }

    if(2 == ioNum || 3 == ioNum) {
        socketIO = createUdp(global_data.svIOport + ioNum - 1);
        FD_SET(socketIO,&readFd);
        if(socketIO >= maxFd)
            maxFd = socketIO + 1;
    }
#endif
    return true;
}

bool io::rcvData2IO()
{
    char tmpData[128] = {0};
    char ioEcho[8] = {0};
    int rcvLen = 0, sendLen = 0;
    socklen_t addrLen = 0;
    uint value;
    memset(tmpData,0,sizeof(tmpData));

    addrLen = sizeof(struct sockaddr_in);


    rcvLen = recvfrom(socketIO,tmpData,MAXDATA,0,(struct sockaddr*)(&ctrlAddr),&addrLen);
    if(rcvLen > 0) {
        printf("rcv io data:%s\n",tmpData);
        if(!strncmp(tmpData,"IO_0=1",strlen("IO_0=1"))) {
            io0Str = 1;
            IOSet(io0Str,0);
            global_data.io0St = 1;
        }
        else if(!strncmp(tmpData,"IO_1=1",strlen("IO_1=1"))) {
            io1Str = 1;
            IOSet(io1Str,1);
            global_data.io1St = 1;
        }
        else if(!strncmp(tmpData,"IO_2=1",strlen("IO_2=1"))) {
            io2Str = 1;
            IOSet(io2Str,2);
            global_data.io2St = 1;
        }
        else if(!strncmp(tmpData,"IO_3=1",strlen("IO_3=1"))) {
            io3Str = 1;
            IOSet(io3Str,3);
            global_data.io3St = 1;
        }
        #ifdef __31DV200__
        else if(!strncmp(tmpData,"IO_2=1",strlen("IO_2=1"))) {
            io2Str = 1;
            IOSet(io2Str,1);
            global_data.io2St = 1;
        }
        else if(!strncmp(tmpData,"IO_3=1",strlen("IO_3=1"))) {
            io3Str = 1;
            IOSet(io3Str,1);
            global_data.io3St = 1;
        }
        #endif
        else if(!strncmp(tmpData,"IO_0=0",strlen("IO_0=0"))) {
            io0Str = 0;
            IOSet(io0Str,0);
            global_data.io0St = 0;
        }
        else if(!strncmp(tmpData,"IO_1=0",strlen("IO_1=0"))) {
            io1Str = 0;
            IOSet(io1Str,1);
            global_data.io1St = 0;
        }
        else if(!strncmp(tmpData,"IO_2=0",strlen("IO_2=0"))) {
            io2Str = 0;
            IOSet(io2Str,2);
            global_data.io2St = 0;
        }
        else if(!strncmp(tmpData,"IO_3=0",strlen("IO_3=0"))) {
            io3Str = 0;
            IOSet(io3Str,3);
            global_data.io3St = 0;
        }
         #ifdef __31DV200__
        else if(!strncmp(tmpData,"IO_2=0",strlen("IO_2=0"))) {
            io2Str = 0;
            IOSet(io2Str,1);
            global_data.io2St = 0;
        }
        else if(!strncmp(tmpData,"IO_3=0",strlen("IO_3=0"))) {
            io3Str = 0;
            IOSet(io3Str,1);
            global_data.io3St = 0;
        }
        #endif
        else if(!strncmp(tmpData,"getIO_0",strlen("getIO_0"))) {
            if(global_data.enIo_0 == 1) {
                if(hd_ver == NJ_A151) {
                    if(outmode)
                    {
                        Hi_GetReg(A151OUT_IO_0_DATA_DATA,&value);
                        if(value & A151OUT_IO_0_DATA_SET) {
                            io0Str = 1;
                            global_data.io0St = 1;
                        }
                        else {
                            io0Str = 0;
                            global_data.io0St = 0;
                        }
                    }
                    else
                    {
                        Hi_GetReg(A151_IO_0_DATA_DATA,&value);
                        if(value & A151_IO_0_DATA_SET) {
                            io0Str = 1;
                            global_data.io0St = 1;
                        }
                        else {
                            io0Str = 0;
                            global_data.io0St = 0;
                        }
                    }
                }
                if(hd_ver == NJ_A323) {
                    if(outmode)
                    {
                        Hi_GetReg(A323_IO_0_DATA_DATA,&value);
                        if(value & A323_IO_0_DATA_SET) {
                            io0Str = 1;
                            global_data.io0St = 1;
                        }
                        else {
                            io0Str = 0;
                            global_data.io0St = 0;
                        }
                    }
                    else
                    {
                        Hi_GetReg(A151_IO_0_DATA_DATA,&value);
                        if(value & A151_IO_0_DATA_SET) {
                            io0Str = 1;
                            global_data.io0St = 1;
                        }
                        else {
                            io0Str = 0;
                            global_data.io0St = 0;
                        }
                    }
                }
#if 0
                else if(hd_ver == NJ_A323){
                    Hi_GetReg(A323_IO_0_DATA_DATA,&value);
                    if(value & A323_IO_0_DATA_SET) {
                        io0Str = 1;
                        global_data.io0St = 1;
                    }
                    else {
                        io0Str = 0;
                        global_data.io0St = 0;
                    }
                }
#endif
                else if(hd_ver == NJ_A186_V1){
                    Hi_GetReg(IO0_DATA_A186_DATA,&value);
                    if(value & IO0_DATA_A186_SET) {
                        io0Str = 1;
                        global_data.io0St = 1;
                    }
                    else {
                        io0Str = 0;
                        global_data.io0St = 0;
                    }
                }
                else if(hd_ver == NJ_A304){
                    Hi_GetReg(A304_IO_0_DATA_DATA,&value);
                    if(value & A304_IO_0_DATA_SET) {
                        io0Str = 1;
                        global_data.io0St = 1;
                    }
                    else {
                        io0Str = 0;
                        global_data.io0St = 0;
                    }
                }
                else if(NJ_A335 == hd_ver) {
                    Hi_GetReg(A335_IO0_DATA_0_7_DATA,&value);
                    if(value & A335_IO0_DATA_0_7_SET) {
                        io0Str = 1;
                        global_data.io0St = 1;
                    }
                    else {
                        io0Str = 0;
                        global_data.io0St = 0;
                    }
                }
#ifdef __7000_NJ__
                if(hd_ver == NJ_21D) {
                    Hi_GetReg(GPIO0_3DATA,&value);
                    if(value & GPIO0_3SET) {
                        io0Str = 1;
                        global_data.io0St = 1;
                    }
                    else {
                        io0Str = 0;
                        global_data.io0St = 0;
                    }
                }
                else if(hd_ver == PTN_21A) {
                    Hi_GetReg(IO_0_10_2_DATA, &value);
                    if(value & IO_0_10_2SET) {
                        io0Str = 1;
                        global_data.io0St = 1;
                    }
                    else {
                        io0Str = 0;
                        global_data.io0St = 0;
                    }
                }
                else {
                    Hi_GetReg(GPIO0_6DATA,&value);
                    if(value & GPIO0_6SET) {
                        io0Str = 1;
                        global_data.io0St = 1;
                    }
                    else {
                        io0Str = 0;
                        global_data.io0St = 0;
                    }
                }


#endif

#ifdef __GPIO1_5__
                if(ir_nj_700k) {
                    Hi_GetReg(GPIO20_5DATA,&value);
                    if(value & GPIO20_5SET) {
                        io0Str = 1;
                        global_data.io0St = 1;
                    }
                    else {
                        io0Str = 0;
                        global_data.io0St = 0;
                    }
                }
                else {
                    if(hd_ver == GZ_700S) {
                        Hi_GetReg(GPIO11_1DATA, &value);
                        if(value & GPIO11_1SET) {
                            io0Str = 1;
                            global_data.io0St = 1;
                        }
                        else {
                            io0Str = 0;
                            global_data.io0St = 0;
                        }
                    }
                    if(hd_ver == GZ_31A_D2) {
                        Hi_GetReg(GPIO15_6DATA, &value);
                        if(value & GPIO15_6SET) {
                            io0Str = 1;
                            global_data.io0St = 1;
                        }
                        else {
                            io0Str = 0;
                            global_data.io0St = 0;
                        }
                    }
                    else if(hd_ver == NJ_A345 || GZ16A_100_V1 == hd_ver) {
                        Hi_GetReg(io_0_set.data, &value);
                        if(value & io_0_set.set) {
                            io0Str = 1;
                            global_data.io0St = 1;
                        }
                        else {
                            io0Str = 0;
                            global_data.io0St = 0;
                        }
                    }
                    else {
                        Hi_GetReg(GPIO1_7DATA,&value);
                        if(value & GPIO1_7SET) {
                            io0Str = 1;
                            global_data.io0St = 1;
                        }
                        else {
                            io0Str = 0;
                            global_data.io0St = 0;
                        }
                    }

                }

#endif
                memset(ioEcho,0,sizeof(ioEcho));
                printf("now io state 0 :%d\n",io0Str);
                if(io0Str) {
                    strcpy(ioEcho,"IO_0=ON");
                }
                else {
                    strcpy(ioEcho,"IO_0=OFF");
                }

                if((global_data.ctlIO0port != 0) && (strlen(global_data.ctlip) >= 7)) {
                    ctrlAddr.sin_family = AF_INET;
                    ctrlAddr.sin_port = htons(global_data.ctlIO0port);
                    ctrlAddr.sin_addr.s_addr = inet_addr(global_data.ctlip);
                    sendLen = sendto(socketIO,ioEcho,strlen(ioEcho),0,(struct sockaddr*)&ctrlAddr,sizeof(struct sockaddr_in));
                    if(sendLen > 0)
                        return true;
                    else {
                        COMMON_PRT("send dev io str to ctrl error\n");
                        return false;
                    }
                }
                else {
                    sendLen = sendto(socketIO,ioEcho,strlen(ioEcho),0,(struct sockaddr*)&ctrlAddr,sizeof(struct sockaddr_in));
                    if(sendLen > 0 ) {
                        COMMON_PRT("UMP not set ctrl IP PORT and echo\n");
                        return true;
                    }
                    else {
                        COMMON_PRT("send dev io str to echo error\n");
                        return false;
                    }

                }
            }
        }
        else if(!strncmp(tmpData,"getIO_1",strlen("getIO_1"))) {
            if(global_data.enIo_1 == 1) {
                if((hd_ver == NJ_7000) || (hd_ver == PTN_7000)) {
                    Hi_GetReg(GPIO0_3DATA,&value);
                    if(value & GPIO0_3SET) {
                        io1Str = 1;
                        global_data.io1St = 1;
                    }
                    else {
                        io1Str = 0;
                        global_data.io1St = 0;
                    }
                }
                else if(hd_ver == PTN_21A) {
                    Hi_GetReg(IO_1_5_6_DATA, &value);
                    if(value & IO_1_5_6_SET){
                        io1Str = 1;
                        global_data.io1St = 1;
                    }
                    else {
                        io1Str = 0;
                        global_data.io1St = 0;
                    }
                }
                else if(hd_ver ==  NJ_21D) {
                    Hi_GetReg(GPIO0_5DATA,&value);
                    if(value & GPIO0_5SET) {
                        io1Str = 1;
                        global_data.io1St = 1;
                    }
                    else {
                        io1Str = 0;
                        global_data.io1St = 0;
                    }
                }
                else if(hd_ver == NJ_A304){
                    Hi_GetReg(A304_IO_1_DATA_DATA,&value);
                    if(value & A304_IO_1_DATA_SET) {
                        io1Str = 1;
                        global_data.io1St = 1;
                    }
                    else {
                        io1Str = 0;
                        global_data.io1St = 0;
                    }
                }
                else if((hd_ver == NJ_700K) || (hd_ver ==GZ_700K) || (hd_ver == GZ_8000)|| (hd_ver == NJ_A156_V2)||(hd_ver == NJ_A241)) {
                    if(ir_nj_700k) {
                        Hi_GetReg(GPIO15_6DATA,&value);
                        if(value & GPIO15_6SET) {
                            io1Str = 1;
                            global_data.io1St = 1;
                        }
                        else {
                            io1Str = 0;
                            global_data.io1St = 0;
                        }


                        Hi_GetReg(GPIO1_7DATA, &value);
                        if(value & GPIO1_7SET) {
                            io1Str = 1;
                            global_data.io1St = 1;
                        }
                        else {
                            io1Str = 0;
                            global_data.io1St = 0;
                        }
                    }
                    else {
                        Hi_GetReg(GPIO20_5DATA,&value);
                        if(value & GPIO20_5SET) {
                            io1Str = 1;
                            global_data.io1St = 1;
                        }
                        else {
                            io1Str = 0;
                            global_data.io1St = 0;
                        }
                    }
                }
                else if(hd_ver == GZ_31A_D2) {
                    Hi_GetReg(GPIO20_5DATA,&value);
                    if(value & GPIO20_5SET) {
                        io1Str = 1;
                        global_data.io1St = 1;
                    }
                    else {
                        io1Str = 0;
                        global_data.io1St = 0;
                    }

                }
                else if(hd_ver == GZ_700S) {
                    Hi_GetReg(GPIO14_7DATA, &value);
                    if(value & GPIO14_7SET) {
                        io1Str = 1;
                        global_data.io1St = 1;
                    }
                    else {
                        io1Str = 0;
                        global_data.io1St = 0;
                    }

                }
                else if(hd_ver == NJ_A151) {
                    if(outmode)
                    {
                        Hi_GetReg(A151OUT_IO_1_DATA_DATA, &value);
                        if(value & A151OUT_IO_1_DATA_SET) {
                            io1Str = 1;
                            global_data.io1St = 1;
                        }
                        else {
                            io1Str = 0;
                            global_data.io1St = 0;
                        }
                    }
                    else
                    {
                        Hi_GetReg(A151_IO_1_DATA_DATA, &value);
                        if(value & A151_IO_1_DATA_SET) {
                            io1Str = 1;
                            global_data.io1St = 1;
                        }
                        else {
                            io1Str = 0;
                            global_data.io1St = 0;
                        }

                    }
                 }
                else if(hd_ver == NJ_A323) {
                    if(outmode)
                    {
                        Hi_GetReg(A323_IO_1_DATA_DATA,&value);
                        if(value & A323_IO_1_DATA_SET) {
                            io1Str = 1;
                            global_data.io1St = 1;
                        }
                        else {
                            io1Str = 0;
                            global_data.io1St = 0;
                        }
                    }
                    else
                    {
                        Hi_GetReg(A151_IO_1_DATA_DATA, &value);
                        if(value & A151_IO_1_DATA_SET) {
                            io1Str = 1;
                            global_data.io1St = 1;
                        }
                        else {
                            io1Str = 0;
                            global_data.io1St = 0;
                        }

                    }
                 }
#if 0
                else if(hd_ver == NJ_A323){
                    Hi_GetReg(A323_IO_1_DATA_DATA,&value);
                    if(value & A323_IO_1_DATA_SET) {
                        io1Str = 1;
                        global_data.io1St = 1;
                    }
                    else {
                        io1Str = 0;
                        global_data.io1St = 0;
                    }
                }
#endif
                else if(hd_ver == NJ_A186_V1){
                    Hi_GetReg(IO1_DATA_A186_DATA,&value);
                    if(value & IO1_DATA_A186_SET) {
                        io1Str = 1;
                        global_data.io1St = 1;
                    }
                    else {
                        io1Str = 0;
                        global_data.io1St = 0;
                    }
                }
                else if(hd_ver == NJ_A345 || GZ16A_100_V1 == hd_ver) {
                    Hi_GetReg(io_1_set.data, &value);
                    if(value & io_1_set.set) {
                        io1Str = 1;
                        global_data.io1St = 1;
                    }
                    else {
                        io1Str = 0;
                        global_data.io1St = 0;
                    }
                }
                printf("now io state 1 :%d\n",io1Str);

                memset(ioEcho,0,sizeof(ioEcho));
                if(io1Str) {
                    strcpy(ioEcho,"IO_1=ON");
                }
                else {
                    strcpy(ioEcho,"IO_1=OFF");
                }

                if((global_data.ctlIO1port != 0) && (strlen(global_data.ctlip) >= 7)) {
                    ctrlAddr.sin_family = AF_INET;
                    ctrlAddr.sin_port = htons(global_data.ctlIO1port);
                    ctrlAddr.sin_addr.s_addr = inet_addr(global_data.ctlip);
                    sendLen = sendto(socketIO,ioEcho,strlen(ioEcho),0,(struct sockaddr*)&ctrlAddr,sizeof(struct sockaddr_in));
                    if(sendLen > 0)
                        return true;
                    else {
                        COMMON_PRT("send dev io str to ctrl error\n");
                        return false;
                    }
                }
                else {
                    sendLen = sendto(socketIO,ioEcho,strlen(ioEcho),0,(struct sockaddr*)&ctrlAddr,sizeof(struct sockaddr_in));
                    if(sendLen > 0 ) {
                        COMMON_PRT("UMP not set ctrl IP PORT and echo\n");
                        return true;
                    }
                    else {
                        COMMON_PRT("send dev io str to echo error\n");
                        return false;
                    }

                }
            }
        }
        else if(!strncmp(tmpData,"getIO_2",strlen("getIO_2"))) {
            if(global_data.enIo_1 == 1) {
                if(GZ16A_100_V1 == hd_ver) {
                    Hi_GetReg(io_2_set.data, &value);
                    if(value & io_2_set.set) {
                        io2Str = 1;
                        global_data.io2St = 1;
                    }
                    else {
                        io2Str = 0;
                        global_data.io2St = 0;
                    }
                }
                memset(ioEcho,0,sizeof(ioEcho));
                if(io2Str) {
                    strcpy(ioEcho,"IO_2=ON");
                }
                else {
                    strcpy(ioEcho,"IO_2=OFF");
                }

                if((global_data.ctlIO2port != 0) && (strlen(global_data.ctlip) >= 7)) {
                    ctrlAddr.sin_family = AF_INET;
                    ctrlAddr.sin_port = htons(global_data.ctlIO2port);
                    ctrlAddr.sin_addr.s_addr = inet_addr(global_data.ctlip);
                    sendLen = sendto(socketIO,ioEcho,strlen(ioEcho),0,(struct sockaddr*)&ctrlAddr,sizeof(struct sockaddr_in));
                    if(sendLen > 0)
                        return true;
                    else {
                        COMMON_PRT("send dev io str to ctrl error\n");
                        return false;
                    }
                }
                else {
                    sendLen = sendto(socketIO,ioEcho,strlen(ioEcho),0,(struct sockaddr*)&ctrlAddr,sizeof(struct sockaddr_in));
                    if(sendLen > 0 ) {
                        COMMON_PRT("UMP not set ctrl IP PORT and echo\n");
                        return true;
                    }
                    else {
                        COMMON_PRT("send dev io str to echo error\n");
                        return false;
                    }

                }
            }
        }
        else if(!strncmp(tmpData,"getIO_3",strlen("getIO_3"))) {
            if(global_data.enIo_3 == 1) {
                if(GZ16A_100_V1 == hd_ver) {
                    Hi_GetReg(io_3_set.data, &value);
                    if(value & io_3_set.set) {
                        io3Str = 1;
                        global_data.io3St = 1;
                    }
                    else {
                        io3Str = 0;
                        global_data.io3St = 0;
                    }
                }
                memset(ioEcho,0,sizeof(ioEcho));
                if(io3Str) {
                    strcpy(ioEcho,"IO_3=ON");
                }
                else {
                    strcpy(ioEcho,"IO_3=OFF");
                }

                if((global_data.ctlIO3port != 0) && (strlen(global_data.ctlip) >= 7)) {
                    ctrlAddr.sin_family = AF_INET;
                    ctrlAddr.sin_port = htons(global_data.ctlIO3port);
                    ctrlAddr.sin_addr.s_addr = inet_addr(global_data.ctlip);
                    sendLen = sendto(socketIO,ioEcho,strlen(ioEcho),0,(struct sockaddr*)&ctrlAddr,sizeof(struct sockaddr_in));
                    if(sendLen > 0)
                        return true;
                    else {
                        COMMON_PRT("send dev io str to ctrl error\n");
                        return false;
                    }
                }
                else {
                    sendLen = sendto(socketIO,ioEcho,strlen(ioEcho),0,(struct sockaddr*)&ctrlAddr,sizeof(struct sockaddr_in));
                    if(sendLen > 0 ) {
                        COMMON_PRT("UMP not set ctrl IP PORT and echo\n");
                        return true;
                    }
                    else {
                        COMMON_PRT("send dev io str to echo error\n");
                        return false;
                    }

                }
            }
        }

        #ifdef __31DV200__
        else if(!strncmp(tmpData,"getIO_2",strlen("getIO_2"))) {
            if(global_data.enIo_2 == 1) {

                if(hd_ver == NJ_A304){
                    Hi_GetReg(A304_IO_2_DATA_DATA,&value);
                    if(value & A304_IO_2_DATA_SET) {
                        io2Str = 1;
                        global_data.io2St = 1;
                    }
                    else {
                        io2Str = 0;
                        global_data.io2St = 0;
                    }
                }

                printf("now io state 2 :%d\n",io2Str);

                memset(ioEcho,0,sizeof(ioEcho));
                if(io2Str) {
                    strcpy(ioEcho,"IO_2=ON");
                }
                else {
                    strcpy(ioEcho,"IO_2=OFF");
                }

                if((global_data.ctlIO2port != 0) && (strlen(global_data.ctlip) >= 7)) {
                    ctrlAddr.sin_family = AF_INET;
                    ctrlAddr.sin_port = htons(global_data.ctlIO2port);
                    ctrlAddr.sin_addr.s_addr = inet_addr(global_data.ctlip);
                    sendLen = sendto(socketIO,ioEcho,strlen(ioEcho),0,(struct sockaddr*)&ctrlAddr,sizeof(struct sockaddr_in));
                    if(sendLen > 0)
                        return true;
                    else {
                        COMMON_PRT("send dev io str to ctrl error\n");
                        return false;
                    }
                }
                else {
                    sendLen = sendto(socketIO,ioEcho,strlen(ioEcho),0,(struct sockaddr*)&ctrlAddr,sizeof(struct sockaddr_in));
                    if(sendLen > 0 ) {
                        COMMON_PRT("UMP not set ctrl IP PORT and echo\n");
                        return true;
                    }
                    else {
                        COMMON_PRT("send dev io str to echo error\n");
                        return false;
                    }

                }
            }
        }

        else if(!strncmp(tmpData,"getIO_3",strlen("getIO_3"))) {
            if(global_data.enIo_3 == 1) {

                if(hd_ver == NJ_A304){
                    Hi_GetReg(A304_IO_3_DATA_DATA,&value);
                    if(value & A304_IO_3_DATA_SET) {
                        io3Str = 1;
                        global_data.io3St = 1;
                    }
                    else {
                        io3Str = 0;
                        global_data.io3St = 0;
                    }
                }

                printf("now io state 3 :%d\n",io3Str);

                memset(ioEcho,0,sizeof(ioEcho));
                if(io3Str) {
                    strcpy(ioEcho,"IO_3=ON");
                }
                else {
                    strcpy(ioEcho,"IO_3=OFF");
                }

                if((global_data.ctlIO3port != 0) && (strlen(global_data.ctlip) >= 7)) {
                    ctrlAddr.sin_family = AF_INET;
                    ctrlAddr.sin_port = htons(global_data.ctlIO3port);
                    ctrlAddr.sin_addr.s_addr = inet_addr(global_data.ctlip);
                    sendLen = sendto(socketIO,ioEcho,strlen(ioEcho),0,(struct sockaddr*)&ctrlAddr,sizeof(struct sockaddr_in));
                    if(sendLen > 0)
                        return true;
                    else {
                        COMMON_PRT("send dev io str to ctrl error\n");
                        return false;
                    }
                }
                else {
                    sendLen = sendto(socketIO,ioEcho,strlen(ioEcho),0,(struct sockaddr*)&ctrlAddr,sizeof(struct sockaddr_in));
                    if(sendLen > 0 ) {
                        COMMON_PRT("UMP not set ctrl IP PORT and echo\n");
                        return true;
                    }
                    else {
                        COMMON_PRT("send dev io str to echo error\n");
                        return false;
                    }

                }
            }
        }
        #endif
#ifdef __7000__
                Hi_GetReg(GPIO0_7DATA,&value);
                if(value & GPIO0_7SET) {
                    ioStr = 1;
                    global_data.ioSt = 1;
                }
                else {
                    ioStr = 0;
                    global_data.ioSt = 0;
                }
#endif


        else {
            COMMON_PRT("ctrl send data error:%s\n",tmpData);
        }

    }
    else {
        COMMON_PRT("not get rcv io data error\n");
        return false;
    }

    auto thisini = Singleton<SoftwareConfig>::getInstance();
    thisini->SetConfig(SoftwareConfig::kIO0st,global_data.io0St);
    thisini->SetConfig(SoftwareConfig::kIO1st,global_data.io1St);
    #ifdef __31DV200__
    thisini->SetConfig(SoftwareConfig::kIO0st,global_data.io2St);
    thisini->SetConfig(SoftwareConfig::kIO1st,global_data.io3St);
    #endif
    thisini->SaveConfig();
}


bool io::IOSet(int set,int ioNum)
{
    uint value;
    if(set == 1) {
        if((hd_ver == NJ_7000) || (hd_ver == PTN_7000)) {
            if(0 == ioNum) {
                Hi_GetReg(GPIO0_6DATA,&value);
                value |= GPIO0_6SET;
                Hi_SetReg(GPIO0_6DATA,value);
            }
            else if(1 == ioNum) {
                Hi_GetReg(GPIO0_3DATA,&value);
                value |= GPIO0_3SET;
                Hi_SetReg(GPIO0_3DATA,value);
            }
        }
        else if(hd_ver == PTN_21A) {
            if(0 == ioNum) {
                Hi_GetReg(IO_0_10_2_DATA, &value);
                value |= IO_0_10_2SET;
                Hi_SetReg(IO_0_10_2_DATA, value);
            }
            else if(1 == ioNum) {
                Hi_GetReg(IO_1_5_6_DATA, &value);
                value |= IO_1_5_6_SET;
                Hi_SetReg(IO_1_5_6_DATA, value);
            }
        }
        else if(NJ_21D == hd_ver) {
            if(0 == ioNum) {
                Hi_GetReg(GPIO0_3DATA,&value);
                value |= GPIO0_3SET;
                Hi_SetReg(GPIO0_3DATA,value);
            }
            else if(1 == ioNum) {
                Hi_GetReg(GPIO0_5DATA,&value);
                value |= GPIO0_5SET;
                Hi_SetReg(GPIO0_5DATA,value);

            }
        }
        else if((hd_ver == NJ_700K) || (hd_ver == GZ_700K) || (hd_ver == GZ_8000)|| (hd_ver == NJ_A156_V2)||(hd_ver == NJ_A241)||(hd_ver == GZ_31A_D2)) {
            if(0 == ioNum) {
                if(ir_nj_700k) {
                    Hi_GetReg(GPIO20_5DATA,&value);
                    value |= GPIO20_5SET;
                    Hi_SetReg(GPIO20_5DATA,value);
                }
                else if(hd_ver == GZ_31A_D2) {
                    Hi_GetReg(GPIO15_6DATA,&value);
                    value |= GPIO15_6SET;
                    Hi_SetReg(GPIO15_6DATA,value);
                    printf("\n31A_D2 IO_0 up\n");
                }
                else {
                    Hi_GetReg(GPIO1_7DATA,&value);
                    value |= GPIO1_7SET;
                    Hi_SetReg(GPIO1_7DATA,value);
                    printf("\nIO_0 up\n");
                }
            }
            else if(1 == ioNum) {
                if(ir_nj_700k) {
                    Hi_GetReg(GPIO15_6DATA,&value);
                    value |= GPIO15_6SET;
                    Hi_SetReg(GPIO15_6DATA,value);

                    Hi_GetReg(GPIO1_7DATA,&value);
                    value |= GPIO1_7SET;
                    Hi_SetReg(GPIO1_7DATA,value);
                }
                else {
                    Hi_GetReg(GPIO20_5DATA,&value);
                    value |= GPIO20_5SET;
                    Hi_SetReg(GPIO20_5DATA,value);
                     printf("\nIO_1 up\n");
                }
            }
        }
        else if(hd_ver == GZ_700S) {
            if(0 == ioNum) {
                Hi_GetReg(GPIO11_1DATA,&value);
                value |= GPIO11_1SET;
                Hi_SetReg(GPIO11_1DATA,value);

            }
            else if(1 == ioNum) {
                Hi_GetReg(GPIO14_7DATA,&value);
                value |= GPIO14_7SET;
                Hi_SetReg(GPIO14_7DATA,value);
            }

        }
        else if(hd_ver == NJ_31DV200) {
            if(0 == ioNum) {
                Hi_GetReg(IO_0_DATA_DATA, &value);
                value |= IO_0_DATA_SET;
                Hi_SetReg(IO_0_DATA_DATA, value);
                printf("set io0 up\n");
            }
            else if(1 == ioNum) {
                Hi_GetReg(IO_1_DATA_DATA, &value);
                value |= IO_1_DATA_SET;
                Hi_SetReg(IO_1_DATA_DATA, value);
            }
            else if(2 == ioNum) {
                Hi_GetReg(IO_2_DATA_DATA, &value);
                value |= IO_2_DATA_SET;
                Hi_SetReg(IO_2_DATA_DATA, value);

            }
            else if(3 == ioNum) {
                Hi_GetReg(IO_3_DATA_DATA, &value);
                value |= IO_3_DATA_SET;
                Hi_SetReg(IO_3_DATA_DATA, value);
            }
        }
        else if(hd_ver == GZ_31DV200) {
            if(0 == ioNum) {
                Hi_GetReg(IO_4_DATA_DATA, &value);
                value |= IO_4_DATA_SET;
                Hi_SetReg(IO_4_DATA_DATA, value);
                printf("set io0 up\n");
            }
            else if(1 == ioNum) {
                Hi_GetReg(IO_5_DATA_DATA, &value);
                value |= IO_5_DATA_SET;
                Hi_SetReg(IO_5_DATA_DATA, value);
            }

        }
        else if(hd_ver == NJ_A151) {
            if(0 == ioNum) {
                if(outmode)
                {
                    Hi_GetReg(A151OUT_IO_0_DATA_DATA, &value);
                    value |= A151OUT_IO_0_DATA_SET;
                    Hi_SetReg(A151OUT_IO_0_DATA_DATA, value);
                    printf("set io0 up(out)\n");
                }
                else
                {
                    Hi_GetReg(A151_IO_0_DATA_DATA, &value);
                    value |= A151_IO_0_DATA_SET;
                    Hi_SetReg(A151_IO_0_DATA_DATA, value);
                    printf("set io0 up\n");
                }
            }
            else if(1 == ioNum) {
                if(outmode)
                {
                    Hi_GetReg(A151OUT_IO_1_DATA_DATA, &value);
                    value |= A151OUT_IO_1_DATA_SET;
                    Hi_SetReg(A151OUT_IO_1_DATA_DATA, value);
                }
                else
                {
                    Hi_GetReg(A151_IO_1_DATA_DATA, &value);
                    value |= A151_IO_1_DATA_SET;
                    Hi_SetReg(A151_IO_1_DATA_DATA, value);
                }
            }

        }
        else if(hd_ver == NJ_A323) {
            if(0 == ioNum) {
                if(outmode)
                {
                    Hi_GetReg(A323_IO_0_DATA_DATA, &value);
                    value |= A323_IO_0_DATA_SET;
                    Hi_SetReg(A323_IO_0_DATA_DATA, value);
                    printf("set A323 io0 up\n");
                }
                else
                {
                    Hi_GetReg(A151_IO_0_DATA_DATA, &value);
                    value |= A151_IO_0_DATA_SET;
                    Hi_SetReg(A151_IO_0_DATA_DATA, value);
                    printf("set io0 up\n");
                }
            }
            else if(1 == ioNum) {
                if(outmode)
                {
                    Hi_GetReg(A323_IO_1_DATA_DATA, &value);
                    value |= A323_IO_1_DATA_SET;
                    Hi_SetReg(A323_IO_1_DATA_DATA, value);
                    printf("set A323 io1 up\n");
                }
                else
                {
                    Hi_GetReg(A151_IO_1_DATA_DATA, &value);
                    value |= A151_IO_1_DATA_SET;
                    Hi_SetReg(A151_IO_1_DATA_DATA, value);
                }
            }

        }
#if 0
        else if(hd_ver == NJ_A323) {
            if(0 == ioNum) {
                Hi_GetReg(A323_IO_0_DATA_DATA, &value);
                value |= A323_IO_0_DATA_SET;
                Hi_SetReg(A323_IO_0_DATA_DATA, value);
                printf("set A323 io0 up\n");
            }
            else if(1 == ioNum) {
                Hi_GetReg(A323_IO_1_DATA_DATA, &value);
                value |= A323_IO_1_DATA_SET;
                Hi_SetReg(A323_IO_1_DATA_DATA, value);
                printf("set A323 io1 up\n");
            }

        }
#endif
        else if(hd_ver == NJ_A186_V1) {
            if(0 == ioNum) {
                Hi_GetReg(IO0_DATA_A186_DATA, &value);
                value |= IO0_DATA_A186_SET;
                Hi_SetReg(IO0_DATA_A186_DATA, value);
                printf("set io0 up\n");
            }
            else if(1 == ioNum) {
                Hi_GetReg(IO1_DATA_A186_DATA, &value);
                value |= IO1_DATA_A186_SET;
                Hi_SetReg(IO1_DATA_A186_DATA, value);
                printf("set io1 up\n");
            }

        }
        else if(NJ_A335 == hd_ver) {
            if(0 == ioNum) {
                Hi_GetReg(A335_IO0_DATA_0_7_DATA, &value);
                value |= A335_IO0_DATA_0_7_SET;
                Hi_SetReg(A335_IO0_DATA_0_7_DATA, value);
            }
            else if(1 == ioNum) {
                Hi_GetReg(A335_IO1_DATA_13_2_DATA, &value);
                value |= A335_IO1_DATA_13_2_SET;
                Hi_SetReg(A335_IO1_DATA_13_2_DATA, value);
            }
        }
        else if(NJ_A345 == hd_ver) {
            if(0 == ioNum) {
                Hi_GetReg(io_0_set.data, &value);
                value |= io_0_set.set;
                Hi_SetReg(io_0_set.data, value);
            }
            else if(1 == ioNum) {
                Hi_GetReg(io_1_set.data, &value);
                value |= io_1_set.set;
                Hi_SetReg(io_1_set.data, value);
            }
        }
        else if(GZ16A_100_V1 == hd_ver) {
            if(0 == ioNum) {
                Hi_GetReg(io_0_set.data, &value);
                value |= io_0_set.set;
                Hi_SetReg(io_0_set.data, value);
            }
            else if(1 == ioNum) {
                cout << "set io 1" << endl;
                Hi_GetReg(io_1_set.data, &value);
                value |= io_1_set.set;
                Hi_SetReg(io_1_set.data, value);
            }
            else if(2 == ioNum) {
                Hi_GetReg(io_2_set.data, &value);
                value |= io_2_set.set;
                Hi_SetReg(io_2_set.data, value);
            }
            else if(3 == ioNum) {
                Hi_GetReg(io_3_set.data, &value);
                value |= io_3_set.set;
                Hi_SetReg(io_3_set.data, value);
            }
        }
#ifdef __7000__
        Hi_GetReg(GPIO0_7DATA,&value);
        value |= GPIO0_7SET;
        Hi_SetReg(GPIO0_7DATA,value);
#endif

    }
    else if(set == 0) {
        if((hd_ver == NJ_7000) || (hd_ver == PTN_7000)) {
            if(0 == ioNum) {
                Hi_GetReg(GPIO0_6DATA,&value);
                value &= (~GPIO0_6SET);
                Hi_SetReg(GPIO0_6DATA,value);
            }
            else if(1 == ioNum) {
                Hi_GetReg(GPIO0_3DATA,&value);
                value &= (~GPIO0_3SET);
                Hi_SetReg(GPIO0_3DATA,value);
            }
        }
        else if(hd_ver == PTN_21A) {
            if(0 == ioNum) {
                Hi_GetReg(IO_0_10_2_DATA, &value);
                value &= (~IO_0_10_2SET);
                Hi_SetReg(IO_0_10_2_DATA, value);
            }
            else if(1 == ioNum) {
                Hi_GetReg(IO_1_5_6_DATA, &value);
                value &= (~IO_1_5_6_SET);
                Hi_SetReg(IO_1_5_6_DATA, value);
            }
        }
        else if(hd_ver == NJ_21D) {
            if(0 ==  ioNum) {
                Hi_GetReg(GPIO0_3DATA, &value);
                value &= (~GPIO0_3SET);
                Hi_SetReg(GPIO0_3DATA,value);
            }
            else {
                Hi_GetReg(GPIO0_5DATA, &value);
                value &= (~GPIO0_5SET);
                Hi_SetReg(GPIO0_5DATA,value);
            }
        }
        else if((hd_ver == NJ_700K) || (hd_ver == GZ_700K) || (hd_ver == GZ_8000)|| (hd_ver == NJ_A156_V2)||(hd_ver == NJ_A241)||(hd_ver == GZ_31A_D2)) {
            if(0 == ioNum) {
                if(ir_nj_700k) {
                    Hi_GetReg(GPIO20_5DATA,&value);
                    value &= (~GPIO20_5SET);
                    Hi_SetReg(GPIO20_5DATA,value);
                }
                else if(hd_ver == GZ_31A_D2) {
                    Hi_GetReg(GPIO15_6DATA,&value);
                    value &= (~GPIO15_6SET);
                    Hi_SetReg(GPIO15_6DATA,value);
                    printf("\n31A_D2 IO_0 down\n");
                }
                else {
                    Hi_GetReg(GPIO1_7DATA,&value);
                    value &= (~GPIO1_7SET);
                    Hi_SetReg(GPIO1_7DATA,value);
                     printf("\nIO_0 down\n");
                }
            }
            else if(1 == ioNum) {
                if(ir_nj_700k) {
                    Hi_GetReg(GPIO15_6DATA,&value);
                    value &= (~GPIO15_6SET);
                    Hi_SetReg(GPIO15_6DATA,value);

                    Hi_GetReg(GPIO1_7DATA,&value);
                    value &= (~GPIO1_7SET);
                    Hi_SetReg(GPIO1_7DATA,value);
                }
                else {
                    Hi_GetReg(GPIO20_5DATA,&value);
                    value &= (~GPIO20_5SET);
                    Hi_SetReg(GPIO20_5DATA,value);
                     printf("\nIO_1 down\n");
                }
            }
        }
        else if(hd_ver == GZ_700S) {
            if(0 ==  ioNum) {
                Hi_GetReg(GPIO11_1DATA, &value);
                value &= (~GPIO11_1SET);
                Hi_SetReg(GPIO11_1DATA,value);
            }
            else {
                Hi_GetReg(GPIO14_7DATA, &value);
                value &= (~GPIO14_7SET);
                Hi_SetReg(GPIO14_7DATA,value);
            }
        }
        else if(hd_ver == NJ_31DV200) {
            if(0 == ioNum) {
                Hi_GetReg(IO_0_DATA_DATA, &value);
                value &= (~IO_0_DATA_SET);
                Hi_SetReg(IO_0_DATA_DATA, value);
            }
            else if(1 == ioNum) {
                Hi_GetReg(IO_1_DATA_DATA, &value);
                value &= (~IO_1_DATA_SET);
                Hi_SetReg(IO_1_DATA_DATA, value);
            }
            else if(2 == ioNum) {
                Hi_GetReg(IO_2_DATA_DATA, &value);
                value &= (~IO_2_DATA_SET);
                Hi_SetReg(IO_2_DATA_DATA, value);

            }
            else if(3 == ioNum) {
                Hi_GetReg(IO_3_DATA_DATA, &value);
                value &= (~IO_3_DATA_SET);
                Hi_SetReg(IO_3_DATA_DATA, value);
            }
        }
        else if(hd_ver == GZ_31DV200) {
            if(0 == ioNum) {
                Hi_GetReg(IO_4_DATA_DATA, &value);
                value &= (~IO_4_DATA_SET);
                Hi_SetReg(IO_4_DATA_DATA, value);
                printf("set io0 down\n");
            }
            else if(1 == ioNum) {
                Hi_GetReg(IO_5_DATA_DATA, &value);
                value &= (~IO_5_DATA_SET);
                Hi_SetReg(IO_5_DATA_DATA, value);
            }

        }
        else if(hd_ver == NJ_A151) {
            if(0 == ioNum) {
                if(outmode)
                {
                    Hi_GetReg(A151OUT_IO_0_DATA_DATA, &value);
                    value &= (~A151OUT_IO_0_DATA_SET);
                    Hi_SetReg(A151OUT_IO_0_DATA_DATA, value);
                    printf("set io0 down(out)\n");
                }
                else
                {
                    Hi_GetReg(A151_IO_0_DATA_DATA, &value);
                    value &= (~A151_IO_0_DATA_SET);
                    Hi_SetReg(A151_IO_0_DATA_DATA, value);
                    printf("set io0 down\n");
                }
            }
            else if(1 == ioNum) {
                if(outmode)
                {
                    Hi_GetReg(A151OUT_IO_1_DATA_DATA, &value);
                    value &= (~A151OUT_IO_1_DATA_SET);
                    Hi_SetReg(A151OUT_IO_1_DATA_DATA, value);
                }
                else
                {
                    Hi_GetReg(A151_IO_1_DATA_DATA, &value);
                    value &= (~A151_IO_1_DATA_SET);
                    Hi_SetReg(A151_IO_1_DATA_DATA, value);
                }
            }

        }
        else if(hd_ver == NJ_A323) {
            if(0 == ioNum) {
                if(outmode)
                {
                    Hi_GetReg(A323_IO_0_DATA_DATA, &value);
                    value &= (~A323_IO_0_DATA_SET);
                    Hi_SetReg(A323_IO_0_DATA_DATA, value);
                    printf("set A323 io0 down\n");
                }
                else
                {
                    Hi_GetReg(A151_IO_0_DATA_DATA, &value);
                    value &= (~A151_IO_0_DATA_SET);
                    Hi_SetReg(A151_IO_0_DATA_DATA, value);
                    printf("set io0 down\n");
                }
            }
            else if(1 == ioNum) {
                if(outmode)
                {
                    Hi_GetReg(A323_IO_1_DATA_DATA, &value);
                    value &= (~A323_IO_1_DATA_SET);
                    Hi_SetReg(A323_IO_1_DATA_DATA, value);
                    printf("set A323 io1 down\n");
                }
                else
                {
                    Hi_GetReg(A151_IO_1_DATA_DATA, &value);
                    value &= (~A151_IO_1_DATA_SET);
                    Hi_SetReg(A151_IO_1_DATA_DATA, value);
                }
            }

        }
#if 0
        else if(hd_ver == NJ_A323) {
            if(0 == ioNum) {
                Hi_GetReg(A323_IO_0_DATA_DATA, &value);
                value &= (~A323_IO_0_DATA_SET);
                Hi_SetReg(A323_IO_0_DATA_DATA, value);
                printf("set A323 io0 down\n");
            }
            else if(1 == ioNum) {
                Hi_GetReg(A323_IO_1_DATA_DATA, &value);
                value &= (~A323_IO_1_DATA_SET);
                Hi_SetReg(A323_IO_1_DATA_DATA, value);
                printf("set A323 io1 down\n");
            }

        }
#endif
        else if(hd_ver == NJ_A186_V1) {
            if(0 == ioNum) {
                Hi_GetReg(IO0_DATA_A186_DATA, &value);
                value &= (~IO0_DATA_A186_SET);
                Hi_SetReg(IO0_DATA_A186_DATA, value);
                printf("set io0 down\n");
            }
            else if(1 == ioNum) {
                Hi_GetReg(IO1_DATA_A186_DATA, &value);
                value &= (~IO1_DATA_A186_SET);
                Hi_SetReg(IO1_DATA_A186_DATA, value);
                printf("set io1 down\n");
            }

        }
        else if(NJ_A335 == hd_ver) {
            if(0 == ioNum) {
                Hi_GetReg(A335_IO0_DATA_0_7_DATA, &value);
                value &= (~A335_IO0_DATA_0_7_SET);
                Hi_SetReg(A335_IO0_DATA_0_7_DATA, value);
                printf("set io0 down\n");
            }
            else if(1 == ioNum) {
                Hi_GetReg(A335_IO1_DATA_13_2_DATA, &value);
                value &= (~A335_IO1_DATA_13_2_SET);
                Hi_SetReg(A335_IO1_DATA_13_2_DATA, value);
                printf("set io1 down\n");
            }
        }
        else if(NJ_A345 == hd_ver) {
            if(0 == ioNum) {
                Hi_GetReg(io_0_set.data, &value);
                value &= (~io_0_set.set);
                Hi_SetReg(io_0_set.data, value);
            }
            else if(1 == ioNum) {
                Hi_GetReg(io_1_set.data, &value);
                value &= (~io_1_set.set);
                Hi_SetReg(io_1_set.data, value);
            }
        }
        else if(GZ16A_100_V1 == hd_ver) {
            if(0 == ioNum) {
                Hi_GetReg(io_0_set.data, &value);
                value &= (~io_0_set.set);
                Hi_SetReg(io_0_set.data, value);
            }
            else if(1 == ioNum) {
                Hi_GetReg(io_1_set.data, &value);
                value &= (~io_1_set.set);
                Hi_SetReg(io_1_set.data, value);
            }
            else if(2 == ioNum) {
                Hi_GetReg(io_2_set.data, &value);
                value &= (~io_2_set.set);
                Hi_SetReg(io_2_set.data, value);
            }
            else if(3 == ioNum) {
                Hi_GetReg(io_3_set.data, &value);
                value &= (~io_3_set.set);
                Hi_SetReg(io_3_set.data, value);
            }
        }
#ifdef __7000__
        Hi_GetReg(GPIO0_7DATA,&value);
        value &= (~GPIO0_7SET);
        Hi_SetReg(GPIO0_7DATA,value);
#endif
    }
    else {
        COMMON_PRT("io set value error\n");
        return  false;
    }

    return true;
}


/*
 * 配合UMP设置，时时生效
 * 全局参数在外部修改，保存
 * */

bool io::UMPsetParam(bool uartFlag,bool ctrlPortFlag,int svPortFlag)
{

    if(ctrlPortFlag) {
        COMMON_PRT("UMP set io ctrl port Param\n");
    }

    if(svPortFlag) {
        FD_CLR(socketIO,&readFd);
        close(socketIO);
        sleep(1);
        if(1 == svPortFlag)
            socketIO = createUdp(global_data.svIOport);         //IO0
        else if(2 == svPortFlag)
            socketIO = createUdp(global_data.svInfraredport);    //IO1
        FD_SET(socketIO,&readFd);
        if(socketIO >= maxFd)
            maxFd = socketIO + 1;
    }

    if(uartFlag) {

    }
    return true;
}
