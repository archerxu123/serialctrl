#include "umpset.h"

umpSet::umpSet()
{

}

umpSet::~umpSet()
{
    FD_CLR(socketUmp,&readFd);
    close(socketUmp);
    socketUmp = -1;
}


bool umpSet::umpIni()
{
    socketUmp = createUdp(UMPPORT);

    AddMemberShip(socketUmp, "224.168.1.1", ETH0);
    FD_SET(socketUmp,&readFd);
    if(socketUmp >= maxFd) {
        maxFd = socketUmp + 1;
    }
    return true;
}

int umpSet::rcvData()
{
    char tmpData[512] = {0};
    int rcvLen = 0;
    char getIp[20];
    socklen_t addrLen = sizeof(struct sockaddr_in);

    memcpy(&umpData,&global_data,sizeof(CONFIGDATA));
    rcvLen = recvfrom(socketUmp,tmpData,512,NULL,(struct sockaddr*)(&umpAddr),&addrLen);
    if(rcvLen > 0) {
        printf("rcv data:%s", tmpData);
        if(!strncmp(tmpData,"reply",strlen("reply"))) {
            if(rcvLen > strlen("reply:")) {
                memset(getIp, 0, sizeof(getIp));
                sscanf(tmpData,"reply:%s",getIp);
                if(!strcmp(getIp, local_ip.c_str())) {
                    mlt_echo = true;
                    return REPLY;
                }
            }
            mlt_echo = false;
            return REPLY;
        }
        else if(!strncmp(tmpData,"del",strlen("del"))) {
            return DEL;
        }
        else {
#ifdef __DEBUG__
            printf("rcv ump set data:\n%s\n",tmpData);
#endif
            if(getUmpData(&umpData,tmpData))
                return PARAM;
            else
                return SAME;
        }
    }
    else {
        COMMON_PRT("rcv ump data error\n");
        return true;
    }
}

bool getP(char **start,char **end,char *data,const char *key)
{
    *start = strstr(data,key);
    if(NULL == *start)
        return false;
    else {
        *end = strchr(*start,0);

        if(NULL == *end)
            return false;
        else {
            return true;
        }
    }
}

bool umpSet::getUmpData(CONFIGDATA *tmp,char *data)
{
    char *start = NULL, *end = NULL;
    char value[8] = {0};

//设置控制端UDP回复地址和对应端口号
    if(getP(&start,&end,data,"kctlip")) {
        memset(tmp->ctlip,0,32);
        start += (strlen("kctlip")+1);
        strncpy(tmp->ctlip,start,end-start);
        if(strncmp(tmp->ctlip,global_data.ctlip,strlen(global_data.ctlip))) {
            return true;
        }
        else {
            return false;
        }
    }


    if(getP(&start,&end,data,"kctl232port")) {
        tmp->ctl232port = 0;
        start += (strlen("kctl232port")+1);
        memset(value,0,8);
        strncpy(value,start,end-start);
        tmp->ctl232port = atoi(value);
        if(tmp->ctl232port != global_data.ctl232port) {
            return true;
        }
        else
            return false;
    }

    if(getP(&start,&end,data,"kctl485port")) {
        tmp->ctl485port = 0;
        start += (strlen("kctl485port")+1);
        memset(value,0,8);
        strncpy(value,start,end-start);
        tmp->ctl485port = atoi(value);
        if(tmp->ctl485port != global_data.ctl485port) {
            return true;
        }
        else
            return false;
    }

    if(getP(&start,&end,data,"kctlrelayport")) {
        tmp->ctlrelayport = 0;
        start += (strlen("kctlrelayport")+1);
        memset(value,0,8);
        strncpy(value,start,end-start);
        tmp->ctlrelayport = atoi(value);
        if(tmp->ctlrelayport != global_data.ctlrelayport) {
            return true;
        }
        else
            return false;
    }

    if(getP(&start,&end,data,"kctlinfraredport")) {
        tmp->ctlinfraredport = 0;
        start += (strlen("kctlinfraredport")+1);
        memset(value,0,8);
        strncpy(value,start,end-start);
        tmp->ctlinfraredport = atoi(value);
        if(tmp->ctlinfraredport != global_data.ctlinfraredport) {
            return true;
        }
        else
            return false;
    }

    if(getP(&start,&end,data,"kctlIO0port")) {
        tmp->ctlIO0port = 0;
        start += (strlen("kctlIO0port")+1);
        memset(value,0,8);
        strncpy(value,start,end-start);
        tmp->ctlIO0port = atoi(value);
        if(tmp->ctlIO0port != global_data.ctlIO0port) {
            return true;
        }
        else
            return false;
    }

    if(getP(&start,&end,data,"kctlIO1port")) {
        tmp->ctlIO1port = 0;
        start += (strlen("kctlIO1port")+1);
        memset(value,0,8);
        strncpy(value,start,end-start);
        tmp->ctlIO1port = atoi(value);
        if(tmp->ctlIO1port != global_data.ctlIO1port) {
            return true;
        }
        else
            return false;
    }

    if(getP(&start,&end,data,"kctlIO2port")) {
        tmp->ctlIO2port = 0;
        start += (strlen("kctlIO2port")+1);
        memset(value,0,8);
        strncpy(value,start,end-start);
        tmp->ctlIO2port = atoi(value);
        if(tmp->ctlIO2port != global_data.ctlIO2port) {
            return true;
        }
        else
            return false;
    }

    if(getP(&start,&end,data,"kctlIO3port")) {
        tmp->ctlIO3port = 0;
        start += (strlen("kctlIO3port")+1);
        memset(value,0,8);
        strncpy(value,start,end-start);
        tmp->ctlIO3port = atoi(value);
        if(tmp->ctlIO3port != global_data.ctlIO3port) {
            return true;
        }
        else
            return false;
    }

//设置本地接收UDP的对应端口号，一般不变
    if(getP(&start,&end,data,"ksv232port")) {
        tmp->sv232port = 0;
        start += (strlen("ksv232port")+1);
        memset(value,0,8);
        strncpy(value,start,end-start);
        tmp->sv232port = atoi(value);
        if(tmp->sv232port != global_data.sv232port) {
            return true;
        }
        else
            return false;
    }

    if(getP(&start,&end,data,"ksv485port")) {
        tmp->sv485port = 0;
        start += (strlen("ksv485port")+1);
        memset(value,0,8);
        strncpy(value,start,end-start);
        tmp->sv485port = atoi(value);
        if(tmp->sv485port != global_data.sv485port) {
            return true;
        }
        else
            return false;
    }

    if(getP(&start,&end,data,"ksvrelayport")) {
        tmp->svrelayport = 0;
        start += (strlen("ksvrelayport")+1);
        memset(value,0,8);
        strncpy(value,start,end-start);
        tmp->svrelayport = atoi(value);
        if(tmp->svrelayport != global_data.svrelayport) {
            return true;
        }
        else
            return false;
    }

    if(getP(&start,&end,data,"ksvinfraredport")) {
        tmp->svInfraredport = 0;
        start += (strlen("ksvinfraredport")+1);
        memset(value,0,8);
        strncpy(value,start,end-start);
        tmp->svInfraredport = atoi(value);
        if(tmp->svInfraredport != global_data.svInfraredport) {
            return true;
        }
        else
            return false;
    }

    if(getP(&start,&end,data,"ksvIOport")) {
        tmp->svIOport = 0;
        start += (strlen("ksvIOport")+1);
        memset(value,0,8);
        strncpy(value,start,end-start);
        tmp->svIOport = atoi(value);
        if(tmp->svIOport != global_data.svIOport ) {
            return true;
        }
        else
            return false;
    }

//设置RS232对应的端口和参数
    if(getP(&start,&end,data,"ken232")) {
        tmp->en232 = 0;
        start += (strlen("ken232")+1);
        memset(value,0,8);
        strncpy(value,start,end-start);
        tmp->en232 = atoi(value);
        if(tmp->en232 != global_data.en232 ) {
            return true;
        }
        else
            return false;
    }

    if(getP(&start,&end,data,"k232port")) {
        memset(tmp->port232,0,8);
        start += (strlen("k232port")+1);
        strncpy(tmp->port232,start,end-start);
        if(strncmp(tmp->port232,global_data.port232,strlen(global_data.port232))) {
            return true;
        }
        else {
            return false;
        }
    }

    if(getP(&start,&end,data,"k232uartspeed")) {
        tmp->uartspeed232 = 0;
        start += (strlen("k232uartspeed")+1);
        memset(value,0,8);
        strncpy(value,start,end-start);
        tmp->uartspeed232 = atol(value);
        if(tmp->uartspeed232 != global_data.uartspeed232 ) {
            return true;
        }
        else
            return false;
    }

    if(getP(&start,&end,data,"k232nbit")) {
        tmp->nbit232 = 0;
        start += (strlen("k232nbit")+1);
        memset(value,0,8);
        strncpy(value,start,end-start);
        tmp->nbit232 = atoi(value);
        if(tmp->nbit232 != global_data.nbit232 ) {
            return true;
        }
        else
            return false;
    }

    if(getP(&start,&end,data,"k232nstop")) {
        tmp->nstop232 = 0;
        start += (strlen("k232nstop")+1);
        memset(value,0,8);
        strncpy(value,start,end-start);
        tmp->nstop232 = atoi(value);
        if(tmp->nstop232 != global_data.nstop232 ) {
            return true;
        }
        else
            return false;
    }

    if(getP(&start,&end,data,"k232event")) {
        memset(tmp->event232,0,4);
        start += (strlen("k232event")+1);
        memset(value,0,8);
        strncpy(value,start,end-start);
        tmp->event232[0] = value[0];
        if(tmp->event232[0] != global_data.event232[0] ) {
            return true;
        }
        else
            return false;
    }

    if(getP(&start,&end,data,"k232vtime")) {
        tmp->nVtime232 = 0;
        start += (strlen("k232vtime")+1);
        memset(value,0,8);
        strncpy(value,start,end-start);
        tmp->nVtime232 = atoi(value);
        if(tmp->nVtime232 != global_data.nVtime232 ) {
            return true;
        }
        else
            return false;
    }

    if(getP(&start,&end,data,"k232vmin")) {
        tmp->nVmin232 = 0;
        start += (strlen("k232vmin")+1);
        memset(value,0,8);
        strncpy(value,start,end-start);
        tmp->nVmin232 = atoi(value);
        if(tmp->nVmin232 != global_data.nVmin232 ) {
            return true;
        }
        else
            return false;
    }

//设置RS485对应端口和参数
    if(getP(&start,&end,data,"ken485")) {
        tmp->en485 = 0;
        start += (strlen("ken485")+1);
        memset(value,0,8);
        strncpy(value,start,end-start);
        tmp->en485 = atoi(value);
        if(tmp->en485 != global_data.en485 ) {
            return true;
        }
        else
            return false;
    }

    if(getP(&start,&end,data,"k485port")) {
        memset(tmp->port485,0,8);
        start += (strlen("k485port")+1);
        strncpy(tmp->port485,start,end-start);
        if(strncmp(tmp->port485,global_data.port485,strlen(global_data.port485))) {
            return true;
        }
        else {
            return false;
        }
    }

    if(getP(&start,&end,data,"k485uartspeed")) {
        tmp->uartspeed485 = 0;
        start += (strlen("k485uartspeed")+1);
        memset(value,0,8);
        strncpy(value,start,end-start);
        tmp->uartspeed485 = atol(value);
        if(tmp->uartspeed485 != global_data.uartspeed485 ) {
            return true;
        }
        else
            return false;
    }

    if(getP(&start,&end,data,"k485nbit")) {
        tmp->nbit485 = 0;
        start += (strlen("k485nbit")+1);
        memset(value,0,8);
        strncpy(value,start,end-start);
        tmp->nbit485 = atoi(value);
        if(tmp->nbit485 != global_data.nbit485 ) {
            return true;
        }
        else
            return false;
    }

    if(getP(&start,&end,data,"k485nstop")) {
        tmp->nstop485 = 0;
        start += (strlen("k485nstop")+1);
        memset(value,0,8);
        strncpy(value,start,end-start);
        tmp->nstop485 = atoi(value);
        if(tmp->nstop485 != global_data.nstop485 ) {
            return true;
        }
        else
            return false;
    }

    if(getP(&start,&end,data,"k485event")) {
        memset(tmp->event485,0,4);
        start += (strlen("k485event")+1);
        memset(value,0,8);
        strncpy(value,start,end-start);
        tmp->event485[0] = value[0];
        if(tmp->event485[0] != global_data.event485[0] ) {
            return true;
        }
        else
            return false;
    }

    if(getP(&start,&end,data,"k485vtime")) {
        tmp->nVtime485 = 0;
        start += (strlen("k485vtime")+1);
        memset(value,0,8);
        strncpy(value,start,end-start);
        tmp->nVtime485 = atoi(value);
        if(tmp->nVtime485 != global_data.nVtime485 ) {
            return true;
        }
        else
            return false;
    }

    if(getP(&start,&end,data,"k485vmin")) {
        tmp->nVmin485 = 0;
        start += (strlen("k485vmin")+1);
        memset(value,0,8);
        strncpy(value,start,end-start);
        tmp->nVmin485 = atoi(value);
        if(tmp->nVmin485 != global_data.nVmin485 ) {
            return true;
        }
        else
            return false;
    }

//设置继电器参数
    if(getP(&start,&end,data,"kenrelay")) {
        tmp->enRelay = 0;
        start += (strlen("kenrelay")+1);
        memset(value,0,8);
        strncpy(value,start,end-start);
        tmp->enRelay = atoi(value);
        if(tmp->enRelay != global_data.enRelay ) {
            return true;
        }
        else
            return false;
    }

//设置红外参数
    if(getP(&start,&end,data,"kenInfrared")) {
        tmp->enInfrared = 0;
        start += (strlen("kenInfrared")+1);
        memset(value,0,8);
        strncpy(value,start,end-start);
        tmp->enInfrared = atoi(value);
        if(tmp->enInfrared != global_data.enInfrared ) {
            return true;
        }
        else
            return false;
    }

    if(getP(&start,&end,data,"kInfraredport")) {
        memset(tmp->portInfrared,0,8);
        start += (strlen("kInfraredport")+1);
        strncpy(tmp->portInfrared,start,end-start);
        if(strncmp(tmp->portInfrared,global_data.portInfrared,strlen(global_data.portInfrared))) {
            return true;
        }
        else {
            return false;
        }
    }

    if(getP(&start,&end,data,"kInfrareduartspeed")) {
        tmp->uartspeedInf = 0;
        start += (strlen("kInfrareduartspeed")+1);
        memset(value,0,8);
        strncpy(value,start,end-start);
        tmp->uartspeedInf = atol(value);
        if(tmp->uartspeedInf != global_data.uartspeedInf ) {
            return true;
        }
        else
            return false;
    }

    if(getP(&start,&end,data,"kInfrarednbit")) {
        tmp->nbitInf = 0;
        start += (strlen("kInfrarednbit")+1);
        memset(value,0,8);
        strncpy(value,start,end-start);
        tmp->nbitInf = atoi(value);
        if(tmp->nbitInf != global_data.nbitInf ) {
            return true;
        }
        else
            return false;
    }

    if(getP(&start,&end,data,"kInfrarednstop")) {
        tmp->nstopInf = 0;
        start += (strlen("kInfrarednstop")+1);
        memset(value,0,8);
        strncpy(value,start,end-start);
        tmp->nstopInf = atoi(value);
        if(tmp->nstopInf != global_data.nstopInf ) {
            return true;
        }
        else
            return false;
    }

    if(getP(&start,&end,data,"kInfraredevent")) {
        memset(tmp->eventInf,0,4);
        start += (strlen("kInfraredevent")+1);
        memset(value,0,8);
        strncpy(value,start,end-start);
        tmp->eventInf[0] = value[0];
        if(tmp->eventInf[0] != global_data.eventInf[0] ) {
            return true;
        }
        else
            return false;
    }

    if(getP(&start,&end,data,"kInfraredvtime")) {
        tmp->nVtimeInf = 0;
        start += (strlen("kInfraredvtime")+1);
        memset(value,0,8);
        strncpy(value,start,end-start);
        tmp->nVtimeInf = atoi(value);
        if(tmp->nVtimeInf != global_data.nVtimeInf ) {
            return true;
        }
        else
            return false;
    }

    if(getP(&start,&end,data,"kInfraredvmin")) {
        tmp->nVminInf = 0;
        start += (strlen("kInfraredvmin")+1);
        memset(value,0,8);
        strncpy(value,start,end-start);
        tmp->nVminInf = atoi(value);
        if(tmp->nVminInf != global_data.nVminInf ) {
            return true;
        }
        else
            return false;
    }

//设置IO参数
    if(getP(&start,&end,data,"kSetIO_0")) {
        tmp->enIo_0 = 0;
        start += (strlen("kSetIO_0")+1);
        memset(value,0,8);
        strncpy(value,start,end-start);
        tmp->enIo_0 = atoi(value);
        if(tmp->enIo_0 != global_data.enIo_0 ) {
            return true;
        }
        else
            return false;
    }

    if(getP(&start,&end,data,"kSetIO_1")) {
        tmp->enIo_1 = 0;
        start += (strlen("kSetIO_1")+1);
        memset(value,0,8);
        strncpy(value,start,end-start);
        tmp->enIo_1 = atoi(value);
        if(tmp->enIo_1 != global_data.enIo_1 ) {
            return true;
        }
        else
            return false;
    }

    if(getP(&start,&end,data,"kSetIO_2")) {
        tmp->enIo_2 = 0;
        start += (strlen("kSetIO_2")+1);
        memset(value,0,8);
        strncpy(value,start,end-start);
        tmp->enIo_2 = atoi(value);
        if(tmp->enIo_2 != global_data.enIo_2 ) {
            return true;
        }
        else
            return false;
    }

    if(getP(&start,&end,data,"kSetIO_3")) {
        tmp->enIo_3 = 0;
        start += (strlen("kSetIO_3")+1);
        memset(value,0,8);
        strncpy(value,start,end-start);
        tmp->enIo_3 = atoi(value);
        if(tmp->enIo_3 != global_data.enIo_3 ) {
            return true;
        }
        else
            return false;
    }

//设置TCP参数
    if(getP(&start,&end,data,"kTcpEnable")) {
        tmp->tcp_en = 0;
        start += (strlen("kTcpEnable")+1);
        memset(value,0,8);
        strncpy(value,start,end-start);
        tmp->tcp_en = atoi(value);
        if(tmp->tcp_en != global_data.tcp_en ) {
            return true;
        }
        else
            return false;
    }

    if(getP(&start,&end,data,"kTcpServerPort")) {
        tmp->tcp_port = 0;
        start += (strlen("kTcpServerPort")+1);
        memset(value,0,8);
        strncpy(value,start,end-start);
        tmp->tcp_port = atoi(value);
        if(tmp->tcp_port != global_data.tcp_port ) {
            return true;
        }
        else
            return false;
    }

    if(getP(&start,&end,data,"kTcpLinkMax")) {
        tmp->tcp_link_max = 0;
        start += (strlen("kTcpLinkMax")+1);
        memset(value,0,8);
        strncpy(value,start,end-start);
        tmp->tcp_link_max = atoi(value);
        if(tmp->tcp_link_max != global_data.tcp_link_max ) {
            return true;
        }
        else
            return false;
    }
    return true;
}



bool umpSet::relyUMP()
{
    FILE *file;
    char data[1024] = {0};
    memset(data,0,1024);
    int sendNum = 0, readnum = 0;

    if(NULL == (file = fopen("/data/remserial.ini","r"))) {
        printf("can't fopen ini file\n");
        return false;
    }

    for(int i = 0; i < 1024; i++) {
        if(0 == (readnum = fread(data+i,1,1,file)))
            break;
        else
            printf("%c ",data[i]);
    }
    fclose(file);
    if(mlt_echo) {
        umpAddr.sin_family 		= AF_INET;
        umpAddr.sin_port   		= htons(4001);
        umpAddr.sin_addr.s_addr 	= inet_addr("224.168.1.1");
    }

    sendNum = sendto(socketUmp,data,strlen(data),0,(struct sockaddr*)&umpAddr,sizeof(struct sockaddr_in));


    if(sendNum <= 0 ) {
        return false;
    }

    return true;
}
