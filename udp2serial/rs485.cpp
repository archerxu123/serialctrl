#include "rs485.h"

rs485::rs485()
{

}

rs485::~rs485()
{
    FD_CLR(dev485Fd,&readFd);
    FD_CLR(socket485,&readFd);
    close(dev485Fd);
    close(socket485);
    close(time_fd);
    system("rmmod mytimer.ko");
    dev485Fd = -1;
    socket485 = -1;
}

bool rs485::iniRs485()
{
    uint value;

    if(hd_ver == GZ_700S) {
        Hi_SetReg(GPIO7_3REG, 0x0);

        Hi_GetReg(GPIO7_3DIR,&value);
        value |= GPIO7_3SET;
        Hi_SetReg(GPIO7_3DIR,value);

        Hi_GetReg(GPIO7_3DATA,&value);
        value &= (~GPIO7_3SET);
        Hi_SetReg(GPIO7_3DATA,value);
    }
    else if(hd_ver == NJ_31DV200) {
        Hi_GetReg(V200_GPIO3_0REG, &value);
        value &= (~0x1);
        Hi_SetReg(V200_GPIO3_0REG, value);

        Hi_GetReg(V200_GPIO3_0DIR, &value);
        value |= V200_GPIO3_0SET;
        Hi_SetReg(V200_GPIO3_0DIR, value);

        Hi_GetReg(V200_GPIO3_0DATA, &value);
        value &= (~V200_GPIO3_0SET);
        Hi_SetReg(V200_GPIO3_0DATA, value);
    }
    else if(hd_ver == NJ_A304) {
        Hi_GetReg(A304_GPIO20_4_REG, &value);
        value |= (0x3);
        Hi_SetReg(A304_GPIO20_4_REG, value);

        Hi_GetReg(A304_GPIO20_4_DIR, &value);
        value |= A304_GPIO20_4_SET;
        Hi_SetReg(A304_GPIO20_4_DIR, value);

        Hi_GetReg(A304_GPIO20_4_DATA, &value);
        value &= (~A304_GPIO20_4_SET);
        Hi_SetReg(A304_GPIO20_4_DATA, value);
        printf("A304-----485init\n");
    }
    else if(NJ_A335 == hd_ver) {
        uint value;
        Hi_SetReg(A335_13_4_MUX, 0x0);
        Hi_GetReg(A335_13_4_DIR, &value);
        value |= A335_13_4_SET;
        Hi_SetReg(A335_13_4_DIR, value);
        Hi_GetReg(A335_13_4_DATA, &value);
        value &= (~A335_13_4_SET);
        Hi_SetReg(A335_13_4_DATA, value);

        Hi_SetReg(A335_5_1_REG, 0x0);
        Hi_GetReg(A335_5_1_DIR, &value);
        value |= A335_5_1_SET;
        Hi_SetReg(A335_5_1_DIR, value);
        printf("---------2\n");
        Hi_GetReg(A335_5_1_DATA, &value);
        value &= (~A335_5_1_SET);
        Hi_SetReg(A335_5_1_DATA, value);
        printf("---------3\n");

    }
    else if(GZ36_200_V1 == hd_ver) {
        ctrl.dir = 0x121C0400;
        ctrl.data = 0x121C0020;
        ctrl.set = 0x8;
        Hi_GetReg(ctrl.dir, &value);
        value |= ctrl.set;
        Hi_SetReg(ctrl.dir, value);

        Hi_GetReg(ctrl.data, &value);
        value &= (~ctrl.set);
        Hi_SetReg(ctrl.data, value);
        uart_addr = 0x120A0018;
    }
    else {
        Hi_SetReg(GPIO4_5REG,0x1);

        Hi_GetReg(GPIO4_5DIR,&value);
        value |= GPIO4_5SET;
        Hi_SetReg(GPIO4_5DIR,value);

        Hi_GetReg(GPIO4_5DATA,&value);
        value &= (~GPIO4_5SET);
        Hi_SetReg(GPIO4_5DATA,value);
    }


#ifdef __SEND_485__
    Hi_GetReg(GPIO4_5DATA,&value);
    value |= GPIO4_5SET;
    Hi_SetReg(GPIO4_5DATA,value);

#endif

    open_port(global_data.port485,&dev485Fd);
    set_optBak(global_data.uartspeed485,global_data.nbit485,global_data.nstop485,   \
               global_data.event485[0],global_data.nVtime485,global_data.nVmin485,&dev485Fd);

    FD_SET(dev485Fd,&readFd);
    if(dev485Fd >= maxFd)
        maxFd = dev485Fd + 1;
    socket485 = createUdp(global_data.sv485port);
    FD_SET(socket485,&readFd);
    if(socket485 >= maxFd)
        maxFd = socket485 + 1;
    if(GZ36_200_V1 != hd_ver) {
        system("insmod mytimer.ko");
        sleep(1);
        time_fd = open(dev_path, O_RDWR|O_TRUNC);
        if(time_fd < 0) {
            perror("open dev_path error");
        }
    }


    if(GZ_700K == hd_ver || NJ_700K == hd_ver || GZ_8000 == hd_ver || NJ_D_700K == hd_ver) {
        uart_addr = 0x120A0018;
    }

    return true;
}

bool rs485::rcvData2uart()
{
    char tmpData[128] = {0};
    int rcvLen = 0;
    socklen_t addrLen = 0;
    uint value;
    memset(tmpData,0,sizeof(tmpData));

    addrLen = sizeof(struct sockaddr_in);

    rcvLen = recvfrom(socket485,tmpData,MAXDATA,0,(struct sockaddr*)(&ctrlAddr),&addrLen);
    if(rcvLen > 0) {
#ifdef __DEBUG__
    print0x(tmpData,rcvLen);
#endif

#ifndef __SEND_485__
        if(hd_ver == GZ_700S) {
            Hi_GetReg(GPIO7_3DATA, &value);
            value |= GPIO7_3SET;
            Hi_SetReg(GPIO7_3DATA, value);
            usleep(50000);
        }
        else if(hd_ver == NJ_31DV200) {
            Hi_GetReg(V200_GPIO3_0DATA, &value);
            value |= V200_GPIO3_0SET;
            Hi_SetReg(V200_GPIO3_0DATA, value);
            usleep(50000);
        }
        else if(hd_ver == NJ_A304) {
            Hi_GetReg(A304_GPIO20_4_DATA, &value);
            value |= A304_GPIO20_4_SET;
            Hi_SetReg(A304_GPIO20_4_DATA, value);
            printf("A304 485 send ready\n");
            usleep(50000);
        }
        else if(NJ_A335 == hd_ver) {
            Hi_GetReg(A335_5_1_DATA, &value);
            value |= A335_5_1_SET;
            Hi_SetReg(A335_5_1_DATA, value);
            printf("A335 485 send ready\n");
            usleep(50000);
        }
        else if(GZ36_200_V1 == hd_ver) {
            Hi_GetReg(ctrl.data, &value);
            value |= ctrl.set;
            Hi_SetReg(ctrl.data, value);
            usleep(50000);
        }
        else {
            Hi_GetReg(GPIO4_5DATA,&value);
            value |= GPIO4_5SET;
            Hi_SetReg(GPIO4_5DATA,value);
            usleep(50000);
        }

        write(dev485Fd,tmpData,rcvLen);
        int hz = 200;
        int ret;
        char buf[4];

        uint value_bit ;
        struct timeval start,stop;
        do {
            gettimeofday(&start, 0);
            if(time_fd >= 0) {
                ret = write(time_fd, &hz, sizeof(hz));
                ret = read(time_fd, buf, sizeof(buf));
            }
            else {
                usleep(1000);
            }
            gettimeofday(&stop, 0);
            printf("start:%ld us, stop:%ld us\n", start.tv_usec, stop.tv_usec);
            Hi_GetReg(uart_addr, &value_bit);
        }
        while ( (value_bit&0x88) != 0x80  );

        if(hd_ver == GZ_700S) {
            Hi_GetReg(GPIO7_3DATA, &value);
            value &= (~GPIO7_3SET);
            Hi_SetReg(GPIO7_3DATA, value);
        }
        else if(hd_ver == NJ_31DV200) {
            Hi_GetReg(V200_GPIO3_0DATA, &value);
            value &= (~V200_GPIO3_0SET);
            Hi_SetReg(V200_GPIO3_0DATA, value);
        }
        else if(hd_ver == NJ_A304) {
            Hi_GetReg(A304_GPIO20_4_DATA, &value);
            value &= (~A304_GPIO20_4_SET);
            Hi_SetReg(A304_GPIO20_4_DATA, value);
            printf("A304 485 recive ready\n");
        }
        else if(NJ_A335 == hd_ver) {
            Hi_GetReg(A335_5_1_DATA, &value);
            value &= (~A335_5_1_SET);
            Hi_SetReg(A335_5_1_DATA, value);
            printf("A335 485 send ready\n");
        }
        else if(GZ36_200_V1 == hd_ver) {
            Hi_GetReg(ctrl.data, &value);
            value &= (~ctrl.set);
            Hi_SetReg(ctrl.data, value);
        }
        else {
            Hi_GetReg(GPIO4_5DATA,&value);
            value &= (~GPIO4_5SET);
            Hi_SetReg(GPIO4_5DATA,value);
        }

#else
        write(dev485Fd,tmpData,rcvLen);
#endif
    }
    else {
        COMMON_PRT("net not rcv 485data\n");
        return false;
    }
    return true;
}


bool rs485::readUart2ctrl()
{
    char tmpData[128] = {0};
    int rdLen = 0, sendLen = 0;
    memset(tmpData,0,sizeof(tmpData));
    int addrLen = 0;

    rdLen = read(dev485Fd,tmpData,MAXDATA);
#ifdef __DEBUG__
    print0x(tmpData,rdLen);
#endif
    if(rdLen > 0) {
        if((global_data.ctl485port != 0) && (strlen(global_data.ctlip) >= 7)) {
            ctrlAddr.sin_family = AF_INET;
            ctrlAddr.sin_port = htons(global_data.ctl485port);
            ctrlAddr.sin_addr.s_addr = inet_addr(global_data.ctlip);
            addrLen = sizeof(struct sockaddr_in);
            sendLen = sendto(socket485,tmpData,rdLen,0,(struct sockaddr*)&ctrlAddr,addrLen);
            if(sendLen > 0)
                return true;
            else {
                perror("send dev 485 data to ctrl error\n");
                return false;
            }
        }
        else {
            COMMON_PRT("UMP not set ctrl IP PORT\n");
            return true;
        }
    }
    else {
        COMMON_PRT("RS485 dev not read data\n");
        return false;
    }
    return true;
}

/*
 * 配合UMP设置，时时生效
 * 全局参数在外部修改，保存
 * */

bool rs485::UMPsetParam(bool uartFlag,bool ctrlPortFlag,bool svPortFlag)
{
    if(uartFlag) {
        COMMON_PRT("UMP set RS485 UART Param\n");
        FD_CLR(dev485Fd,&readFd);
        close(dev485Fd);
        sleep(1);

        dev485Fd = -1;
        open_port(global_data.port485,&dev485Fd);
        set_optBak(global_data.uartspeed485,global_data.nbit485,global_data.nstop485,   \
                   global_data.event485[0],global_data.nVtime485,global_data.nVmin485,&dev485Fd);
        FD_SET(dev485Fd,&readFd);
        if(dev485Fd >= maxFd)
            maxFd = dev485Fd + 1;

    }

    if(ctrlPortFlag) {
        COMMON_PRT("UMP set RS485 ctrl port Param\n");
    }

    if(svPortFlag) {
        FD_CLR(socket485,&readFd);
        close(socket485);
        sleep(1);

        socket485 = createUdp(global_data.sv485port);
        FD_SET(socket485,&readFd);
        if(socket485 >= maxFd)
            maxFd = socket485 + 1;
    }
    return true;
}
