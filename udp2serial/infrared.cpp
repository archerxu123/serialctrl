#include "infrared.h"

infrared::infrared()
{

}

infrared::~infrared()
{
    FD_CLR(socketInf,&readFd);
    close(socketInf);
    close(devInfFd);
    socketInf = -1;
    devInfFd = -1;
    if(hd_ver == NJ_700K) {
        system("rmmod gpio_ir_timer_nj.ko");
    }
    else {
        system("rmmod gpio_ir_timer.ko");
    }

}

bool infrared::infraredIni(int irModel)
{
    uint value;
    if(irTime) {
        if(hd_ver == NJ_700K) {
            system("insmod gpio_ir_timer_nj.ko");
        }
        else if(hd_ver == NJ_A151)
            {
                if(outmode){
                    system("insmod gpio_ir_timer.ko mode='out'");
                    printf("chose out ko\n");
                }
                else{
                    system("insmod gpio_ir_timer.ko mode='in'");
                    printf("chose in ko\n");
                }
            }
        else if(hd_ver == NJ_A323)
            {
                if(outmode){
                    system("insmod gpio_ir_timer.ko mode='out'");
                    printf("chose out ko\n");
                }
                else{
                    system("insmod gpio_ir_timer.ko mode='in'");
                    printf("chose in ko\n");
                }
            }
        else {
            system("insmod gpio_ir_timer.ko");
        }
        sleep(1);

        if(1 == irModel) {          //红外为发射状态
            if(hd_ver == NJ_A186_V1) {
                Hi_GetReg(IO1_DIR_A186_DIR, &value);  //IO_1发送  应用层拉DIR即可
                value |= IO1_DIR_A186_SET;
                Hi_SetReg(IO1_DIR_A186_DIR, value);

                Hi_GetReg(IO1_DIR_A186_DATA, &value);
                value |= IO1_DIR_A186_SET;
                Hi_SetReg(IO1_DIR_A186_DATA, value);
                printf("----ir send-----\n");
            }
        }
        else if(2 == irModel) {     //红外学习状态，需要IO_0置为输出高电平，供电
            if(hd_ver == NJ_A186_V1) {

                Hi_GetReg(IO1_DIR_A186_DIR, &value);  //IO_1接收
                value |= IO1_DIR_A186_SET;
                Hi_SetReg(IO1_DIR_A186_DIR, value);

                Hi_GetReg(IO1_DIR_A186_DATA, &value);
                value &= (~IO1_DIR_A186_SET);
                Hi_SetReg(IO1_DIR_A186_DATA, value);


                Hi_GetReg(IO0_DIR_A186_DIR, &value); //IO_0拉高供电
                value |= IO0_DIR_A186_SET;
                Hi_SetReg(IO0_DIR_A186_DIR, value);

                Hi_SetReg(IO0_DATA_A186_MUX, 0x0);

                Hi_GetReg(IO0_DIR_A186_DATA, &value);
                value |= IO0_DIR_A186_SET;
                Hi_SetReg(IO0_DIR_A186_DATA, value);

                Hi_GetReg(IO0_DATA_A186_DIR, &value);
                value |= IO0_DATA_A186_SET;
                Hi_SetReg(IO0_DATA_A186_DIR, value);

                Hi_GetReg(IO0_DATA_A186_DATA, &value);
                value |= IO0_DATA_A186_SET;
                Hi_SetReg(IO0_DATA_A186_DATA, value);
                printf("----ir recive-----\n");
            }
        }
#ifdef __21D_core__
        if((1 == irModel) && (hd_ver == NJ_A151))  //红外发送  设置DIR整体方向输出
         {
            if(outmode)
                {
                    Hi_GetReg(A151OUT_IO_1_DIR_REG,&value);
                    value &= (~0x1);
                    Hi_SetReg(A151OUT_IO_1_DIR_REG, value);

                    Hi_GetReg(A151OUT_IO_1_DIR_DIR, &value);
                    value |= A151OUT_IO_1_DIR_SET;
                    Hi_SetReg(A151OUT_IO_1_DIR_DIR, value);

                    Hi_GetReg(A151OUT_IO_1_DIR_DATA, &value);
                    value |= A151OUT_IO_1_DIR_SET;
                    Hi_SetReg(A151OUT_IO_1_DIR_DATA, value);
                }
            else
                {
                    Hi_GetReg(A151_IO_1_DIR_REG,&value);
                    value &= (~0x1);
                    Hi_SetReg(A151_IO_1_DIR_REG, value);  //设置DIR为io模式

                    Hi_GetReg(A151_IO_1_DIR_DIR, &value);
                    value |= A151_IO_1_DIR_SET;
                    Hi_SetReg(A151_IO_1_DIR_DIR, value); //设置控制方向的io(DIR)方向为输出，写入数据来控制整体方向

                    Hi_GetReg(A151_IO_1_DIR_DATA, &value);
                    value |= A151_IO_1_DIR_SET;
                    Hi_SetReg(A151_IO_1_DIR_DATA, value);
                }
         }
        if((2 == irModel) && (hd_ver == NJ_A151))  //红外学习    设置DIR整体方向输入
         {
            if(outmode)
                {
                    Hi_GetReg(A151OUT_IO_1_DIR_REG,&value);
                    value &= (~0x1);
                    Hi_SetReg(A151OUT_IO_1_DIR_REG, value);

                    Hi_GetReg(A151OUT_IO_1_DIR_DIR, &value);
                    value |= A151OUT_IO_1_DIR_SET;
                    Hi_SetReg(A151OUT_IO_1_DIR_DIR, value);


                    Hi_GetReg(A151OUT_IO_1_DIR_DATA, &value);
                    value &= (~A151OUT_IO_1_DIR_SET);
                    Hi_SetReg(A151OUT_IO_1_DIR_DATA, value);

                    //学习时io_0拉高供电
                    Hi_GetReg(A151OUT_IO_0_DIR_REG,&value);
                    value &= (~0x1);
                    Hi_SetReg(A151OUT_IO_0_DIR_REG, value);

                    Hi_GetReg(A151OUT_IO_0_DATA_REG, &value);
                    value &= (~0x1);
                    Hi_SetReg(A151OUT_IO_0_DATA_REG, value);//设置IO 0 DATA接口为IO模式

                    Hi_GetReg(A151OUT_IO_0_DIR_DIR, &value);
                    value |= A151OUT_IO_0_DIR_SET;
                    Hi_SetReg(A151OUT_IO_0_DIR_DIR, value);

                    Hi_GetReg(A151OUT_IO_0_DIR_DATA, &value);
                    value |= A151OUT_IO_0_DIR_SET;
                    Hi_SetReg(A151OUT_IO_0_DIR_DATA, value);//DIR拉高 写1 设置整体方向为输出

                    Hi_GetReg(A151OUT_IO_0_DATA_DIR, &value);
                    value |= A151OUT_IO_0_DATA_SET;
                    Hi_SetReg(A151OUT_IO_0_DATA_DIR, value);//设置io 0口方向为输出，可向io 0口gpio9_4写数据

                    Hi_GetReg(A151OUT_IO_0_DATA_DATA, &value);
                    value |= A151OUT_IO_0_DATA_SET;
                    Hi_SetReg(A151OUT_IO_0_DATA_DATA, value);
                }
            else
                {
                    Hi_GetReg(A151_IO_1_DIR_REG,&value);
                    value &= (~0x1);
                    Hi_SetReg(A151_IO_1_DIR_REG, value);  //设置DIR为io模式

                    Hi_GetReg(A151_IO_1_DIR_DIR, &value);
                    value |= A151_IO_1_DIR_SET;
                    Hi_SetReg(A151_IO_1_DIR_DIR, value); //设置控制方向的io(DIR)方向为输出，写入数据来控制整体方向

                    Hi_GetReg(A151_IO_1_DIR_DATA, &value);
                    value &= (~A151_IO_1_DIR_SET);
                    Hi_SetReg(A151_IO_1_DIR_DATA, value);

                    //学习时io_0拉高供电    输入板DATA数据口不需要复用 只复用DIR方向口即可
                    Hi_GetReg(A151_IO_0_DIR_REG,&value);
                    value &= (~0x1);
                    Hi_SetReg(A151_IO_0_DIR_REG, value);

                    Hi_GetReg(A151_IO_0_DIR_DIR, &value);
                    value |= A151_IO_0_DIR_SET;
                    Hi_SetReg(A151_IO_0_DIR_DIR, value);

                    Hi_GetReg(A151_IO_0_DIR_DATA, &value);
                    value |= A151_IO_0_DIR_SET;
                    Hi_SetReg(A151_IO_0_DIR_DATA, value);//DIR拉高 写1 设置整体方向为输出

                    Hi_GetReg(A151_IO_0_DATA_DIR, &value);
                    value |= A151_IO_0_DATA_SET;
                    Hi_SetReg(A151_IO_0_DATA_DIR, value);//设置io 0口方向为输出，可向io 0口gpio0_5写数据

                    Hi_GetReg(A151_IO_0_DATA_DATA, &value);
                    value |= A151_IO_0_DATA_SET;
                    Hi_SetReg(A151_IO_0_DATA_DATA, value);
                }
         }

#endif

#ifdef __21D_core__A323
        if((1 == irModel) && (hd_ver == NJ_A323))  //红外发送  设置DIR整体方向输出
         {
            if(outmode)
                {

//                    Hi_GetReg(A323_IO_0_DIR_DIR, &value);
//                    value |= A323_IO_0_DIR_SET;
//                    Hi_SetReg(A323_IO_0_DIR_DIR, value);

//                    Hi_GetReg(A323_IO_0_DIR_DATA, &value);
//                    value |= A323_IO_0_DIR_SET;
//                    Hi_SetReg(A323_IO_0_DIR_DATA, value);

                //IO_1做红外口默认就是输入 不能做红外发送
                }
            else
                {
                    Hi_GetReg(A151_IO_1_DIR_REG,&value);
                    value &= (~0x1);
                    Hi_SetReg(A151_IO_1_DIR_REG, value);  //设置DIR为io模式

                    Hi_GetReg(A151_IO_1_DIR_DIR, &value);
                    value |= A151_IO_1_DIR_SET;
                    Hi_SetReg(A151_IO_1_DIR_DIR, value); //设置控制方向的io(DIR)方向为输出，写入数据来控制整体方向

                    Hi_GetReg(A151_IO_1_DIR_DATA, &value);
                    value |= A151_IO_1_DIR_SET;
                    Hi_SetReg(A151_IO_1_DIR_DATA, value);
                }
         }
        if((2 == irModel) && (hd_ver == NJ_A323))  //红外学习    设置DIR整体方向输入
         {
            if(outmode)
                {
                  /***************IO_0做红外口**************/
//                    Hi_GetReg(A323_IO_0_DIR_DIR, &value);
//                    value |= A323_IO_0_DIR_SET;
//                    Hi_SetReg(A323_IO_0_DIR_DIR, value);


//                    Hi_GetReg(A323_IO_0_DIR_DATA, &value);
//                    value &= (~A323_IO_0_DIR_SET);
//                    Hi_SetReg(A323_IO_0_DIR_DATA, value);
                 //学习时io_1拉高供电  拉不高 默认输入

//                Hi_GetReg(A323_IO_1_DIR_DIR, &value);
//                value |= A323_IO_1_DIR_SET;
//                Hi_SetReg(A323_IO_1_DIR_DIR, value);


//                Hi_GetReg(A323_IO_1_DIR_DATA, &value);
//                value |= A323_IO_1_DIR_SET;
//                Hi_SetReg(A323_IO__DIR_DATA, value);


//                Hi_GetReg(A323_IO_1_DATA_DIR, &value);
//                value |= A323_IO_1_DATA_SET;
//                Hi_SetReg(A323_IO_1_DATA_DIR, value);//设置io 0口方向为输出，

//                Hi_GetReg(A323_IO_1_DATA_DATA, &value);
//                value |= A323_IO_1_DATA_SET;
//                Hi_SetReg(A323_IO_1_DATA_DATA, value);
                 /****************************************/


                  /***************IO_1做红外口**************/
                //io_1默认输入 无需操作
                 //学习时io_0拉高供电
                    Hi_GetReg(A323_IO_0_DIR_DIR, &value);
                    value |= A323_IO_0_DIR_SET;
                    Hi_SetReg(A323_IO_0_DIR_DIR, value);


                    Hi_GetReg(A323_IO_0_DIR_DATA, &value);
                    value |= A323_IO_0_DIR_SET;
                    Hi_SetReg(A323_IO_0_DIR_DATA, value);

                    Hi_GetReg(A323_IO_0_DATA_DIR, &value);
                    value |= A323_IO_0_DATA_SET;
                    Hi_SetReg(A323_IO_0_DATA_DIR, value);//设置io 0口方向为输出，

                    Hi_GetReg(A323_IO_0_DATA_DATA, &value);
                    value |= A323_IO_0_DATA_SET;
                    Hi_SetReg(A323_IO_0_DATA_DATA, value);
                }
            else
                {
                    Hi_GetReg(A151_IO_1_DIR_REG,&value);
                    value &= (~0x1);
                    Hi_SetReg(A151_IO_1_DIR_REG, value);  //设置DIR为io模式

                    Hi_GetReg(A151_IO_1_DIR_DIR, &value);
                    value |= A151_IO_1_DIR_SET;
                    Hi_SetReg(A151_IO_1_DIR_DIR, value); //设置控制方向的io(DIR)方向为输出，写入数据来控制整体方向

                    Hi_GetReg(A151_IO_1_DIR_DATA, &value);
                    value &= (~A151_IO_1_DIR_SET);
                    Hi_SetReg(A151_IO_1_DIR_DATA, value);

                    //学习时io_0拉高供电    输入板DATA数据口不需要复用 只复用DIR方向口即可
                    Hi_GetReg(A151_IO_0_DIR_REG,&value);
                    value &= (~0x1);
                    Hi_SetReg(A151_IO_0_DIR_REG, value);

                    Hi_GetReg(A151_IO_0_DIR_DIR, &value);
                    value |= A151_IO_0_DIR_SET;
                    Hi_SetReg(A151_IO_0_DIR_DIR, value);

                    Hi_GetReg(A151_IO_0_DIR_DATA, &value);
                    value |= A151_IO_0_DIR_SET;
                    Hi_SetReg(A151_IO_0_DIR_DATA, value);//DIR拉高 写1 设置整体方向为输出

                    Hi_GetReg(A151_IO_0_DATA_DIR, &value);
                    value |= A151_IO_0_DATA_SET;
                    Hi_SetReg(A151_IO_0_DATA_DIR, value);//设置io 0口方向为输出，可向io 0口gpio0_5写数据

                    Hi_GetReg(A151_IO_0_DATA_DATA, &value);
                    value |= A151_IO_0_DATA_SET;
                    Hi_SetReg(A151_IO_0_DATA_DATA, value);
                }
         }

#endif

#ifdef __7000__
        Hi_GetReg(GPIO0_6DIR,&value);
        value |= GPIO0_6SET;
        Hi_SetReg(GPIO0_6DIR,value);

        Hi_GetReg(GPIO0_6DATA,&value);
        value |= GPIO0_6SET;
        Hi_SetReg(GPIO0_6DATA,value);
#endif

#ifdef  __7000_NJ__
        if(hd_ver ==PTN_21A) {
            Hi_SetReg(IO_1_5_6_MUX, 0x1);
            Hi_SetReg(IO_DIR_11_6_MUX, 0x0);

            Hi_GetReg(IO_DIR_11_6_DIR, &value);
            value |= IO_DIR_11_6_SET;
            Hi_SetReg(IO_DIR_11_6_DIR, value);
            if(1 == irModel){
                Hi_GetReg(IO_DIR_11_6_DATA, &value);
                value |= IO_DIR_11_6_SET;
                Hi_SetReg(IO_DIR_11_6_DATA, value);

                Hi_GetReg(IO_1_5_6_DIR, &value);
                value |= IO_1_5_6_SET;
                Hi_SetReg(IO_1_5_6_DIR, value);

            }
            else if(2 ==  irModel) {
                Hi_GetReg(IO_DIR_11_6_DATA, &value);
                value &= (~IO_DIR_11_6_SET);
                Hi_SetReg(IO_DIR_11_6_DATA, value);

                Hi_GetReg(IO_1_5_6_DIR, &value);
                value &= (~IO_1_5_6_SET);
                Hi_SetReg(IO_1_5_6_DIR, value);

                Hi_SetReg(IO_0_10_2_MUX, 0x0);
                Hi_SetReg(IO_DIR_11_3_MUX, 0x0);

                Hi_GetReg(IO_DIR_11_3_DIR, &value);
                value |= IO_DIR_11_3_SET;
                Hi_SetReg(IO_DIR_11_3_DIR, value);

                Hi_GetReg(IO_DIR_11_3_DATA, &value);
                value |= IO_DIR_11_3_SET;
                Hi_SetReg(IO_DIR_11_3_DATA, value);

                Hi_GetReg(IO_0_10_2_DIR, &value);
                value |= IO_0_10_2SET;
                Hi_SetReg(IO_0_10_2_DIR, value);

                Hi_GetReg(IO_0_10_2_DATA, &value);
                value |= IO_0_10_2SET;
                Hi_SetReg(IO_0_10_2_DATA, value);
            }
        }
        else {
            if(1 == irModel) {          //红外为发射状态
                Hi_GetReg(GPIO0_4DIR,&value);
                value |= GPIO0_4SET;
                Hi_SetReg(GPIO0_4DIR,value);

                Hi_GetReg(GPIO0_4DATA,&value);
                value |= GPIO0_4SET;
                Hi_SetReg(GPIO0_4DATA,value);
            }
            else if(2 == irModel) {     //红外学习状态，需要IO置为输出高电平，供电
                Hi_SetReg(GPIO13_5MUX,0x0);

                Hi_GetReg(GPIO13_5DIR,&value);
                value |= GPIO13_5SET;
                Hi_SetReg(GPIO13_5DIR,value);

                Hi_GetReg(GPIO13_5DATA,&value);
                value |= GPIO13_5SET;
                Hi_SetReg(GPIO13_5DATA,value);

                Hi_GetReg(GPIO0_6DIR,&value);
                value |= GPIO0_6SET;
                Hi_SetReg(GPIO0_6DIR,value);

                Hi_GetReg(GPIO0_6DATA,&value);
                value |= GPIO0_6SET;
                Hi_SetReg(GPIO0_6DATA,value);


                Hi_GetReg(GPIO0_4DIR,&value);
                value |= GPIO0_4SET;
                Hi_SetReg(GPIO0_4DIR,value);

                Hi_GetReg(GPIO0_4DATA,&value);
                value &= (~GPIO0_4SET);
                Hi_SetReg(GPIO0_4DATA,value);
            }
        }



#endif


#ifdef __GPIO1_5__
        if((1 == irModel) && (hd_ver != NJ_700K)) {          //红外为发射状态
            if(hd_ver == GZ_700S) {
                Hi_SetReg(GPIO11_3REG, 0);

                Hi_GetReg(GPIO11_3DIR, &value);
                value |= GPIO11_3SET;
                Hi_SetReg(GPIO11_3DIR, value);

                Hi_GetReg(GPIO11_3DATA, &value);
                value |= GPIO11_3SET;
                Hi_SetReg(GPIO11_3DATA, value);

                Hi_GetReg(GPIO14_7DIR, &value);
                value |= GPIO14_7SET;
                Hi_SetReg(GPIO14_7DIR, value);
            }
            else if(NJ_A345 == hd_ver) {
                ir_dir.dir = 0x12150400;
                ir_dir.data = 0x12150010;
                ir_dir.set = 0x4;

                /*--- 先置方向引角为高， 做为输出 ---*/
                Hi_GetReg(ir_dir.dir, &value);
                value |= ir_dir.set;
                Hi_SetReg(ir_dir.dir, value);

                Hi_GetReg(ir_dir.data, &value);
                value |= ir_dir.set;
                Hi_SetReg(ir_dir.data, value);
            }
            else {
                Hi_SetReg(GPIO1_5MUX,0x1);

                Hi_GetReg(GPIO1_5DIR,&value);
                value |= GPIO1_5SET;
                Hi_SetReg(GPIO1_5DIR,value);

                Hi_GetReg(GPIO1_5DATA,&value);
                value |= GPIO1_5SET;
                Hi_SetReg(GPIO1_5DATA,value);
            }

        }
        else if(2 == irModel) {     //红外学习状态，需要IO置为输出高电平，供电
            if(hd_ver == NJ_700K) {
                Hi_SetReg(GPIO20_5MUX,0x0);

                Hi_GetReg(GPIO20_5DIR,&value);
                value |= GPIO20_5SET;
                Hi_SetReg(GPIO20_5DIR,value);

                Hi_GetReg(GPIO20_5DATA,&value);
                value |= GPIO20_5SET;
                Hi_SetReg(GPIO20_5DATA,value);
            }
            else if(hd_ver == GZ_700S) {
                Hi_SetReg(GPIO11_2REG, 0x1);

                Hi_GetReg(GPIO11_2DIR, &value);
                value |= GPIO11_2SET;
                Hi_SetReg(GPIO11_2DIR, value);

                Hi_GetReg(GPIO11_2DATA, &value);
                value |= GPIO11_2SET;
                Hi_SetReg(GPIO11_2DATA, value);

                Hi_SetReg(GPIO11_1REG, 0x1);

                Hi_GetReg(GPIO11_1DIR, &value);
                value |= GPIO11_1SET;
                Hi_SetReg(GPIO11_1DIR, value);
                Hi_GetReg(GPIO11_1DATA, &value);
                value |= GPIO11_1SET;
                Hi_SetReg(GPIO11_1DATA, value);

                Hi_SetReg(GPIO11_3REG, 0x0);
                Hi_GetReg(GPIO11_3DIR, &value);
                value |= GPIO11_3SET;
                Hi_SetReg(GPIO11_3DIR, value);

                Hi_GetReg(GPIO11_3DATA, &value);
                value &= (~GPIO11_3SET);
                Hi_SetReg(GPIO11_3DATA, value);

            }
            else if(hd_ver == GZ_31A_D2) {
                Hi_SetReg(GPIO1_6MUX,0x1);

                Hi_GetReg(GPIO1_6DIR,&value);
                value |= GPIO1_6SET;
                Hi_SetReg(GPIO1_6DIR,value);

                Hi_GetReg(GPIO1_6DATA,&value);
                value |= GPIO1_6SET;
                Hi_SetReg(GPIO1_6DATA,value);

                Hi_SetReg(GPIO15_6MUX,0x1);

                Hi_GetReg(GPIO15_6DIR,&value);
                value |= GPIO15_6SET;
                Hi_SetReg(GPIO15_6DIR,value);

                Hi_GetReg(GPIO15_6DATA,&value);
                value |= GPIO15_6SET;
                Hi_SetReg(GPIO15_6DATA,value);

                Hi_SetReg(GPIO1_5MUX,0x1);

                Hi_GetReg(GPIO1_5DIR,&value);
                value |= GPIO1_5SET;
                Hi_SetReg(GPIO1_5DIR,value);

                Hi_GetReg(GPIO1_5DATA,&value);
                value &= (~GPIO1_5SET);
                Hi_SetReg(GPIO1_5DATA,value);

            }
            else if(hd_ver == NJ_A345) {
                ir_pow_data.data = 0x121B0080;
                ir_pow_data.set = 0x20;
                Hi_GetReg(ir_pow_data.data, &value);
                value |= ir_pow_data.set;
                Hi_SetReg(ir_pow_data.data, value);

                ir_dir.dir = 0x12150400;
                ir_dir.data = 0x12150010;
                ir_dir.set = 0x4;

                /*--- 先置方向引角为低， 做为输入 ---*/
                Hi_GetReg(ir_dir.dir, &value);
                value |= ir_dir.set;
                Hi_SetReg(ir_dir.dir, value);

                Hi_GetReg(ir_dir.data, &value);
                value &= (~ir_dir.set);
                Hi_SetReg(ir_dir.data, value);
            }
            else {
                Hi_SetReg(GPIO1_6MUX,0x1);

                Hi_GetReg(GPIO1_6DIR,&value);
                value |= GPIO1_6SET;
                Hi_SetReg(GPIO1_6DIR,value);

                Hi_GetReg(GPIO1_6DATA,&value);
                value |= GPIO1_6SET;
                Hi_SetReg(GPIO1_6DATA,value);

                Hi_SetReg(GPIO1_7MUX,0x1);

                Hi_GetReg(GPIO1_7DIR,&value);
                value |= GPIO1_7SET;
                Hi_SetReg(GPIO1_7DIR,value);

                Hi_GetReg(GPIO1_7DATA,&value);
                value |= GPIO1_7SET;
                Hi_SetReg(GPIO1_7DATA,value);

                Hi_SetReg(GPIO1_5MUX,0x1);

                Hi_GetReg(GPIO1_5DIR,&value);
                value |= GPIO1_5SET;
                Hi_SetReg(GPIO1_5DIR,value);

                Hi_GetReg(GPIO1_5DATA,&value);
                value &= (~GPIO1_5SET);
                Hi_SetReg(GPIO1_5DATA,value);
            }

        }

#endif

#ifdef __GPIO1_6__
            Hi_SetReg(GPIO1_6MUX,0x1);

            Hi_GetReg(GPIO1_6DIR,&value);
            value |= GPIO1_6SET;
            Hi_SetReg(GPIO1_6DIR,value);

            Hi_GetReg(GPIO1_6DATA,&value);
            value |= GPIO1_6SET;
            Hi_SetReg(GPIO1_6DATA,value);
#endif
        devInfFd = open(DEVPATH,   O_RDWR | O_TRUNC);
        if(-1 == devInfFd) {
            COMMON_PRT("open %s error\n",DEVPATH);
            return false;
        }
    }
    else {
        open_port(global_data.portInfrared,&devInfFd);
        set_optBak(global_data.uartspeedInf,global_data.nbitInf,global_data.nstopInf,   \
                   global_data.eventInf[0],global_data.nVtimeInf,global_data.nVminInf,&devInfFd);

        FD_SET(devInfFd,&readFd);
        if(devInfFd >= maxFd)
            maxFd = devInfFd + 1;
    }
    printf("ir sv port:%d\n",global_data.svInfraredport);
    socketInf = createUdp(global_data.svInfraredport);
    FD_SET(socketInf,&readFd);
    if(socketInf >= maxFd)
        maxFd = socketInf + 1;
    return true;
}


bool infrared::rcvData2Ir()
{
    char tmpData[MAXDATA] = {0};
    int rcvLen = 0,len = 0,errorlen = 0, sendLen = 0 ;
    socklen_t addrLen = 0;
    memset(tmpData,0,sizeof(tmpData));
    char irEcho[8] = {0};

    addrLen = sizeof(struct sockaddr_in);

    rcvLen = recvfrom(socketInf,tmpData,MAXDATA,0,(struct sockaddr*)(&ctrlAddr),&addrLen);
    if(rcvLen > 0) {
#ifdef __DEBUG__
    print0x(tmpData,rcvLen);
#endif
    if(1 == global_data.enInfrared) {
        len = write(devInfFd,tmpData,rcvLen);
        sleep(1);
    }
    else if(2 == global_data.enInfrared) {
        if((tmpData[0] == 0x55) && (tmpData[1] == 0xA1)) {
            len = write(devInfFd,tmpData,2);
            memset(tmpData,0,MAXDATA);
            len = read(devInfFd,tmpData,MAXDATA);
            for(int i = 0; i < len; i++) {
                if(0 == tmpData[i])
                    errorlen++;
            }
            if((errorlen > len/3) || (len < 30)) {
                memset(irEcho,0,sizeof(irEcho));
                strcpy(irEcho,"error");
                sendLen = sendto(socketInf,irEcho,strlen(irEcho),0,(struct sockaddr*)&ctrlAddr,sizeof(struct sockaddr_in));
            }
            else {
                sendLen = sendto(socketInf,tmpData,len,0,(struct sockaddr*)&ctrlAddr,sizeof(struct sockaddr_in));
            }
        }
    }

    }
    else {
        COMMON_PRT("net not rcv ir data\n");
        return false;
    }
    return true;
}

/*
 * 配合UMP设置，时时生效
 * 全局参数在外部修改，保存
 * */

bool infrared::UMPsetParam(bool uartFlag,bool ctrlPortFlag,bool svPortFlag)
{
    if(uartFlag) {
        COMMON_PRT("UMP set ir UART Param\n");
        FD_CLR(devInfFd,&readFd);
        close(devInfFd);
        sleep(1);

        devInfFd = -1;
        open_port(global_data.portInfrared,&devInfFd);
        set_optBak(global_data.uartspeedInf,global_data.nbitInf,global_data.nstopInf,   \
                   global_data.eventInf[0],global_data.nVtimeInf,global_data.nVminInf,&devInfFd);

        FD_SET(devInfFd,&readFd);
        if(devInfFd >= maxFd)
            maxFd = devInfFd + 1;
    }

    if(ctrlPortFlag) {
        COMMON_PRT("UMP set ir ctrl port Param\n");
    }

    if(svPortFlag) {
        FD_CLR(socketInf,&readFd);
        close(socketInf);
        sleep(1);

        socketInf = createUdp(global_data.svInfraredport);
        FD_SET(socketInf,&readFd);
        if(socketInf >= maxFd)
            maxFd = socketInf + 1;
    }

    return true;
}

