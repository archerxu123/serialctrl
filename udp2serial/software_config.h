#ifndef Software_Config_H
#define Software_Config_H

#include <stdio.h>
#include <string.h>
#include <string>
#include <vector>
#include <iostream>

#include <readini.h>
#include "global.h"


using namespace std;
#define ETH0_NAME "eth0"
#define ETH1_NAME "eth1"
#define DNS_NAME  "/etc/resolv.conf"
#define DATA_FILE "/data"
#define SERIAL_CONFIG_FILE "remserial.ini"

extern char filepath_[36];

#define PARA_NAME_LEN 56
#define EnumtoStr(val) #val //把枚举变量的名称. 返回类型为const char[]

class SoftwareConfig
{
public:
    typedef enum {
        //控制端IP和端口，UMP可设置
        kctlip = 0,
        kctl232port,
        kctl485port,
        kctlrelayport,
        kctlinfraredport,
        kctlIO0port,
        kctlIO1port,
        kctlIO2port,
        kctlIO3port,
        //板子UDP接收端口，原则上不变
        ksv232port,
        ksv485port,
        ksvrelayport,
        kinfraredport,
        ksvIOport,
        //RS232--串口使能和属性
        ken232,
        k232port,
        k232uartspeed,
        k232nbit,
        k232nstop,
        k232event,
        k232vtime,
        k232vmin,
        //RS485--串口使能和属性
        ken485,
        k485port,
        k485uartspeed,
        k485nbit,
        k485nstop,
        k485event,
        k485vtime,
        k485vmin,
        //继电器---使能和状态
        kenrelay,
        krelayst,
        //红外---使能和属性（GPIO模拟和串口两种形式，通过硬件号区分）
        kenInfrared,
        kInfraredport,
        kInfrareduartspeed,
        kInfrarednbit,
        kInfrarednstop,
        kInfraredevent,
        kInfraredvtime,
        kInfraredvmin,
        //IO--0(未使能)，1（接收），2（发送）
        kSet0IO,
        kIO0st,
        kSet1IO,
        kIO1st,
        kSet2IO,
        kIO2st,
        kSet3IO,
        kIO3st,
        //TCP
        kTcpEnable,
        kTcpServerPort,
        kTcpLinkMax,
        kSoftWareConfigIDMax,
    }SoftWareConfigID;

    SoftwareConfig();
    ~SoftwareConfig();

    bool ReadConfig();
    bool SaveConfig();
    void PrintConfig();
    bool SetConfig(const SoftWareConfigID kId, string value);
    bool SetConfig(const SoftWareConfigID kId, int value);
    string GetConfig(const SoftWareConfigID kId);
    bool reset();

private:
    vector<string> configvalue_;
    //const char *const filepath_ = "./hi3531d_venc.ini"; //const data,const pointer
    char paramsnamelist[kSoftWareConfigIDMax][PARA_NAME_LEN]; //存放配置文件的前缀名称
};

#endif // Software_Config_H
