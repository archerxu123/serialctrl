#include "global.h"

#include "rs232.h"
#include "rs485.h"
#include "relay.h"
#include "infrared.h"
#include "io.h"
#include "umpset.h"
#include "io_edc.h"
#include "tcp_server.h"

rs232 *rs232P;
rs232 *rs232bakP;       //教育一体板 双232口
rs485 *rs485P;
relay *relayP;
infrared *irP;
io *ioP0;
io *ioP1;
io *ioP2;
io *ioP3;
#ifdef __31DV200__
io *ioP2;
io *ioP3;
relay *relayP1;
#endif
io_edc *ioP_edc;
umpSet *umpP;

void HandleSig(int signo)
{
    printf("program exit--->0x%x\n",signo);
    if(en232) {
        delete rs232P;
    }

    if(en485) {
        delete rs485P;
    }

    if(enRelay) {
        delete relayP;
    }

    if(enIr) {
        delete irP;
    }

    if(enIo0) {
        delete ioP0;
    }

    if(enIo1) {
        delete ioP1;
    }

    delete umpP;

    exit(-1);
}

bool iniParam()
{
    auto thisini = Singleton<SoftwareConfig>::getInstance();
    memset(&global_data,0,sizeof(CONFIGDATA));

//控制软件回复端口
    strcpy(global_data.ctlip,thisini->GetConfig(SoftwareConfig::kctlip).c_str());
    global_data.ctl232port = str2int(thisini->GetConfig(SoftwareConfig::kctl232port));
    global_data.ctl485port = str2int(thisini->GetConfig(SoftwareConfig::kctl485port));
    global_data.ctlrelayport = str2int(thisini->GetConfig(SoftwareConfig::kctlrelayport));
    global_data.ctlinfraredport = str2int(thisini->GetConfig(SoftwareConfig::kctlinfraredport));
    global_data.ctlIO0port = str2int(thisini->GetConfig(SoftwareConfig::kctlIO0port));
    global_data.ctlIO1port = str2int(thisini->GetConfig(SoftwareConfig::kctlIO1port));
    global_data.ctlIO2port = str2int(thisini->GetConfig(SoftwareConfig::kctlIO2port));
    global_data.ctlIO3port = str2int(thisini->GetConfig(SoftwareConfig::kctlIO3port));

//本地受控端口
    global_data.sv232port = str2int(thisini->GetConfig(SoftwareConfig::ksv232port));
    global_data.sv485port = str2int(thisini->GetConfig(SoftwareConfig::ksv485port));
    global_data.svrelayport = str2int(thisini->GetConfig(SoftwareConfig::ksvrelayport));
    global_data.svInfraredport = str2int(thisini->GetConfig(SoftwareConfig::kinfraredport));
    global_data.svIOport = str2int(thisini->GetConfig(SoftwareConfig::ksvIOport));

//RS232参数
    global_data.en232 = str2int(thisini->GetConfig(SoftwareConfig::ken232));
    strcpy(global_data.port232,thisini->GetConfig(SoftwareConfig::k232port).c_str());
    global_data.uartspeed232 = str2int(thisini->GetConfig(SoftwareConfig::k232uartspeed));
    global_data.nbit232 = str2int(thisini->GetConfig(SoftwareConfig::k232nbit));
    global_data.nstop232 = str2int(thisini->GetConfig(SoftwareConfig::k232nstop));
    strcpy(global_data.event232,thisini->GetConfig(SoftwareConfig::k232event).c_str());
    global_data.nVtime232 = str2int(thisini->GetConfig(SoftwareConfig::k232vtime));
    global_data.nVmin232 = str2int(thisini->GetConfig(SoftwareConfig::k232vmin));

//RS485参数
    global_data.en485 = str2int(thisini->GetConfig(SoftwareConfig::ken485));
    strcpy(global_data.port485,thisini->GetConfig(SoftwareConfig::k485port).c_str());
    global_data.uartspeed485 = str2int(thisini->GetConfig(SoftwareConfig::k485uartspeed));
    global_data.nbit485 = str2int(thisini->GetConfig(SoftwareConfig::k485nbit));
    global_data.nstop485 = str2int(thisini->GetConfig(SoftwareConfig::k485nstop));
    strcpy(global_data.event485,thisini->GetConfig(SoftwareConfig::k485event).c_str());
    global_data.nVtime485 = str2int(thisini->GetConfig(SoftwareConfig::k485vtime));
    global_data.nVmin485 = str2int(thisini->GetConfig(SoftwareConfig::k485vmin));

//继电器参数
    global_data.enRelay = str2int(thisini->GetConfig(SoftwareConfig::kenrelay));
    global_data.relaySt = str2int(thisini->GetConfig(SoftwareConfig::krelayst));

//红外参数  串口红外和GPIO定时器红外
    global_data.enInfrared = str2int(thisini->GetConfig(SoftwareConfig::kenInfrared));
    strcpy(global_data.portInfrared,thisini->GetConfig(SoftwareConfig::kInfraredport).c_str());
    global_data.uartspeedInf = str2int(thisini->GetConfig(SoftwareConfig::kInfrareduartspeed));
    global_data.nbitInf = str2int(thisini->GetConfig(SoftwareConfig::kInfrarednbit));
    global_data.nstopInf = str2int(thisini->GetConfig(SoftwareConfig::kInfrarednstop));
    strcpy(global_data.eventInf,thisini->GetConfig(SoftwareConfig::kInfraredevent).c_str());
    global_data.nVtimeInf = str2int(thisini->GetConfig(SoftwareConfig::kInfraredvtime));
    global_data.nVminInf = str2int(thisini->GetConfig(SoftwareConfig::kInfraredvmin));

//IO参数
    global_data.enIo_0 = str2int(thisini->GetConfig(SoftwareConfig::kSet0IO).c_str());
    global_data.io0St = str2int(thisini->GetConfig(SoftwareConfig::kIO0st).c_str());
    global_data.enIo_1 = str2int(thisini->GetConfig(SoftwareConfig::kSet1IO).c_str());
    global_data.io1St = str2int(thisini->GetConfig(SoftwareConfig::kIO1st).c_str());
    global_data.enIo_2 = str2int(thisini->GetConfig(SoftwareConfig::kSet2IO).c_str());
    global_data.io2St = str2int(thisini->GetConfig(SoftwareConfig::kIO2st).c_str());
    global_data.enIo_3 = str2int(thisini->GetConfig(SoftwareConfig::kSet3IO).c_str());
    global_data.io3St = str2int(thisini->GetConfig(SoftwareConfig::kIO3st).c_str());

//TCP参数
    global_data.tcp_en = str2int(thisini->GetConfig(SoftwareConfig::kTcpEnable).c_str());
    global_data.tcp_port = str2int(thisini->GetConfig(SoftwareConfig::kTcpServerPort).c_str());
    global_data.tcp_link_max = str2int(thisini->GetConfig(SoftwareConfig::kTcpLinkMax).c_str());

    return true;
}

bool readHdmode(char *hd_mode)
{
    FILE *hdFp = NULL;
    int rdLen = 0;
//    char buff[256] = {0};

//    char *st = NULL, *end = NULL;

    if(NULL == (hdFp = fopen("/home/mode","r"))) {
        printf("open mode error\n");
        return false;
    }
    //memset(buff,0,256);
    memset(hd_mode,0,32);

    rdLen = fread(hd_mode,1,32,hdFp);
    if(rdLen > 0) {
//        printf("read mode:%s\n",buff);
//        st = strstr(buff,"\"board\": \"");
//        st += strlen("\"board\": \"");
//        end = strchr(st,'\"');
//        strncpy(hd_version,st,end-st);
        printf("get mode:%s\n",hd_mode);
    }
    else{
        fclose(hdFp);
        return false;
    }
    fclose(hdFp);
    return true;
}


bool hd_232 = true;
bool hd_485 = false;
bool hd_relay = false;
bool hd_ir = false;
bool hd_io = false;
bool hd_232_edc = false;

bool ir_nj_700k = false;

bool ir_nj_21d = false;




int main()
{
    bool hi3521d_7000 = false;

#ifdef  __7000_NJ__
        hi3521d_7000 = true;
#endif
    signal(SIGHUP, HandleSig); //NOTE: 关闭终端
    signal(SIGINT, HandleSig); //NOTE: 检测Ctrl+C按键
    signal(SIGTERM, HandleSig); //NOTE: 检测killall 命令
    signal(SIGSEGV, HandleSig); //NOTE: 检测 Segmentation fault!


    char hdVersion[32] = {0};
    char hdmode[32] = {0};
    memset(hdVersion,0,32);
    memset(hdmode,0,32);

#ifdef __AUTO_TEST__
    int socket_test = createUdp(16460);
    char tmp[MAXDATA] = {0};
    struct sockaddr c_addr;
    socklen_t c_addr_len;
    c_addr_len = sizeof(struct sockaddr_in);

    while(1) {
        memset(tmp,0,MAXDATA);
        char *splt = NULL;
        int i = 0;
        int len = recvfrom(socket_test,tmp,MAXDATA,0,(struct sockaddr*)&c_addr,&c_addr_len);
        splt = strstr(tmp,"START");
        if(splt) {
            splt = strstr(tmp,",");
            splt++;
            printf("data:%s\n",splt);
            while(splt[i] != ']') {
                hdVersion[i] = splt[i];
                i++;
            }
            printf("get hdVersion :%s\n",hdVersion);
            sendto(socket_test,"[OK]",4,0,(struct sockaddr*)&c_addr,sizeof(struct sockaddr_in));
            break;
        }
    }
#else

    readHdVersion(hdVersion);
 //   readHdmode(hdmode);
#endif
    local_ip = GetLocalIp();

//根据硬件版本号，使能不同的硬件
    if(!strncmp(hdVersion,HD_2SDI_NJ,strlen(HD_2SDI_NJ))) {
        hd_232 = true;
        hd_ir = true;
    }
    else if(!strncmp(hdVersion,HD_D_HDMI_NJ,strlen(HD_D_HDMI_NJ))) {
        hd_232 = true;
        hd_relay = true;
        hd_485 = true;
        hd_ir = true;
        hd_io = true;
        ir_nj_700k = true;
        hd_ver = NJ_700K;
    }
    else if(!strncmp(hdVersion,HD_4HDMI_NJ,strlen(HD_4HDMI_NJ))) {
        hd_232 = true;
    }
    else if(!strncmp(hdVersion,HD_4MIX_NJ,strlen(HD_4MIX_NJ))) {
        hd_232 = true;
    }
    else if(!strncmp(hdVersion,HD_300T_NJ,strlen(HD_300T_NJ))) {
        hd_232 = true;
        hd_ir = true;
    }
    else if(!strncmp(hdVersion,HD_7000_NJ,strlen(HD_7000_NJ))) {
        hd_232 = true;
        hd_ir = true;
        hd_io = true;
        hd_ver = NJ_7000;
    }
    else if(!strncmp(hdVersion,HD_7020_NJ,strlen(HD_7020_NJ))) {
        hd_232 = true;
        hd_ir = true;
        hd_io = true;
        hd_ver = NJ_7000;
        irTime = true;
    }
    else if(!strncmp(hdVersion,HD_7000_NJ_HD,strlen(HD_7000_NJ_HD))) {
        hd_232 = true;
        hd_ir = true;
        hd_io = true;
        hd_ver = NJ_7000;
        irTime = true;
    }
    else if(!strncmp(hdVersion,HD_700K_GZ,strlen(HD_700K_GZ))) {
        hd_232 = true;
        hd_ir = true;
        hd_io = true;
        hd_relay = true;
        hd_485 = true;
        hd_ver = GZ_700K;
    }
    else if(!strncmp(hdVersion, HD_700S_GZ, strlen(HD_700S_GZ))) {
        hd_232 = true;
        hd_ir = true;
        hd_io = true;
        hd_relay = true;
        hd_485 = true;
        hd_ver = GZ_700S;
        irTime = true;
        en_3536 = true;
    }
    else if(!strncmp(hdVersion,HD_700K_GZ_A,strlen(HD_700K_GZ_A))) {
        hd_232 = true;
        hd_ir = true;
        hd_io = true;
        hd_relay = true;
        hd_485 = true;
        hd_ver = GZ_700K;
        irTime = true;
    }
    else if(!strncmp(hdVersion,HD_700K_GZ_V2,strlen(HD_700K_GZ_V2))) {
        hd_232 = true;
        hd_ir = true;
        hd_io = true;
        hd_relay = true;
        hd_485 = true;
        hd_ver = GZ_700K;
        irTime = true;
    }
    else if(!strncmp(hdVersion,HD_A156_NJ_V2,strlen(HD_A156_NJ_V2))) {
        hd_232 = true;
        hd_ir = true;
        hd_io = true;
        hd_relay = true;
        hd_485 = true;
        hd_ver = NJ_A156_V2;
        irTime = true;
    }
    else if(!strncmp(hdVersion,HD_A241_NJ,strlen(HD_A241_NJ))) {
        hd_232 = true;
        hd_ir = true;
        hd_io = true;
        hd_relay = true;
        hd_485 = true;
        hd_ver = NJ_A241;
        irTime = true;
    }
    else if(!strncmp(hdVersion,HD_21D_D_HDMI,strlen(HD_21D_D_HDMI))) {
        hd_232 = true;
        hd_ir = true;
        hd_io = true;
        hd_relay = true;
        hd_485 = true;
        hd_ver = NJ_A335;
        irTime = true;
    }
    else if(!strncmp(hdVersion,HD_700K_NJ_V1,strlen(HD_700K_NJ_V1))) {
        hd_232 = true;
        hd_relay = true;
        hd_485 = true;
    }
    else if(!strncmp(hdVersion,HD_700K_NJ_V2,strlen(HD_700K_NJ_V2))) {
        hd_232 = true;
        hd_relay = true;
        hd_485 = true;
        hd_ir = true;
        hd_io = true;
        ir_nj_700k = true;
        hd_ver = NJ_700K;
    }
    else if(!strncmp(hdVersion,HD_700K_NJ_V3,strlen(HD_700K_NJ_V3))) {
        hd_232 = true;
        hd_relay = true;
        hd_485 = true;
        hd_ir = true;
        hd_io = true;
        ir_nj_700k = true;
        hd_ver = NJ_700K;
    }
    else if(!strncmp(hdVersion,HD_8000_GZ,strlen(HD_8000_GZ))) {
        hd_232 = true;
        hd_ir = true;
        hd_io = true;
        hd_relay = true;
        hd_485 = true;
        hd_ver = GZ_8000;
    }
    else if(!strncmp(hdVersion,HD_DEC_GZ,strlen(HD_DEC_GZ))) {
        hd_232 = true;
        hd_relay = true;
        hd_232_edc = true;
        hd_ver = GZ_EDC;
    }
    else if(!strncmp(hdVersion,HD_7000_PTN,strlen(HD_7000_PTN))) {
        hd_232 = true;
        hd_ir  = true;
        hd_io  = true;
        hd_ver = PTN_7000;
    }
    else if(!strncmp(hdVersion,HD_21A_PTN,strlen(HD_21A_PTN))) {
        hd_232 = true;
        hd_ir  = true;
        hd_io  = true;
        hd_ver = PTN_21A;
    }
    else if(!strncmp(hdVersion,HD_31DV200_GZ,strlen(HD_31DV200_GZ))) {
        hd_232 = true;
        hd_ir  = true;
        hd_io  = true;
        hd_ver = GZ_31DV200;
    }
    else if(!strncmp(hdVersion,HD_31DV200_NJ,strlen(HD_31DV200_NJ))) {
        hd_232 = true;
        hd_relay = true;
        hd_485 = true;
        hd_ir = true;
        hd_io = true;
//        ir_nj_700k = true;
        hd_ver = NJ_A304;

    }
    else if(!strncmp(hdVersion,HD_A151_NJ,strlen(HD_A151_NJ))) {
        hd_232 = true;
        hd_ir  = true;
        hd_io  = true;
        hd_ver = NJ_A151;
        irTime = true;
        readHdmode(hdmode);
        if(!strncmp(hdmode,"out",3)){
            outmode=true;
        }
        else
            outmode=false;
    }
    else if(!strncmp(hdVersion,HD_A323_NJ,strlen(HD_A323_NJ))) {
        hd_232 = true;
        hd_ir  = true;
        hd_io  = true;
        hd_ver = NJ_A323;
        irTime = true;
        readHdmode(hdmode);
        if(!strncmp(hdmode,"out",3)){
            outmode=true;
        }
        else
            outmode=false;

    }
    else if(!strncmp(hdVersion,HD_A186_NJ,strlen(HD_A186_NJ))) {
        hd_232 = true;
        hd_ir = true;
        hd_io = true;
        hd_ver = NJ_A186_V1;
        irTime = true;
    }
    else if(!strncmp(hdVersion,HD_31A_D1_GZ,strlen(HD_31A_D1_GZ))) {
        hd_232 = true;
        hd_ir  = true;
        hd_io  = true;
        hd_ver = GZ_31A_D2;
        irTime = true;
    }
    else if(!strncmp(hdVersion,HD_300R_GZ,strlen(HD_300R_GZ))) {
        hd_232 = true;
    }
#if 0
    if(!strncmp(hdVersion,IRTIMEHD,strlen(IRTIMEHD))) {
        COMMON_PRT("this hardware version support time ir\n");
        irTime = true;
    }
#endif
    else if(!strncmp(hdVersion,HD_7000_PTN,strlen(HD_7000_PTN))) {
        COMMON_PRT("this hardware version support time ir\n");
        irTime = true;
    }
    else if(!strncmp(hdVersion,HD_21A_PTN,strlen(HD_21A_PTN))) {
        COMMON_PRT("this hardware version support time ir\n");
        irTime = true;
    }
    else if(!strncmp(hdVersion,IR_8000,strlen(IR_8000))) {
        COMMON_PRT("this hardware version support time ir\n");
        irTime = true;
    }
    else if(!strncmp(hdVersion,HD_700K_NJ_V2,strlen(HD_700K_NJ_V2))) {
        COMMON_PRT("this hardware version support time ir\n");
        irTime = true;
    }
    else if(!strncmp(hdVersion,HD_D_HDMI_NJ,strlen(HD_D_HDMI_NJ))) {
        COMMON_PRT("this hardware version support time ir\n");
        irTime = true;
    }
    else if(!strncmp(hdVersion, HD_21D_860, strlen(HD_21D_860))) {
        hd_232 = true;
        hd_io = true;
        hd_ir = true;
        irTime = true;
        hd_ver = NJ_A345;
    }
    else if(!strncmp(hdVersion, HD_GZ16A_200, strlen(HD_GZ16A_200))) {
        hd_232 = true;
        hd_relay = true;
        hd_ver = GZ16A_200_V1;
    }
    else if(!strncmp(hdVersion, HD_GZ16A_100, strlen(HD_GZ16A_100))) {
        hd_232 = true;
        hd_io = true;
        hd_ir = true;
        hd_ver = GZ16A_100_V1;
    }
    else if(!strncmp(hdVersion, HD_GZ36_200_V1, strlen(HD_GZ36_200_V1))) {
        hd_232 = true;
        hd_485 = true;
        hd_ver = GZ36_200_V1;
    }

#ifdef __7000__
    irTime = true;
#endif

    //1-读取本地配置文件，初始化全局变量
    auto thisini = Singleton<SoftwareConfig>::getInstance();
    thisini->ReadConfig();
    thisini->PrintConfig();
    thisini->SaveConfig();

    if(!strncmp(hdVersion,HD_7020_NJ,strlen(HD_7020_NJ))) {
        thisini->SetConfig(SoftwareConfig::k232port, "com2");
        thisini->SaveConfig();
    }
    if(PTN_21A == hd_ver) {
        thisini->SetConfig(SoftwareConfig::k232port, "com0");
        thisini->SaveConfig();
    }
    if(NJ_A304 == hd_ver) {
        thisini->SetConfig(SoftwareConfig::k232port, "com3");
        thisini->SetConfig(SoftwareConfig::k485port, "com4");
        thisini->SaveConfig();
        printf("A304 UART 232-com3 485-com4  OK\n");
    }
    if(NJ_A151 == hd_ver) {
        uint value;                               //串口复用
        Hi_GetReg(A151_UART_RX_REG,&value);
        value |= (0x1);
        Hi_SetReg(A151_UART_RX_REG, value);
        Hi_GetReg(A151_UART_TX_REG,&value);
        value |= (0x1);
        Hi_SetReg(A151_UART_TX_REG, value);
        printf("UART REG OK\n");
    }
    if(NJ_A323 == hd_ver) {
        uint value;                               //串口复用
        Hi_GetReg(A323_UART_RX_REG,&value);
        value |= (0x1);
        Hi_SetReg(A323_UART_RX_REG, value);
        Hi_GetReg(A323_UART_TX_REG,&value);
        value |= (0x1);
        Hi_SetReg(A323_UART_TX_REG, value);
        printf("UART A323 REG OK\n");
//        thisini->SetConfig(SoftwareConfig::k232port, "com2");
//        thisini->SaveConfig();
    }
    if(GZ_31A_D2 == hd_ver) {
        uint value;                               //串口复用
        Hi_GetReg(GZ31A_D2_UART_RX_REG,&value);
        value |= (0x1);
        Hi_SetReg(GZ31A_D2_UART_RX_REG, value);
        Hi_GetReg(GZ31A_D2_UART_TX_REG,&value);
        value |= (0x1);
        Hi_SetReg(GZ31A_D2_UART_TX_REG, value);
        printf("UART REG OK\n");
        thisini->SetConfig(SoftwareConfig::k232port, "com3");
        thisini->SaveConfig();
    }
    else if(NJ_A345 == hd_ver) {
        thisini->SetConfig(SoftwareConfig::k232port, "com2");
        thisini->SaveConfig();
    }
    else if(GZ16A_200_V1 == hd_ver) {
        thisini->SetConfig(SoftwareConfig::k232port, "com2");
        thisini->SaveConfig();
    }
    else if(GZ16A_100_V1 == hd_ver) {
        thisini->SetConfig(SoftwareConfig::k232port, "com3");
        thisini->SaveConfig();
    }
    else if(GZ36_200_V1 == hd_ver) {
        thisini->SetConfig(SoftwareConfig::k232port, "com2");
        thisini->SetConfig(SoftwareConfig::k485port, "com3");
        thisini->SaveConfig();
    }

    iniParam();
    FD_ZERO(&readFd);

    int umpFlag = -1;


    //2-RS232初始化
    if(global_data.en232 && hd_232) {
        en232 = true;
        COMMON_PRT("rs232 enable\n");
        rs232P = new rs232();
        rs232P->iniRs232();
        if(NJ_A335 == hd_ver) {
            global_data.en485 = false;
            thisini->SetConfig(SoftwareConfig::ken485, false);
            thisini->SaveConfig();
        }
    }

    //3-RS485初始化
    if(global_data.en485 && (false == hi3521d_7000) && hd_485) {
        en485 = true;
        COMMON_PRT("rs485 enable\n");
        if(NJ_A335 == hd_ver) {
            memset(global_data.port485, 0, sizeof(global_data.port485));
            strcpy(global_data.port485, "com1");
            thisini->SetConfig(SoftwareConfig::k485port, "com1");
            thisini->SaveConfig();
        }
        printf("-----\n");
        rs485P = new rs485();
        rs485P->iniRs485();
    }

    //4-relay初始化
    if(global_data.enRelay && (false == hi3521d_7000) && hd_relay) {
        enRelay = true;
        COMMON_PRT("relay enable %d\n", global_data.relaySt);
        relayP = new relay();
        relayP->relayIni();
        relayP->relaySet(global_data.relaySt);
    }
    //5-ir初始化
    if(global_data.enInfrared && hd_ir) {
        if(NJ_A335 == hd_ver)
             {
            global_data.enInfrared = false;
            thisini->SetConfig(SoftwareConfig::kenInfrared, false);
            thisini->SaveConfig();
        }
        enIr = true;
        COMMON_PRT("ir enable\n");
        irP = new infrared();
        irP->infraredIni(global_data.enInfrared);
    }

    //6-0-io0初始化
    if(global_data.enIo_0 && hd_io) {

        COMMON_PRT("IO 0 enable\n");
        enIo0 = true;
        ioP0 = new io();
        ioP0->ioIni(0);
        if(global_data.enIo_0 == 2) {
            ioP0->IOSet(global_data.io0St,0);
        }

    }

    if(global_data.enIo_0 && hd_232_edc) {
        COMMON_PRT("IO_edc 0 enable\n");
        sleep(2);
        ioP_edc = new io_edc();
        ioP_edc->ioIni_edc();
    }

    //6-1-io1未初始化为红外，初始化为IO;红外优先级高于IO
    if((global_data.enIo_1) && (global_data.enInfrared == false) && hd_io) {
        enIo1 = true;
        COMMON_PRT("IO 1 enablea\n");
        ioP1 = new io();
        ioP1->ioIni(1);
        if(global_data.enIo_1 == 2) {
            ioP1->IOSet(global_data.io1St,1);
        }
    }

    if(global_data.enIo_2 && hd_io) {
        COMMON_PRT("IO 2 enable, %d\n", global_data.enIo_2);
        enIo2 = true;
        ioP2 = new io();
        ioP2->ioIni(2);
        if(global_data.enIo_2 == 2) {
            ioP2->IOSet(global_data.io2St,2);
        }
    }

    if(global_data.enIo_3 && hd_io) {

        COMMON_PRT("IO 3 enable\n");
        enIo3 = true;
        ioP3 = new io();
        ioP3->ioIni(3);
        if(global_data.enIo_2 == 3) {
            ioP2->IOSet(global_data.io2St,3);
        }
    }

    //7-UMP初始化
    umpP = new umpSet();
    umpP->umpIni();

    while(1) {
        FD_ZERO(&readFd);
        if(en232 && hd_232) {
            FD_SET(rs232P->dev232Fd,&readFd);
            FD_SET(rs232P->socket232,&readFd);
            if(hd_232_edc) {
                FD_SET(rs232P->dev232Fd_edc,&readFd);
                FD_SET(rs232P->socket232_edc,&readFd);
            }
            else if(GZ16A_200_V1 == hd_ver || GZ36_200_V1 == hd_ver) {
                FD_SET(rs232P->dev232Fd_edc, &readFd);
            }
        }

        if(en485 && (false == hi3521d_7000) && hd_485) {
            FD_SET(rs485P->dev485Fd,&readFd);
            FD_SET(rs485P->socket485,&readFd);
        }

        if(enRelay && (false == hi3521d_7000) && hd_relay) {
            FD_SET(relayP->socketRelay,&readFd);
        }

        if(enIr && hd_ir) {
            FD_SET(irP->socketInf,&readFd);
        }

        if(enIo0 && hd_io) {
            FD_SET(ioP0->socketIO,&readFd);
        }

        if(enIo1 && hd_io) {
            FD_SET(ioP1->socketIO,&readFd);
        }

        if(enIo2 && hd_io) {
            FD_SET(ioP2->socketIO,&readFd);
        }

        if(enIo3 && hd_io) {
            FD_SET(ioP3->socketIO,&readFd);
        }

        if(hd_232_edc && global_data.enIo_0) {
            FD_SET(ioP_edc->socketIO,&readFd);
        }

        FD_SET(umpP->socketUmp,&readFd);

        if ( select(maxFd,&readFd,NULL,NULL,0) <= 0 ){
            printf("select error\n");
            break;
        }

        if(en232 && hd_232) {
            if (FD_ISSET(rs232P->socket232,&readFd)) {
                rs232P->rcvData2uart();
                continue;
            }

            if(FD_ISSET(rs232P->dev232Fd,&readFd)) {
                rs232P->readUart2ctrl();
                continue;
            }

            if(hd_232_edc) {
                if (FD_ISSET(rs232P->socket232_edc,&readFd)) {
                    rs232P->rcvData2uart_edc();
                    continue;
                }

                if(FD_ISSET(rs232P->dev232Fd_edc,&readFd)) {
                    rs232P->readUart2ctrl_edc();
                    continue;
                }
            }
            else if(GZ16A_200_V1 == hd_ver || GZ36_200_V1 == hd_ver) {
                if(FD_ISSET(rs232P->dev232Fd_edc,&readFd)) {
                    rs232P->readUart2ctrl_edc();
                    continue;
                }
            }
        }

        if(en485 && (false == hi3521d_7000) && hd_485) {
            if(FD_ISSET(rs485P->socket485,&readFd)) {
                rs485P->rcvData2uart();
                continue;
            }

            if(FD_ISSET(rs485P->dev485Fd,&readFd)) {
                rs485P->readUart2ctrl();
                continue;
            }
        }

        if(enRelay && (false ==  hi3521d_7000) && hd_relay) {
            if(FD_ISSET(relayP->socketRelay,&readFd)) {
                relayP->rcvData2Relay();
                continue;
            }
        }

        if(enIr && hd_ir) {
            if(FD_ISSET(irP->socketInf,&readFd)) {
                irP->rcvData2Ir();
                continue;
            }
        }

        if(enIo0 && hd_io) {
            if(FD_ISSET(ioP0->socketIO,&readFd)) {
                ioP0->rcvData2IO();
                continue;
            }
        }

        if(hd_232_edc && global_data.enIo_0) {
            if(FD_ISSET(ioP_edc->socketIO,&readFd)) {
                ioP_edc->rcvData2IO_edc();
                continue;
            }
        }

        if(enIo1 && hd_io) {
            if(FD_ISSET(ioP1->socketIO,&readFd)) {
                ioP1->rcvData2IO();
                continue;
            }
        }

        if(enIo2 && hd_io) {
            if(FD_ISSET(ioP2->socketIO,&readFd)) {
                ioP2->rcvData2IO();
                continue;
            }
        }

        if(enIo3 && hd_io) {
            if(FD_ISSET(ioP3->socketIO,&readFd)) {
                ioP3->rcvData2IO();
                continue;
            }
        }

        if(FD_ISSET(umpP->socketUmp,&readFd)) {
            umpFlag = umpP->rcvData();
            switch (umpFlag) {
                case DEL:
                    thisini->reset();
                    break;
                case REPLY:
                    umpP->relyUMP();
                    break;
                case PARAM:
//UMP设置控制端回复IP和端口号
                    memset(global_data.ctlip,0,32);
                    strcpy(global_data.ctlip,umpP->umpData.ctlip);
                    global_data.ctl232port =  umpP->umpData.ctl232port;
                    global_data.ctl485port = umpP->umpData.ctl485port;
                    global_data.ctlrelayport = umpP->umpData.ctlrelayport;
                    global_data.ctlinfraredport = umpP->umpData.ctlinfraredport;
                    global_data.ctlIO0port = umpP->umpData.ctlIO0port;
                    global_data.ctlIO1port = umpP->umpData.ctlIO1port;
                    global_data.ctlIO2port = umpP->umpData.ctlIO2port;
                    global_data.ctlIO3port = umpP->umpData.ctlIO3port;

                    thisini->SetConfig(SoftwareConfig::kctlip,global_data.ctlip);
                    thisini->SetConfig(SoftwareConfig::kctl232port,global_data.ctl232port);
                    thisini->SetConfig(SoftwareConfig::kctl485port,global_data.ctl485port);
                    thisini->SetConfig(SoftwareConfig::kctlrelayport,global_data.ctlrelayport);
                    thisini->SetConfig(SoftwareConfig::kctlinfraredport,global_data.ctlinfraredport);
                    thisini->SetConfig(SoftwareConfig::kctlIO0port,global_data.ctlIO0port);
                    thisini->SetConfig(SoftwareConfig::kctlIO1port,global_data.ctlIO1port);
                    thisini->SetConfig(SoftwareConfig::kctlIO2port,global_data.ctlIO2port);
                    thisini->SetConfig(SoftwareConfig::kctlIO3port,global_data.ctlIO3port);

                    global_data.tcp_en = umpP->umpData.tcp_en;
                    global_data.tcp_port = umpP->umpData.tcp_port;
                    global_data.tcp_link_max = umpP->umpData.tcp_link_max;
                    thisini->SetConfig(SoftwareConfig::kTcpEnable, global_data.tcp_en);
                    thisini->SetConfig(SoftwareConfig::kTcpServerPort, global_data.tcp_port);
                    thisini->SetConfig(SoftwareConfig::kTcpLinkMax, global_data.tcp_link_max);

//UMP设置RS232
                    if((global_data.en232 != umpP->umpData.en232) && hd_232) {
                        if(en232) {
                            delete rs232P;
                            en232 = false;
                        }
                        else {
                            if(NJ_A335 == hd_ver) {
                                if(en485) {
                                    delete rs485P;
                                    en485 = false;
                                }

                                global_data.en485 = false;
                                thisini->SetConfig(SoftwareConfig::ken485, false);
                                thisini->SaveConfig();
                            }
                            rs232P = new rs232();
                            rs232P->iniRs232();
                            en232 = true;

                        }
                        global_data.en232 = umpP->umpData.en232;
                    }

                    memset(global_data.port232,0,8);
                    strcpy(global_data.port232,umpP->umpData.port232);
                    global_data.uartspeed232 = umpP->umpData.uartspeed232;
                    global_data.nbit232 = umpP->umpData.nbit232;
                    global_data.nstop232 = umpP->umpData.nstop232;
                    global_data.event232[0] = umpP->umpData.event232[0];
                    global_data.nVtime232 = umpP->umpData.nVtime232;
                    global_data.nVmin232 = umpP->umpData.nVmin232;

                    thisini->SetConfig(SoftwareConfig::ken232,global_data.en232);
                    thisini->SetConfig(SoftwareConfig::k232port,global_data.port232);
                    thisini->SetConfig(SoftwareConfig::k232uartspeed,global_data.uartspeed232);
                    thisini->SetConfig(SoftwareConfig::k232nbit,global_data.nbit232);
                    thisini->SetConfig(SoftwareConfig::k232nstop,global_data.nstop232);
                    thisini->SetConfig(SoftwareConfig::k232event,global_data.event232);
                    thisini->SetConfig(SoftwareConfig::k232vtime,global_data.nVtime232);
                    thisini->SetConfig(SoftwareConfig::k232vmin,global_data.nVmin232);

                    if(en232 && hd_232) {
                        rs232P->UMPsetParam(false,false,true);
                    }

//UMP设置RS485
                    if(false ==  hi3521d_7000) {
                        if((global_data.en485 != umpP->umpData.en485) && hd_485) {
                            if(en485) {
                                delete rs485P;
                                en485 = false;
                            }
                            else {
                                if(NJ_A335 == hd_ver) {
                                    if(en232) {
                                        delete rs232P;
                                        en232 = false;
                                    }

                                    memset(global_data.port485, 0, sizeof(global_data.port485));
                                    strcpy(global_data.port485, "com1");
                                    thisini->SetConfig(SoftwareConfig::k485port, "com1");
                                    thisini->SaveConfig();
                                }
                                rs485P = new rs485();
                                rs485P->iniRs485();
                                en485 = true;
                            }
                            global_data.en485 = umpP->umpData.en485;
                        }
                        memset(global_data.port485,0,8);
                        strcpy(global_data.port485,umpP->umpData.port485);
                        global_data.uartspeed485 = umpP->umpData.uartspeed485;
                        global_data.nbit485 = umpP->umpData.nbit485;
                        global_data.nstop485 = umpP->umpData.nstop485;
                        global_data.event485[0] = umpP->umpData.event485[0];
                        global_data.nVtime485 = umpP->umpData.nVtime485;
                        global_data.nVmin485 = umpP->umpData.nVmin485;

                        thisini->SetConfig(SoftwareConfig::ken485,global_data.en485);
                        thisini->SetConfig(SoftwareConfig::k485port,global_data.port485);
                        thisini->SetConfig(SoftwareConfig::k485uartspeed,global_data.uartspeed485);
                        thisini->SetConfig(SoftwareConfig::k485nbit,global_data.nbit485);
                        thisini->SetConfig(SoftwareConfig::k485nstop,global_data.nstop485);
                        thisini->SetConfig(SoftwareConfig::k485event,global_data.event485);
                        thisini->SetConfig(SoftwareConfig::k485vtime,global_data.nVtime485);
                        thisini->SetConfig(SoftwareConfig::k485vmin,global_data.nVmin485);

                        if(en485 && hd_485) {
                            rs485P->UMPsetParam(false,false,true);
                        }
                    }


//UMP设置relay
                    if(false == hi3521d_7000) {
                        if((global_data.enRelay != umpP->umpData.enRelay) && hd_relay) {
                            if(enRelay) {
                                delete relayP;
                                enRelay = false;
                            }
                            else {
                                relayP =  new relay();
                                relayP->relayIni();
                                enRelay = true;
                            }
                            global_data.enRelay = umpP->umpData.enRelay;
                        }

                        thisini->SetConfig(SoftwareConfig::kenrelay,global_data.enRelay);
                    }

//UMP设置红外
                    if((global_data.enInfrared != umpP->umpData.enInfrared) && hd_ir) {
                        if(enIr) {
                            if(umpP->umpData.enInfrared) {      //UMP切换模式
                                delete irP;
                                usleep(20000);
                                irP = new infrared();
                                irP->infraredIni(umpP->umpData.enInfrared);
                            }
                            else {
                                delete irP;
                                enIr = false;
                            }
                        }
                        else {
                            irP = new infrared();
                            irP->infraredIni(umpP->umpData.enInfrared);
                            enIr = true;
                        }
                        global_data.enInfrared = umpP->umpData.enInfrared;
                    }
                    if(!irTime) {
                        memset(global_data.portInfrared,0,8);
                        strcpy(global_data.portInfrared,umpP->umpData.portInfrared);
                        global_data.uartspeedInf = umpP->umpData.uartspeedInf;
                        global_data.nbitInf = umpP->umpData.nbitInf;
                        global_data.nstopInf = umpP->umpData.nstopInf;
                        global_data.eventInf[0] = umpP->umpData.eventInf[0];
                        global_data.nVtimeInf = umpP->umpData.nVtimeInf;
                        global_data.nVminInf = umpP->umpData.nVminInf;


                        thisini->SetConfig(SoftwareConfig::kInfraredport,global_data.portInfrared);
                        thisini->SetConfig(SoftwareConfig::kInfrareduartspeed,global_data.uartspeedInf);
                        thisini->SetConfig(SoftwareConfig::kInfrarednbit,global_data.nbitInf);
                        thisini->SetConfig(SoftwareConfig::kInfrarednstop,global_data.nstopInf);
                        thisini->SetConfig(SoftwareConfig::kInfraredevent,global_data.eventInf);
                        thisini->SetConfig(SoftwareConfig::kInfraredvtime,global_data.nVtimeInf);
                        thisini->SetConfig(SoftwareConfig::kInfraredvmin,global_data.nVminInf);

                        if(enIr && hd_ir) {
                            irP->UMPsetParam(true,false,false);
                        }
                    }
                    thisini->SetConfig(SoftwareConfig::kenInfrared,global_data.enInfrared);

//设置IO
                    if((global_data.enIo_0 != umpP->umpData.enIo_0) && hd_io) {
                        if(enIo0) {
                            if(umpP->umpData.enIo_0) {
                                global_data.enIo_0 = umpP->umpData.enIo_0;
                                delete ioP0;
                                ioP0 = new io();
                                ioP0->ioIni(0);
                            }
                            else {
                                global_data.enIo_0 = umpP->umpData.enIo_0;
                                delete ioP0;
                                enIo0 = false;
                            }
                        }
                        else {
                            global_data.enIo_0 = umpP->umpData.enIo_0;
                            ioP0 = new io();
                            ioP0->ioIni(0);
                            enIo0 = true;
                        }
                    }

                    if((global_data.enIo_0 != umpP->umpData.enIo_0) && hd_232_edc) {
                        if(global_data.enIo_0) {
                            global_data.enIo_0 = umpP->umpData.enIo_0;
                            delete ioP_edc;
                        }
                        else {
                            global_data.enIo_0 = umpP->umpData.enIo_0;
                            ioP_edc = new io_edc;
                            ioP_edc->ioIni_edc();
                        }
                    }
                    thisini->SetConfig(SoftwareConfig::kSet0IO,global_data.enIo_0);

                    if((global_data.enIo_1 != umpP->umpData.enIo_1) && hd_io) {
                        if(enIo1) {
                            if(umpP->umpData.enIo_1) {
                                global_data.enIo_1 = umpP->umpData.enIo_1;
                                delete ioP1;
                                ioP1 = new io();
                                ioP1->ioIni(1);
                            }
                            else {
                                global_data.enIo_1 = umpP->umpData.enIo_1;
                                delete ioP1;
                                enIo1 = false;
                            }
                        }
                        else {
                            global_data.enIo_1 = umpP->umpData.enIo_1;
                            ioP1 = new io();
                            ioP1->ioIni(1);
                            enIo1 = true;
                        }
                    }
                    thisini->SetConfig(SoftwareConfig::kSet1IO,global_data.enIo_1);

                    if((global_data.enIo_2 != umpP->umpData.enIo_2) && hd_io) {
                        if(enIo2) {
                            if(umpP->umpData.enIo_2) {
                                global_data.enIo_2 = umpP->umpData.enIo_2;
                                cout << "io2:" << global_data.enIo_2 << endl;
                                delete ioP2;
                                cout << "delete io2:" << global_data.enIo_2 << endl;
                                ioP2 = new io();
                                ioP2->ioIni(2);
                                cout << "newio2:" << global_data.enIo_2 << endl;
                            }
                            else {
                                global_data.enIo_2 = umpP->umpData.enIo_2;
                                delete ioP2;
                                enIo2 = false;
                            }
                        }
                        else {
                            global_data.enIo_2 = umpP->umpData.enIo_2;
                            ioP2 = new io();
                            ioP2->ioIni(2);
                            enIo2 = true;
                        }
                    }
                    thisini->SetConfig(SoftwareConfig::kSet2IO,global_data.enIo_2);

                    if((global_data.enIo_3 != umpP->umpData.enIo_3) && hd_io) {
                        if(enIo3) {
                            if(umpP->umpData.enIo_3) {
                                global_data.enIo_3 = umpP->umpData.enIo_3;
                                delete ioP3;
                                ioP3 = new io();
                                ioP3->ioIni(3);
                            }
                            else {
                                global_data.enIo_3 = umpP->umpData.enIo_3;
                                delete ioP3;
                                enIo3 = false;
                            }
                        }
                        else {
                            global_data.enIo_3 = umpP->umpData.enIo_3;
                            ioP3 = new io();
                            ioP3->ioIni(3);
                            enIo3 = true;
                        }
                    }
                    thisini->SetConfig(SoftwareConfig::kSet3IO,global_data.enIo_3);
                    thisini->SaveConfig();
                    break;
                case SAME:
                    break;
                default:
                    break;
            }
            continue;
        }

    }
    return 0;
}
