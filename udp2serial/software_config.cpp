#include "software_config.h"

char filepath_[36];

SoftwareConfig::SoftwareConfig()
{
    if( !MakeDir(DATA_FILE) ){
        sprintf(filepath_, "./%s", SERIAL_CONFIG_FILE);
        }else{
        sprintf(filepath_, "%s/%s", DATA_FILE, SERIAL_CONFIG_FILE);
        }

    // 初始化 向量类型softwareConfig,定义其长度为kSoftWareConfigIDMax，内容都先置为空
    configvalue_.assign(kSoftWareConfigIDMax, "");
    //network
    configvalue_[kctlip]            =   "0.0.0.0";
    configvalue_[kctl232port]       =   "0";
    configvalue_[kctl485port]       =   "0";
    configvalue_[kctlrelayport]     =   "0";
    configvalue_[kctlinfraredport]  =   "0";
    configvalue_[kctlIO0port]       =   "0";
    configvalue_[kctlIO1port]       =   "0";
    //板子UDP接收端口，原则上不变
    configvalue_[ksv232port]        =   "16232";
    configvalue_[ksv485port]        =   "16485";
    configvalue_[ksvrelayport]      =   "16456";
    configvalue_[kinfraredport]     =   "16457";
    configvalue_[ksvIOport]         =   "16458";
    //RS232--串口使能和属性
    configvalue_[ken232]            =   "1";
    configvalue_[k232port]          =   "com1";
    configvalue_[k232uartspeed]     =   "115200";
    configvalue_[k232nbit]          =   "8";
    configvalue_[k232nstop]         =   "1";
    configvalue_[k232event]         =   "N";
    configvalue_[k232vtime]         =   "3";
    configvalue_[k232vmin]          =   "10";
    //RS485--串口使能和属性
    configvalue_[ken485]            =   "0";
    configvalue_[k485port]          =   "com2";
    configvalue_[k485uartspeed]     =   "9600";
    configvalue_[k485nbit]          =   "8";
    configvalue_[k485nstop]         =   "1";
    configvalue_[k485event]         =   "N";
    configvalue_[k485vtime]         =   "3";
    configvalue_[k485vmin]          =   "10";
    //继电器
    configvalue_[kenrelay]          =   "0";
    configvalue_[krelayst]          =   "0";
    //红外---使能和属性（GPIO模拟和串口两种形式，通过硬件号区分）
    configvalue_[kenInfrared]       =   "0";
    configvalue_[kInfraredport]     =   "com2";
    configvalue_[kInfrareduartspeed]=   "9600";
    configvalue_[kInfrarednbit]     =   "8";
    configvalue_[kInfrarednstop]    =   "1";
    configvalue_[kInfraredevent]    =   "N";
    configvalue_[kInfraredvtime]    =   "3";
    configvalue_[kInfraredvmin]     =   "10";
    //IO--0(未使能)，1（接收），2（发送）
    configvalue_[kSet0IO]            =   "0";
    configvalue_[kIO0st]             =   "0";
    configvalue_[kSet1IO]            =   "0";
    configvalue_[kIO1st]             =   "0";
    configvalue_[kSet2IO]            =   "0";
    configvalue_[kIO2st]             =   "0";
    configvalue_[kSet3IO]            =   "0";
    configvalue_[kIO3st]             =   "0";
    //TCP
    configvalue_[kTcpEnable]         =   "1";
    configvalue_[kTcpServerPort]     =   "8899";
    configvalue_[kTcpLinkMax]        =   "32";


    snprintf(paramsnamelist[kctlip],                    PARA_NAME_LEN, "%s=", EnumtoStr(kctlip));
    snprintf(paramsnamelist[kctl232port],               PARA_NAME_LEN, "%s=", EnumtoStr(kctl232port));
    snprintf(paramsnamelist[kctl485port],               PARA_NAME_LEN, "%s=", EnumtoStr(kctl485port));
    snprintf(paramsnamelist[kctlrelayport],             PARA_NAME_LEN, "%s=", EnumtoStr(kctlrelayport));
    snprintf(paramsnamelist[kctlinfraredport],          PARA_NAME_LEN, "%s=", EnumtoStr(kctlinfraredport));
    snprintf(paramsnamelist[kctlIO0port],               PARA_NAME_LEN, "%s=", EnumtoStr(kctlIO0port));
    snprintf(paramsnamelist[kctlIO1port],               PARA_NAME_LEN, "%s=", EnumtoStr(kctlIO1port));
    snprintf(paramsnamelist[ksv232port],                PARA_NAME_LEN, "%s=", EnumtoStr(ksv232port));
    snprintf(paramsnamelist[ksv485port],                PARA_NAME_LEN, "%s=", EnumtoStr(ksv485port));
    snprintf(paramsnamelist[ksvrelayport],              PARA_NAME_LEN, "%s=", EnumtoStr(ksvrelayport));
    snprintf(paramsnamelist[kinfraredport],             PARA_NAME_LEN, "%s=", EnumtoStr(kinfraredport));
    snprintf(paramsnamelist[ksvIOport],                 PARA_NAME_LEN, "%s=", EnumtoStr(ksvIOport));
    snprintf(paramsnamelist[ken232],                    PARA_NAME_LEN, "%s=", EnumtoStr(ken232));
    snprintf(paramsnamelist[k232port],                  PARA_NAME_LEN, "%s=", EnumtoStr(k232port));
    snprintf(paramsnamelist[k232uartspeed],             PARA_NAME_LEN, "%s=", EnumtoStr(k232uartspeed));
    snprintf(paramsnamelist[k232nbit],                  PARA_NAME_LEN, "%s=", EnumtoStr(k232nbit));
    snprintf(paramsnamelist[k232nstop],                 PARA_NAME_LEN, "%s=", EnumtoStr(k232nstop));
    snprintf(paramsnamelist[k232event],                 PARA_NAME_LEN, "%s=", EnumtoStr(k232event));
    snprintf(paramsnamelist[k232vtime],                 PARA_NAME_LEN, "%s=", EnumtoStr(k232vtime));
    snprintf(paramsnamelist[k232vmin],                  PARA_NAME_LEN, "%s=", EnumtoStr(k232vmin));
    snprintf(paramsnamelist[ken485],                    PARA_NAME_LEN, "%s=", EnumtoStr(ken485));
    snprintf(paramsnamelist[k485port],                  PARA_NAME_LEN, "%s=", EnumtoStr(k485port));
    snprintf(paramsnamelist[k485uartspeed],             PARA_NAME_LEN, "%s=", EnumtoStr(k485uartspeed));
    snprintf(paramsnamelist[k485nbit],                  PARA_NAME_LEN, "%s=", EnumtoStr(k485nbit));
    snprintf(paramsnamelist[k485nstop],                 PARA_NAME_LEN, "%s=", EnumtoStr(k485nstop));
    snprintf(paramsnamelist[k485event],                 PARA_NAME_LEN, "%s=", EnumtoStr(k485event));
    snprintf(paramsnamelist[k485vtime],                 PARA_NAME_LEN, "%s=", EnumtoStr(k485vtime));
    snprintf(paramsnamelist[k485vmin],                  PARA_NAME_LEN, "%s=", EnumtoStr(k485vmin));
    snprintf(paramsnamelist[kenrelay],                  PARA_NAME_LEN, "%s=", EnumtoStr(kenrelay));
    snprintf(paramsnamelist[krelayst],                  PARA_NAME_LEN, "%s=", EnumtoStr(krelayst));
    snprintf(paramsnamelist[kenInfrared],               PARA_NAME_LEN, "%s=", EnumtoStr(kenInfrared));
    snprintf(paramsnamelist[kInfraredport],             PARA_NAME_LEN, "%s=", EnumtoStr(kInfraredport));
    snprintf(paramsnamelist[kInfrareduartspeed],        PARA_NAME_LEN, "%s=", EnumtoStr(kInfrareduartspeed));
    snprintf(paramsnamelist[kInfrarednbit],             PARA_NAME_LEN, "%s=", EnumtoStr(kInfrarednbit));
    snprintf(paramsnamelist[kInfrarednstop],            PARA_NAME_LEN, "%s=", EnumtoStr(kInfrarednstop));
    snprintf(paramsnamelist[kInfraredevent],            PARA_NAME_LEN, "%s=", EnumtoStr(kInfraredevent));
    snprintf(paramsnamelist[kInfraredvtime],            PARA_NAME_LEN, "%s=", EnumtoStr(kInfraredvtime));
    snprintf(paramsnamelist[kInfraredvmin],             PARA_NAME_LEN, "%s=", EnumtoStr(kInfraredvmin));
    snprintf(paramsnamelist[kSet0IO],                   PARA_NAME_LEN, "%s=", EnumtoStr(kSet0IO));
    snprintf(paramsnamelist[kIO0st],                    PARA_NAME_LEN, "%s=", EnumtoStr(kIO0st));
    snprintf(paramsnamelist[kSet1IO],                   PARA_NAME_LEN, "%s=", EnumtoStr(kSet1IO));
    snprintf(paramsnamelist[kIO1st],                    PARA_NAME_LEN, "%s=", EnumtoStr(kIO1st));
    snprintf(paramsnamelist[kSet2IO],                   PARA_NAME_LEN, "%s=", EnumtoStr(kSet2IO));
    snprintf(paramsnamelist[kIO2st],                    PARA_NAME_LEN, "%s=", EnumtoStr(kIO2st));
    snprintf(paramsnamelist[kSet3IO],                   PARA_NAME_LEN, "%s=", EnumtoStr(kSet3IO));
    snprintf(paramsnamelist[kIO3st],                    PARA_NAME_LEN, "%s=", EnumtoStr(kIO3st));
    snprintf(paramsnamelist[kTcpEnable],                PARA_NAME_LEN, "%s=", EnumtoStr(kTcpEnable));
    snprintf(paramsnamelist[kTcpServerPort],            PARA_NAME_LEN, "%s=", EnumtoStr(kTcpServerPort));
    snprintf(paramsnamelist[kTcpLinkMax],               PARA_NAME_LEN, "%s=", EnumtoStr(kTcpLinkMax));

}

SoftwareConfig::~SoftwareConfig()
{
    COMMON_PRT("SoftwareConfig Normal Exit!\n");
}

bool SoftwareConfig::ReadConfig()
{
    COMMON_PRT("read_config:%s\n", filepath_);

    read_ini ri( filepath_ );
    for(uint i = kctlip; i < kSoftWareConfigIDMax; i++){
        ri.find_value( paramsnamelist[i],   configvalue_[i] );
    }

    return true;
}


bool SoftwareConfig::SaveConfig()
{
    FILE *f = NULL;
    f = fopen( filepath_, "wb" );
    if(f)
    {
        for(uint i = kctlip; i < kSoftWareConfigIDMax; i++){
            fprintf(f, "%s%s\n", paramsnamelist[i], configvalue_[i].c_str());
        }
        fflush(f);
        fclose(f);
        f = NULL;
        COMMON_PRT("save config succeed");
        return true;

    }

    COMMON_PRT("save config failed");
    return false;
}

void SoftwareConfig::PrintConfig()
{
    for(uint i = kctlip; i < kSoftWareConfigIDMax; i++){
        printf("%s%s\n", paramsnamelist[i], configvalue_[i].c_str());
    }
}

bool SoftwareConfig::SetConfig(const SoftWareConfigID kId, string value)
{
    if(kId < kSoftWareConfigIDMax)
        configvalue_[kId] = value;
    else
        return false;

    return true;
}

bool SoftwareConfig::SetConfig(const SoftWareConfigID kId, int value)
{
    if(kId < kSoftWareConfigIDMax)
        configvalue_[kId] = int2str(value);
    else
        return false;

    return true;
}

string SoftwareConfig::GetConfig(const SoftWareConfigID kId)
{
    if(kId < kSoftWareConfigIDMax)
        return configvalue_[kId];
    else
        return "error";
}


bool SoftwareConfig::reset()
{
    configvalue_[kctlip]            =   "0.0.0.0";
    configvalue_[kctl232port]       =   "0";
    configvalue_[kctl485port]       =   "0";
    configvalue_[kctlrelayport]     =   "0";
    configvalue_[kctlinfraredport]  =   "0";
    configvalue_[kctlIO0port]       =   "0";
    configvalue_[kctlIO1port]       =   "0";
    //板子UDP接收端口，原则上不变
    configvalue_[ksv232port]        =   "16232";
    configvalue_[ksv485port]        =   "16485";
    configvalue_[ksvrelayport]      =   "16456";
    configvalue_[kinfraredport]     =   "16457";
    configvalue_[ksvIOport]         =   "16458";
    //RS232--串口使能和属性
    configvalue_[ken232]            =   "1";
    configvalue_[k232port]          =   "com1";
    configvalue_[k232uartspeed]     =   "115200";
    configvalue_[k232nbit]          =   "8";
    configvalue_[k232nstop]         =   "1";
    configvalue_[k232event]         =   "N";
    configvalue_[k232vtime]         =   "3";
    configvalue_[k232vmin]          =   "10";
    //RS485--串口使能和属性
    configvalue_[ken485]            =   "0";
    configvalue_[k485port]          =   "com2";
    configvalue_[k485uartspeed]     =   "9600";
    configvalue_[k485nbit]          =   "8";
    configvalue_[k485nstop]         =   "1";
    configvalue_[k485event]         =   "N";
    configvalue_[k485vtime]         =   "3";
    configvalue_[k485vmin]          =   "10";
    //继电器
    configvalue_[kenrelay]          =   "0";
    configvalue_[krelayst]          =   "0";
    //红外---使能和属性（GPIO模拟和串口两种形式，通过硬件号区分）
    configvalue_[kenInfrared]       =   "0";
    configvalue_[kInfraredport]     =   "com2";
    configvalue_[kInfrareduartspeed]=   "9600";
    configvalue_[kInfrarednbit]     =   "8";
    configvalue_[kInfrarednstop]    =   "1";
    configvalue_[kInfraredevent]    =   "N";
    configvalue_[kInfraredvtime]    =   "3";
    configvalue_[kInfraredvmin]     =   "10";
    //IO--0(未使能)，1（接收），2（发送）
    configvalue_[kSet0IO]           =   "0";
    configvalue_[kIO0st]            =   "0";
    configvalue_[kSet1IO]           =   "0";
    configvalue_[kIO1st]            =   "0";
    //TCP
    configvalue_[kTcpEnable]        =   "1";
    configvalue_[kTcpServerPort]    =   "8899";
    configvalue_[kTcpLinkMax]       =   "32";

    this->SaveConfig();
    return true;
}

