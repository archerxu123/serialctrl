#最新带GPIO红外
TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

#指定生成的应用程序名
TARGET = udpSerial
#指定目标文件(obj)的存放目录
OBJECTS_DIR += ../obj

#3536 V400编译选项
#不检测定义后未使用的参数错误等
#QMAKE_CXXFLAGS += -std=c++11 \
#                  -mcpu=cortex-a7 \
#                  -mfloat-abi=softfp \
#                  -mfpu=neon-vfpv4 \
#                  -mno-unaligned-access \
#                  -fno-aggressive-loop-optimizations
#QMAKE_CFLAGS += -mcpu=cortex-a7 \
#                -mfloat-abi=softfp \
#                -mfpu=neon-vfpv4 \
#                -mno-unaligned-access \
#                -fno-aggressive-loop-optimizations
#BASE_PATH = /home/9_my_code_tool/public/lib/hisilicon/hi3536/sdk-v1.0.7.0


#31A
#QMAKE_CXXFLAGS += -std=c++11 \
#                  -mcpu=cortex-a9 \
#                  -mfloat-abi=softfp \
#                  -mfpu=neon \
#                  -mno-unaligned-access \
#                  -fno-aggressive-loop-optimizations
#QMAKE_CFLAGS += -mcpu=cortex-a9 \
#                -mfloat-abi=softfp \
#                -mfpu=neon \
#                -mno-unaligned-access \
#                -fno-aggressive-loop-optimizations

#BASE_PATH = /home/9_my_code_tool/public/lib/hisilicon/hi3531a/sdk-v1.0.5.0

#INCLUDEPATH += $${BASE_PATH}/include
#LIBS += $${BASE_PATH}/arm-hisiv400/lib/libmpi.a \
#        $${BASE_PATH}/arm-hisiv400/lib/libjpeg.a \
#        $${BASE_PATH}/arm-hisiv400/lib/libVoiceEngine.a \
#        $${BASE_PATH}/arm-hisiv400/lib/libupvqe.a \
#        $${BASE_PATH}/arm-hisiv400/lib/libdnvqe.a \

#QMAKE_CXXFLAGS += -Wno-unused-parameter \
#                  -Wno-unused-but-set-variable \
#                  -Wno-unused-but-set-parameter \
#                  -Wno-narrowing \
#                  -Wno-literal-suffix \
#                  -std=c++11
#QMAKE_CFLAGS += -Wno-unused-parameter \
#                -Wno-unused-but-set-variable \
#                -Wno-unused-but-set-parameter

#INCLUDEPATH += /home/jhxu/work/gitclone/new_udp2serial/common   \

#BASE_PATH = /home/pubilc-tool-lib/Hisilicon/lib/arm-hisiv500-hi3531d-sdk-v1.0.4.0
#BASE_PATH = /home/pubilc-tool-lib/Hisilicon/lib/arm-hisiv500-hi3521d-sdk-v1.0.4.0


#21A
#BASE_PATH = /home/9_my_code_tool/public/lib/hisilicon/hi3521a/sdk-v1.0.7.0

#QMAKE_CXXFLAGS += -std=c++11 \
#                  -mcpu=cortex-a7 \
#                  -mfloat-abi=softfp \
#                  -mfpu=neon-vfpv4 \
#                  -mno-unaligned-access \
#                  -fno-aggressive-loop-optimizations
#QMAKE_CFLAGS += -mcpu=cortex-a7 \
#                -mfloat-abi=softfp \
#                -mfpu=neon-vfpv4 \
#                -mno-unaligned-access \
#                -fno-aggressive-loop-optimizations

#INCLUDEPATH += $${BASE_PATH}/include
#LIBS += $${BASE_PATH}/arm-hisiv510/lib/libmpi.a \
#        $${BASE_PATH}/arm-hisiv510/lib/libjpeg.a \
#        $${BASE_PATH}/arm-hisiv510/lib/libVoiceEngine.a \
#        $${BASE_PATH}/arm-hisiv510/lib/libupvqe.a \
#        $${BASE_PATH}/arm-hisiv510/lib/libdnvqe.a \

#31DV200
BASE_PATH = /home/9_my_code_tool/public/lib/hisilicon/hi3531dv200/sdk-v2.0.1.2/aarch64-himix200
INCLUDEPATH += /home/jhxu/work/gitclone/new_udp2serial/common
INCLUDEPATH += /home/9_my_code_tool/public/lib/hisilicon/hi3531dv200/sdk-v2.0.1.2/include
LIBS += $${BASE_PATH}/lib/libmpi.a \
        $${BASE_PATH}/lib/libVoiceEngine.a \
        $${BASE_PATH}/lib/libupvqe.a \
        $${BASE_PATH}/lib/libdnvqe.a

DEFINES += __31DV200__

#定义编译选项
DEFINES += __LINUX__
DEFINES += __DEBUG__

DEFINES += __GPIO1_5__     #700k
#DEFINES += __GPIO1_6__
#DEFINES += __7000__
#DEFINES += __7000_NJ__      #21A
#DEFINES += __8000__

#DEFINES += __AUTO_TEST__
#DEFINES += __31DV200__

SOURCES += main.cpp \
    software_config.cpp \
    global.cpp \
    rs232.cpp \
    ../common/common.cpp \
    uart.cpp \
    rs485.cpp \
    hi_mpp.cpp \
    relay.cpp \
    infrared.cpp \
    io.cpp \
    umpset.cpp \
    io_edc.cpp \
    tcp_server.cpp

HEADERS += \
    software_config.h \
    global.h \
    ../common/singleton.h \
    ../common/common.h \
    ../common/mytimer.h \
    rs232.h \
    uart.h \
    rs485.h \
    hi_mpp.h \
    relay.h \
    infrared.h \
    io.h \
    umpset.h \
    io_edc.h \
    tcp_server.h

LIBS += -lpthread -lm -ldl -lrt \
        -lstdc++

#DEFINES += __SEND_485__
