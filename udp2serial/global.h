#ifndef GLOBAL_H
#define GLOBAL_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <syslog.h>
#include <signal.h>
#include <errno.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>      /* inet(3) functions */
#include <net/if.h>         /* ifreq struct */
#include <sys/ioctl.h>      /* ioctl() functions */
#include <net/route.h>      /* RTF_GATEWAY */
#include <netdb.h>
#include <string.h>
#include <termios.h>
#include <linux/netlink.h>
#include <asm-generic/types.h>
#include <stdbool.h>
#include <pthread.h>
#include "singleton.h"
#include "common.h"
#include "software_config.h"
//#include "udpsocket/networkinfo.h"

#define MAXDATA     512
#define UMPPORT     16123

#define ROUTEPATH "/proc/net/route"
#define ETH0      "eth0"
#define ETH1      "eth1"
#define ETHLO     "lo"

typedef enum {
    kHWADDR  = SIOCGIFHWADDR,
    kADDR    = SIOCGIFADDR,
    kBRDADDR = SIOCGIFBRDADDR,
    kNETMASK = SIOCGIFNETMASK,
    kALL
}IF_CONFIG;

#define HD_700K_NJ_V1   "SHNJ2301fd_100_V1"     //232 485 relay
#define HD_700K_NJ_V2   "SHNJ2301fd_100_V2"     //232 485 relay
#define HD_700K_NJ_V3   "SHNJ2301fd_100_V3"     //232 485 relay ir io
#define HD_D_HDMI_NJ    "SHNJ2301fd_110_V1"     //双层HDMI 232 485 relay

#define HD_4HDMI_NJ     "SHNJ2301fd_300_V1"     //232
#define HD_4MIX_NJ      "SHNJ2301fd_400_V1"     //232
#define HD_2SDI_NJ      "SHNJ23015d_400_V1"     //232  uart-ir
#define HD_300T_NJ      "SHNJ23015d_100_V2"    //232  uarr_ir


#define HD_700K_GZ      "SHGZ2301fd_300_V1"     //232 485 relay ir-gpio io
#define HD_700K_GZ_A    "SHGZ2301fa_100_V1"     //232 485 relay ir-gpio io
#define HD_700K_GZ_V2   "SHGZ2301fd_300_V2"     //232 485 relay ir-gpio io
#define HD_A156_NJ_V2   "SHNJ2301fd_100_V4"     //232 485 relay ir-gpio io  --南京仿广州31d v2
#define HD_A241_NJ      "SHNJ2301fa_100_V1"     //232 485 relay ir-gpio io  --和A156硬件相同 类似700k

#define HD_8000_GZ      "SHGZ2301fd_310_V1"     //232 485 relay ir-gpio io
#define HD_700S_GZ      "SHGZ230240_100_V1"     //232 485 relay ir-gpio io   --广州3536
#define HD_7000_NJ      "SHNJ23014d4_7000_V1"   //232 ir-gpio io
#define HD_7020_NJ      "SHNJ23014d4_7020_V1"   //232 ir-gpio io
#define HD_7000_NJ_HD   "SHNJ23014d4_7010_V1"   //232 ir-gpio io 南京硬环出7000
#define HD_7000_PTN     "SHDJ23014d4_7000_V1"   //232 ir-gpio io

#define HD_21A_PTN      "SHDJ23015a_100_V1"     //232 ir-gpio io

#define HD_DEC_GZ       "SHGZ2301fd_400_V1"     //uart1-232,uart2-232  relay
#define HD_300R_GZ      "SHGZ2301fd_100_V1"

#define HD_31DV200_GZ   "SHGZ2301fdv200_100_V1" //232 ir-gpio io

#define HD_31DV200_NJ  "SHNJ2301fdv200_200_V1" //232  485 relay ir-gpio io

#define HD_A151_NJ      "SHNJ23015d_500_V1_" //232 ir-gpio io    --21D核心板 out(rx)做了新版，A323，硬件版本号统一对应A323
#define HD_A323_NJ      "SHNJ23015d_500_V1" //232 ir-gpio io    --21D核心板
#define HD_A186_NJ      "SHNJ23015d_7100_V1" //232 ir-gpio io  --21d 7000plus
#define HD_31A_D1_GZ   "SHGZ2301fa_200_V1" //232 ir-gpio io
#define HD_21D_D_HDMI   "SHNJ23015d_600_V1" //232 485 relay io_ir
#define HD_21D_860      "SHNJ23015d_860_V1" //232 io_ir   --21D墙插
#define HD_GZ16A_200    "SHGZ23010a_200_V1"     //232-2 relay
#define HD_GZ16A_100    "SHGZ23010a_100_V1"     //232-uart3   IO-4
#define HD_GZ36_200_V1  "SHGZ230240_200_V1"     //485 232-2

#ifdef __GPIO1_5__

#define IRTIMEHD    "SHGZ2301fd_300_V1"


#endif

#define IR_8000     "SHGZ2301fd_310_V1"

#ifdef __7000_NJ__
#define IRTIMEHD    "SHNJ23014d4_7000_V1"
#endif

#define A335_13_4_MUX       0x120F0174              //0-GPIO
#define A335_13_4_DIR       0x12220400
#define A335_13_4_DATA      0x12220040
#define A335_13_4_SET       0x10

typedef struct configdata_
{
    char ctlip[32];
    int ctl232port;
    int ctl485port;
    int ctlrelayport;
    int ctlinfraredport;
    int ctlIO0port;
    int ctlIO1port;
    int ctlIO2port;
    int ctlIO3port;
    int sv232port;
    int sv485port;
    int svrelayport;
    int svInfraredport;
    int svIOport;
    int en232;
    char port232[8];
    long uartspeed232;
    int nbit232;
    int nstop232;
    char event232[4];
    int nVtime232;
    int nVmin232;
    int en485;
    char port485[8];
    long uartspeed485;
    int nbit485;
    int nstop485;
    char event485[4];
    int nVtime485;
    int nVmin485;
    int enRelay;
    int relaySt;    //1-on  0-off
    int enInfrared;
    char portInfrared[8];
    long uartspeedInf;
    int nbitInf;
    int nstopInf;
    char eventInf[4];
    int nVtimeInf;
    int nVminInf;
    int enIo_0;
    int io0St;
    int enIo_1;
    int io1St;
    int enIo_2;
    int io2St;
    int enIo_3;
    int io3St;
    int tcp_en;
    int tcp_port;
    #ifdef __31DV200__
    int ctlIO2port;
    int ctlIO3port;
    int enIo_2;
    int io2St;
    int enIo_3;
    int io3St;
    int enRelay1;
    int relay1St;
    #endif
    int tcp_link_max;
}CONFIGDATA;

//select()»úÖÆ
typedef struct uartParam
{
    char port[8];
    long speed;
    int nbit;
    int nstop;
    int event;
    int Vtime;
    int Vmin;
}UARTPARAM;

typedef enum {
    NJ_700K = 0,
    NJ_D_700K,      //双层HDMI
    NJ_7000,
    NJ_21D,
    GZ_700K,
    NJ_A156_V2,
    NJ_A241,
    NJ_A335,
    GZ_8000,
    GZ_EDC,
    PTN_7000,
    PTN_21A,
    GZ_700S,
    NJ_31DV200,
    GZ_31DV200,
    NJ_A304,      //南京31DV200
    NJ_A151,     //21D核心板
    NJ_A323,     //21D核心板
    NJ_A186_V1,   // 7000plus
    GZ_31A_D2,   //广州31a_D2
    NJ_A345,
    GZ16A_200_V1,
    GZ16A_100_V1,
    GZ36_200_V1,
    MAX_HD
}VERSION;


extern CONFIGDATA global_data;
extern fd_set readFd;
extern int maxFd;
extern bool en232;
extern bool en485;
extern bool enRelay;
extern bool enIr;
extern bool enIo0;
extern bool enIo1;
extern bool enIo2;
extern bool enIo3;
extern bool irTime;

extern bool hd_232_edc;
extern bool ir_nj_700k;
extern bool ir_nj_21d;
extern VERSION hd_ver;

extern bool outmode;

extern string local_ip;
extern bool en_3536;

#ifdef __31DV200__
extern bool enIo2;
extern bool enIo3;
extern bool enRelay1;
#endif


#endif // GLOBAL_H
