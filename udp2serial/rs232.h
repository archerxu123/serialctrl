//RS232通常启用UART1
#ifndef RS232_H
#define RS232_H
#include "global.h"
#include "uart.h"
#include "tcp_server.h"
#include "hi_mpp.h"
#define HEAD232_0      "hd232_0="
#define HEAD232_1      "hd232_1="


class rs232
{
public:
    rs232();
    ~rs232();
    bool iniRs232();
    bool rcvData2uart();
    bool readUart2ctrl();
    bool rcvData2uart_edc();
    bool readUart2ctrl_edc();
    bool UMPsetParam(bool uartFlag,bool ctrlPortFlag,bool svPortFlag);

public:
    int dev232Fd = -1;
    int dev232Fd_edc = -1;
    int socket232 = -1;
    int socket232_edc = -1;
private:
    struct sockaddr_in ctrlAddr;
    tcp_server *tcp_sv;
    static void* pth_tcp_server(void *arg);


};

#endif // RS232_H
