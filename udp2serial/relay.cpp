#include "relay.h"

relay::relay()
{

}

relay::~relay()
{
    close(socketRelay);
    socketRelay = -1;
}

bool relay::relayIni()
{
    uint value;
    if(hd_ver == GZ_700S) {
        Hi_SetReg(RELAY_36_REG,0x1);
        relayStr = global_data.relaySt;
        relaySet(relayStr);

        Hi_GetReg(RELAY_36_DIR,&value);
        value |= RELAY_36_SET;
        Hi_SetReg(RELAY_36_DIR,value);
    }
    else if(hd_ver == GZ_EDC) {
        Hi_SetReg(RELAY_REG_EDC,0x0);
        relayStr = global_data.relaySt;
        relaySet(relayStr);

        Hi_GetReg(RELAY_DIR_EDC,&value);
        value |= RELAY_SET_EDC;
        Hi_SetReg(RELAY_DIR_EDC,value);
    }
    else if(hd_ver == NJ_31DV200) {
        Hi_GetReg(RELAY_0_REG, &value);
        value &= (~0x1);
        Hi_SetReg(RELAY_0_REG, value);

        Hi_GetReg(RELAY_0_DIR, &value);
        value |= RELAY_0_SET;
        Hi_SetReg(RELAY_0_DIR, value);

        Hi_GetReg(RELAY_1_REG, &value);
        value &= (~0x1);
        Hi_SetReg(RELAY_1_REG, value);

        Hi_GetReg(RELAY_1_DIR, &value);
        value |= RELAY_1_SET;
        Hi_SetReg(RELAY_1_DIR, value);
    }
    else if(NJ_A335 == hd_ver) {
        Hi_SetReg(A335_6_2_REG , 0x1);

        Hi_GetReg(A335_6_2_DIR, &value);
        value |= A335_6_2_SET;
        Hi_SetReg(A335_6_2_DIR, value);
        relayStr = global_data.relaySt;
        relaySet(relayStr);
    }
    else if(GZ16A_200_V1 == hd_ver) {
        io.dir = 0x201E0400;
        io.data = 0x201E0010;
        io.set = 0x4;
        Hi_GetReg(io.dir, &value);
        value |= io.set;
        Hi_SetReg(io.dir, value);
        relayStr = global_data.relaySt;
        relaySet(relayStr);
    }
    else {
        Hi_SetReg(RELAY_REG,0x1);
        relayStr = global_data.relaySt;
        relaySet(relayStr);

        Hi_GetReg(RELAY_DIR,&value);
        value |= RELAY_SET;
        Hi_SetReg(RELAY_DIR,value);

    }



    socketRelay = createUdp(global_data.svrelayport);
    FD_SET(socketRelay,&readFd);
    if(socketRelay >= maxFd)
        maxFd = socketRelay + 1;

    return true;
}

bool relay::rcvData2Relay()
{
    char tmpData[128] = {0};
    char relayEcho[8] = {0};
    int rcvLen = 0;
    socklen_t addrLen = 0;
    int  sendLen;
    memset(tmpData,0,sizeof(tmpData));
    auto thisini = Singleton<SoftwareConfig>::getInstance();

    addrLen = sizeof(struct sockaddr_in);

    rcvLen = recvfrom(socketRelay,tmpData,MAXDATA,0,(struct sockaddr*)(&ctrlAddr),&addrLen);
    if(rcvLen > 0 ) {
#ifdef __DEBUG__
        print0x(tmpData,rcvLen);
#endif
        if(!strncmp(tmpData,"ON",2)) {
            relayStr = 1;
            relaySet(relayStr);
            global_data.relaySt = 1;
            thisini->SetConfig(SoftwareConfig::krelayst,global_data.relaySt);
        }
        else if(!strncmp(tmpData,"OFF",3)) {
            relayStr = 0;
            relaySet(relayStr);
            global_data.relaySt = 0;
            thisini->SetConfig(SoftwareConfig::krelayst,global_data.relaySt);
        }
        else if(!strncmp(tmpData, "relay_0=ON", strlen("relay_0=ON"))) {
            printf("set relay 0\n");
            relaySet(1, 0);
        }
        else if(!strncmp(tmpData, "relay_1=ON", strlen("relay_1=ON"))) {
            relaySet(1, 1);
        }
        else if(!strncmp(tmpData, "relay_0=OFF", strlen("relay_0=OFF"))) {
            relaySet(0, 0);
        }
        else if(!strncmp(tmpData, "relay_1=OFF", strlen("relay_1=OFF"))) {
            relaySet(0, 1);
        }
        else if(!strncmp(tmpData,"GET",3)) {
            memset(relayEcho,0,sizeof(relayEcho));
            if(relayStr == 1) {
                strcpy(relayEcho,"ON");
            }
            else if(relayStr == 0) {
                strcpy(relayEcho,"OFF");
            }
            if((global_data.ctlrelayport != 0) && (strlen(global_data.ctlip) >= 7)) {
                ctrlAddr.sin_family = AF_INET;
                ctrlAddr.sin_port = htons(global_data.ctlrelayport);
                ctrlAddr.sin_addr.s_addr = inet_addr(global_data.ctlip);
                sendLen = sendto(socketRelay,relayEcho,strlen(relayEcho),0,(struct sockaddr*)&ctrlAddr,sizeof(struct sockaddr_in));
                if(sendLen > 0)
                    return true;
                else {
                    COMMON_PRT("send dev relay str to ctrl error\n");
                    return false;
                }
            }
            else {
                sendLen = sendto(socketRelay,relayEcho,strlen(relayEcho),0,(struct sockaddr*)&ctrlAddr,sizeof(struct sockaddr_in));
                if(sendLen > 0) {
                    COMMON_PRT("UMP not set ctrl IP PORT and echo\n");
                    return true;
                }
                else {
                    COMMON_PRT("send dev relay str to ctrl error\n");
                    return false;
                }
            }

        }
        else {
            COMMON_PRT("ctrl relay send error data:%s\n",tmpData);
            return false;
        }
        thisini->SaveConfig();

    }
    else {
        COMMON_PRT("recv ctrl relay data error\n");
        return false;
    }
    return true;
}

bool relay::relaySet(int set, int relay_num)
{
    uint value;
    if(0 == relay_num) {
        if(set == 1) {
            if(hd_ver == GZ_700S) {
                Hi_GetReg(RELAY_36_DATA,&value);
                value |= RELAY_36_SET;
                Hi_SetReg(RELAY_36_DATA,value);
            }
            else if(hd_ver == GZ_EDC) {
                Hi_GetReg(RELAY_DATA_EDC,&value);
                value |= RELAY_SET_EDC;
                Hi_SetReg(RELAY_DATA_EDC,value);
            }
            else if(hd_ver == NJ_31DV200) {
                Hi_GetReg(RELAY_0_DATA, &value);
                value |= RELAY_0_SET;
                Hi_SetReg(RELAY_0_DATA, value);
                printf("set relay 0 on\n");
            }
            else if(NJ_A335 == hd_ver) {
                Hi_GetReg(A335_6_2_DATA, &value);
                value |= A335_6_2_SET;
                Hi_SetReg(A335_6_2_DATA, value);
            }
            else if(GZ16A_200_V1 == hd_ver) {
                Hi_GetReg(io.data, &value);
                value |= io.set;
                Hi_SetReg(io.data, value);
            }
            else {
                Hi_GetReg(RELAY_DATA,&value);
                value |= RELAY_SET;
                Hi_SetReg(RELAY_DATA,value);
            }
        }
        else if(set == 0) {
            if(hd_ver == GZ_700S) {
                Hi_GetReg(RELAY_36_DATA,&value);
                value &= (~RELAY_36_SET);
                Hi_SetReg(RELAY_36_DATA,value);
            }
            else if(hd_ver == GZ_EDC) {
                Hi_GetReg(RELAY_DATA_EDC,&value);
                value &= (~RELAY_SET_EDC);
                Hi_SetReg(RELAY_DATA_EDC,value);
            }
            else if(hd_ver == NJ_31DV200) {
                Hi_GetReg(RELAY_0_DATA, &value);
                value &= (~RELAY_0_SET);
                Hi_SetReg(RELAY_0_DATA, value);
            }
            else if(NJ_A335 == hd_ver) {
                Hi_GetReg(A335_6_2_DATA, &value);
                value &= (~A335_6_2_SET);
                Hi_SetReg(A335_6_2_DATA, value);
            }
            else if(GZ16A_200_V1 == hd_ver) {
                Hi_GetReg(io.data, &value);
                value &= (~io.set);
                Hi_SetReg(io.data, value);
            }
            else {
                Hi_GetReg(RELAY_DATA,&value);
                value &= (~RELAY_SET);
                Hi_SetReg(RELAY_DATA,value);
            }
        }
        else {
            COMMON_PRT("relay set value error\n");
            return  false;
        }
    }
    else if(1 == relay_num){
        if(set == 1) {
            if(hd_ver == NJ_31DV200) {
                Hi_GetReg(RELAY_1_DATA, &value);
                value |= RELAY_1_SET;
                Hi_SetReg(RELAY_1_DATA, value);
            }

        }
        else if(set == 0) {
            if(hd_ver == NJ_31DV200) {
                Hi_GetReg(RELAY_1_DATA, &value);
                value &= (~RELAY_1_SET);
                Hi_SetReg(RELAY_1_DATA, value);
            }
        }
        else {
            COMMON_PRT("relay set value error\n");
            return  false;
        }
    }


    return true;
}

/*
 * 配合UMP设置，时时生效
 * 全局参数在外部修改，保存
 * */

bool relay::UMPsetParam(bool uartFlag,bool ctrlPortFlag,bool svPortFlag)
{

    if(ctrlPortFlag) {
        COMMON_PRT("UMP set relay ctrl port Param\n");
    }

    if(svPortFlag) {
        FD_CLR(socketRelay,&readFd);
        close(socketRelay);
        sleep(1);

        socketRelay = createUdp(global_data.svrelayport);
        FD_SET(socketRelay,&readFd);
        if(socketRelay >= maxFd)
            maxFd = socketRelay + 1;
    }
    return true;
}
